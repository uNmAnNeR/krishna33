from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin

admin.autodiscover()


urlpatterns = patterns('',
	# Examples:
	# url(r'^$', 'k33.views.home', name='home'),
	# url(r'^k33/', include('k33.foo.urls')),

	# Uncomment the admin/doc line below to enable admin documentation:
	# url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

	# Uncomment the next line to enable the admin:
	url(r'^admin/', include(admin.site.urls)),
	url(r'^votes/',  include('k33.votes.urls')),
	url(r'^accounts/',  include('k33.accounts.urls', namespace='accounts')),
	url(r'^cooking/',  include('k33.cooking.urls')),
	url(r'^posts/',  include('k33.posts.urls', namespace='posts')),
	url(r'^events/',  include('k33.events.urls', namespace='events')),
	url(r'^comments/',  include('k33.comments.urls')),
	url(r'^materials/',  include('k33.materials.urls')),
    # url(r'^production/',  include('k33.production.urls')),
    url(r'^store/',  include('k33.store.urls')),
    url(r'^bookkeeping/',  include('k33.bookkeeping.urls')),
    url(r'^stream/', include('k33.stream.urls')),
	url(r'^$', 'krishna33.views.index', name='index'),
    url(r'^contact$', 'krishna33.views.contact', name='contact'),
	url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),


    # TODO
    url(r'^api/',  include('k33.posts.api.urls')),
    url(r'^api/',  include('k33.uploads.api.urls')),
    url(r'^api/',  include('k33.accounts.api.urls')),
    url(r'^api/',  include('k33.partners.api.urls')),
    url(r'^api/production/',  include('k33.production.api.urls')),
    url(r'^api/store/',  include('k33.store.api.urls')),
    url(r'^api/materials/',  include('k33.materials.api.urls')),

    url(r'^api/bookkeeping/',  include('k33.bookkeeping.api.urls')),

)
from django.conf import settings
if settings.DEBUG:
	# static files (images, css, javascript, etc.)
	urlpatterns += patterns('',
		(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
		'document_root': settings.MEDIA_ROOT}))
