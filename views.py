from django.shortcuts import render
from k33.posts.views import filter_user_posts


# Create your views here.
def index(request):
    user = request.user
    posts = filter_user_posts(user)
    posts = posts.order_by('-updated_at')  # '-published_at',
    return render(request, 'index.html', locals())


def contact(request):
    return render(request, 'contact.html')
