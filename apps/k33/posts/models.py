from datetime import datetime, timedelta

from django.db import models

from k33.comments.models import Commentable
from k33.votes.models import Votable
from k33.private.models import Privatable
from k33.tags.models import Taggable
from k33.uploads.models import ContainsUploads
from sanitize import sanitize_html


from django.contrib.auth import get_user_model

from k33.common.mixins import (CreatedUpdatedAtMixin, Publishable, ModifiedAfterPublishingHelper)

POSTS_MEDIA_DIR = 'posts'


class Post(CreatedUpdatedAtMixin, Publishable, ModifiedAfterPublishingHelper):
    # TODO Publishable, createAt, modifyAt
    TITLE_MIN_LENGTH = 2
    TITLE_MAX_LENGTH = 100

    author = models.ForeignKey(get_user_model())
    raw_content = models.TextField(default='')
    rendered_content = models.TextField(default='')
    title = models.CharField(max_length=100)
    # publish_date = models.DateTimeField(null=True, blank=True)
    # modify_date = models.DateTimeField(auto_now=True)
    # deleted = models.BooleanField(default=False)
    # published = models.BooleanField(default=False)
    # hidden = models.BooleanField(default=False)

    def __str__(self):
        return '{}: {} ({})'.format(self.author, self.title, self.published_at)

    @property
    def cut_content(self):
        return self.rendered_content[:1000]

    def save(self, *args, **kwargs):
        # created = self.pk
        self.rendered_content = sanitize_html(self.raw_content)  # TODO render
        # if created and not self.title:
        #     self.title = 'Сохраненный пост %s' % str(self.id)
        super().save(*args, **kwargs)
        # if not created and not self.title:
        #     self.title = 'Новый пост %s' % str(self.id)
        #     kwargs['force_insert'] = False  # create() uses this, which causes error
        #     super().save(*args, **kwargs)


    # # TODO toggle publsih
    # def publish(self):
    #     self.publish_date = datetime.utcnow()
    #     self.published = True
    #     self.save()
    #
    # def unpublish(self):
    #     self.published = False
    #     self.save()
