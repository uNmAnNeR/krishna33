# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Post.votable_ptr'
        db.delete_column('posts_post', 'votable_ptr_id')

        # Deleting field 'Post.taggable_ptr'
        db.delete_column('posts_post', 'taggable_ptr_id')

        # Deleting field 'Post.commentable_ptr'
        db.delete_column('posts_post', 'commentable_ptr_id')

        # Deleting field 'Post.privatable_ptr'
        db.delete_column('posts_post', 'privatable_ptr_id')

        # Deleting field 'Post.containsuploads_ptr'
        db.delete_column('posts_post', 'containsuploads_ptr_id')

        # Adding field 'Post.id'
        db.add_column('posts_post', 'id',
                      self.gf('django.db.models.fields.AutoField')(primary_key=True, default=-1),
                      keep_default=False)


        # Changing field 'Post.rendered_content'
        db.alter_column('posts_post', 'rendered_content', self.gf('django.db.models.fields.TextField')())

        # Changing field 'Post.raw_content'
        db.alter_column('posts_post', 'raw_content', self.gf('django.db.models.fields.TextField')())

    def backwards(self, orm):

        # User chose to not deal with backwards NULL issues for 'Post.votable_ptr'
        raise RuntimeError("Cannot reverse this migration. 'Post.votable_ptr' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Post.votable_ptr'
        db.add_column('posts_post', 'votable_ptr',
                      self.gf('django.db.models.fields.related.OneToOneField')(unique=True, to=orm['votes.Votable']),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'Post.taggable_ptr'
        raise RuntimeError("Cannot reverse this migration. 'Post.taggable_ptr' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Post.taggable_ptr'
        db.add_column('posts_post', 'taggable_ptr',
                      self.gf('django.db.models.fields.related.OneToOneField')(unique=True, to=orm['tags.Taggable']),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'Post.commentable_ptr'
        raise RuntimeError("Cannot reverse this migration. 'Post.commentable_ptr' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Post.commentable_ptr'
        db.add_column('posts_post', 'commentable_ptr',
                      self.gf('django.db.models.fields.related.OneToOneField')(primary_key=True, unique=True, to=orm['comments.Commentable']),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'Post.privatable_ptr'
        raise RuntimeError("Cannot reverse this migration. 'Post.privatable_ptr' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Post.privatable_ptr'
        db.add_column('posts_post', 'privatable_ptr',
                      self.gf('django.db.models.fields.related.OneToOneField')(unique=True, to=orm['private.Privatable']),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'Post.containsuploads_ptr'
        raise RuntimeError("Cannot reverse this migration. 'Post.containsuploads_ptr' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Post.containsuploads_ptr'
        db.add_column('posts_post', 'containsuploads_ptr',
                      self.gf('django.db.models.fields.related.OneToOneField')(unique=True, to=orm['uploads.ContainsUploads']),
                      keep_default=False)

        # Deleting field 'Post.id'
        db.delete_column('posts_post', 'id')


        # Changing field 'Post.rendered_content'
        db.alter_column('posts_post', 'rendered_content', self.gf('django.db.models.fields.TextField')(null=True))

        # Changing field 'Post.raw_content'
        db.alter_column('posts_post', 'raw_content', self.gf('django.db.models.fields.TextField')(null=True))

    models = {
        'accounts.myuser': {
            'Meta': {'object_name': 'MyUser'},
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '255', 'unique': 'True', 'db_index': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'related_name': "'user_set'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'owned': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'related_name': "'user_set'", 'blank': 'True'})
        },
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '80', 'unique': 'True'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'contenttypes.contenttype': {
            'Meta': {'db_table': "'django_content_type'", 'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType'},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'posts.post': {
            'Meta': {'object_name': 'Post'},
            'author': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['accounts.MyUser']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modify_date': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'publish_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'published': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'raw_content': ('django.db.models.fields.TextField', [], {'default': "''"}),
            'rendered_content': ('django.db.models.fields.TextField', [], {'default': "''"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['posts']