# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Post'
        db.create_table('posts_post', (
            ('containsuploads_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['uploads.ContainsUploads'], unique=True)),
            ('taggable_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['tags.Taggable'], unique=True)),
            ('privatable_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['private.Privatable'], unique=True)),
            ('votable_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['votes.Votable'], unique=True)),
            ('commentable_ptr', self.gf('django.db.models.fields.related.OneToOneField')(primary_key=True, to=orm['comments.Commentable'], unique=True)),
            ('author', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['accounts.MyUser'])),
            ('raw_content', self.gf('django.db.models.fields.TextField')(blank=True, null=True)),
            ('rendered_content', self.gf('django.db.models.fields.TextField')(blank=True, null=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('publish_date', self.gf('django.db.models.fields.DateTimeField')(blank=True, null=True)),
            ('modify_date', self.gf('django.db.models.fields.DateTimeField')(blank=True, auto_now=True)),
            ('published', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('posts', ['Post'])


    def backwards(self, orm):
        # Deleting model 'Post'
        db.delete_table('posts_post')


    models = {
        'accounts.myuser': {
            'Meta': {'object_name': 'MyUser'},
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '255', 'unique': 'True', 'db_index': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['auth.Group']", 'blank': 'True', 'related_name': "'user_set'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'owned': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['auth.Permission']", 'blank': 'True', 'related_name': "'user_set'"})
        },
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '80', 'unique': 'True'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['auth.Permission']", 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'unique_together': "(('content_type', 'codename'),)", 'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'comments.commentable': {
            'Meta': {'object_name': 'Commentable'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'contenttypes.contenttype': {
            'Meta': {'unique_together': "(('app_label', 'model'),)", 'ordering': "('name',)", 'db_table': "'django_content_type'", 'object_name': 'ContentType'},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'posts.post': {
            'Meta': {'_ormbases': ['comments.Commentable', 'votes.Votable', 'private.Privatable', 'tags.Taggable', 'uploads.ContainsUploads'], 'object_name': 'Post'},
            'author': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['accounts.MyUser']"}),
            'commentable_ptr': ('django.db.models.fields.related.OneToOneField', [], {'primary_key': 'True', 'to': "orm['comments.Commentable']", 'unique': 'True'}),
            'containsuploads_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['uploads.ContainsUploads']", 'unique': 'True'}),
            'modify_date': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'}),
            'privatable_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['private.Privatable']", 'unique': 'True'}),
            'publish_date': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'null': 'True'}),
            'published': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'raw_content': ('django.db.models.fields.TextField', [], {'blank': 'True', 'null': 'True'}),
            'rendered_content': ('django.db.models.fields.TextField', [], {'blank': 'True', 'null': 'True'}),
            'taggable_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['tags.Taggable']", 'unique': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'votable_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['votes.Votable']", 'unique': 'True'})
        },
        'private.privatable': {
            'Meta': {'object_name': 'Privatable'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'private': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        'tags.taggable': {
            'Meta': {'object_name': 'Taggable'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'uploads.containsuploads': {
            'Meta': {'object_name': 'ContainsUploads'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'votes.votable': {
            'Meta': {'object_name': 'Votable'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['posts']