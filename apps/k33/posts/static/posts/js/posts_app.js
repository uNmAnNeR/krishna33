$(function () {
	'use strict';

	var FormLayout = Backbone.Marionette.Layout.extend({
		tagName: 'form',
		events: {
			'click button.submit': 'submitClicked'
		},

		submitClicked: function(e){
			e.preventDefault();
			var data = Backbone.Syphon.serialize(this);
			this.trigger("form:submit", data);
		},

		onFormDataInvalid: function(errors){
			var $form = this.$el;

			var clearFormErrors = function(){
				// var $form = $view.find("form");
				$form.find("span.help-block").each(function(){
					$(this).remove();
				});
				$form.find(".form-group.has-error").each(function(){
					$(this).removeClass("has-error");
				});
			}

			var markErrors = function(value, key) {
				var $formGroup = $form.find("[name="+ key + "]").closest('div.form-group');
				
				if ($formGroup.length ===0) {
					// элемент не был найден, добавляем в крышу
					var $errorEl = $('<span>', { class: "help-block label label-danger", text: key+": "+value });
					$form.prepend($errorEl);//.addClass("has-error");
				} else {
					var $errorEl = $('<span>', { class: "help-block", text: value });
					$formGroup.append($errorEl).addClass("has-error");	
				}
			}

			clearFormErrors();
			_.each(errors, markErrors);
		}
	});

	var Upload = Backbone.Model.extend({
		urlRoot: '/api/uploads',

		setType: function() {
			var type = 'unknown';
			if ((/\.(gif|jpg|jpeg|tiff|png)$/i).test(this.get('url'))) {              
				type = 'image';
			}
			this.set({'type': type}, {silent: true});
		},

		initialize: function() {
			this.setType();
			this.on('change', function() {
				if (this.hasChanged('url'))
					this.setType();
			});

		},
	});

	var UploadCollection = Backbone.Collection.extend({
		model: Upload,
		url: Upload.prototype.urlRoot,
	});

	var Post = Backbone.Model.extend({
		urlRoot: '/api/posts',

		setAuthor: function() {
			this.author = new Account(this.get('author'));
			this.set({
					'authorIsOwner': this.author.id == app.user.id
				}, {
					silent: true
				}
			);
		},

		setUploads: function() {
			console.log('uploads', this.get('uploads'))
			this.uploads = new UploadCollection(this.get('uploads'));
		},

		initialize: function() {
			this.setAuthor();
			this.setUploads();
			console.log('private', this);
			this.on('change', function() {
				if (this.hasChanged('author'))
					this.setAuthor();
				if (this.hasChanged('uploads'))
					this.setUploads();
			});

		},

		variants: {
			cut: {
				fields: ['id', 'author', 'private', 'title', 'cut_content', 
				'published', 'publish_date', 'modify_date'],
			},

			edit: {
				fields: ['id', 'private', 'title', 
				'uploads', 'raw_content', 'rendered_content'],
			},

			whole: {
				fields: ['id', 'author', 'private', 'title', 'rendered_content', 
				'published', 'publish_date', 'modify_date'],
			},
		},

		// нельзя записать в обычный defaults, хотя и удобно,
		// т.к. тогда не будет работать подгрузка полей, потому что 
		// дефолтные значения попадают в obj.attributes
		newDefaults: {
			private: true,
			title: '',
			raw_content: '',
		}
	});

	var PostCollection = Backbone.Collection.extend({
		model: Post,
		url: Post.prototype.urlRoot,
	});

	var PostLayout = Backbone.Marionette.Layout.extend({
		template: "#post-template",

		regions: {
			managePanel: ".manage-panel",
			author: ".author",
			content: ".content"
		}
	});

	var PostManagePanelView = Marionette.ItemView.extend({
		template: '#post-manage-panel-template',

		events: {
			'click a.delete': 'deleteClicked',
			'click a.publish': 'publishClicked',
			'click a.unpublish': 'unpublishClicked',
		},

		serializeData: function() {
			// добавим поле является ли пользователь админом
			var data = this.model.toJSON();
			data.userIsStaff = app.user.get('is_staff');
			return data;
		},

		deleteClicked: function(e){
			e.preventDefault();
			this.trigger("post:delete", this.model);
			this.model.destroy({
				success: function() {
					app.trigger("posts:list");
				}
			});
			
		},

		publishClicked: function(e){
			e.preventDefault();
			var url = "/api/posts/"+this.model.get('id')+"/publish"; //e.target.href;
			this.trigger("post:publish", this.model);
			var that = this;
			$.post(url, function(data) {
				that.model.set(data);
				app.trigger("posts:list");
			});
		},

		unpublishClicked: function(e){
			e.stopPropagation();
			var url = "/api/posts/"+this.model.get('id')+"/unpublish"; //e.target.href;
			this.trigger("post:unpublish", this.model);
			var that = this;
			$.post(url, function(data) {
				that.model.set(data);
			});
		},

		modelEvents: {
			'change': "modelChanged"
		},

		modelChanged: function() {
			this.render();
		}
	});

	var PostContentView = Marionette.ItemView.extend({
		template: '#post-content-template',
	});

	var PostCutContentView = Marionette.ItemView.extend({
		template: '#post-cut-content-template',
		className: 'post-cut-content',
	});

	var PostAuthorView = Marionette.ItemView.extend({
		template: '#post-author-template',
	});


	// EDIT
	var PostEditLayout = FormLayout.extend({
		template: "#post-edit-template",
		
		regions: {
			uploads: ".uploads",
			editContent: ".edit-content",
		}
	});

	var PostUploadView = Marionette.ItemView.extend({
		tagName: 'li',
		template: '#post-upload-template',

		events: {
			'click a.delete': 'deleteClicked',
		},

		deleteClicked: function(e) {
			e.preventDefault();
			this.model.destroy();
		},
	});

	var PostUploadsListView = Marionette.CompositeView.extend({
		tagName: 'ul',
		// className: "posts-list",
		className: 'list-inline',
		template: '#post-uploads-list-template',
		// emptyView: NoContactsView,
		itemView: PostUploadView,
		// itemViewContainer: "tbody",

		initialize: function(){
			this.listenTo(this.collection, "reset", function(){
				this.appendHtml = function(collectionView, itemView, index){
					collectionView.$el.append(itemView.el);
				}
			});
		},

		onCompositeCollectionRendered: function(){
			this.appendHtml = function(collectionView, itemView, index){
				collectionView.$el.prepend(itemView.el);
			}
		},

		collectionEvents: {
			'change': "collectionChanged"
		},

		collectionChanged: function() {
			this.render();
		},
	});

	var PostEditContentView = Marionette.ItemView.extend({
		template: '#post-edit-content-template',
	});

	var PostListView = Marionette.CompositeView.extend({
		// tagName: "table",
		className: "posts-list",
		template: "#post-list-template",
		// emptyView: NoContactsView,
		itemView: PostCutContentView,
		// itemViewContainer: "tbody",
	});

	var loadPost = function(id, variant) {
		var d = $.Deferred();
		var options = Post.prototype.variants[variant];
		var post = undefined;
		if (app.post && app.post.id == id) {
			// есть ли пост в кеше
			post = app.post;
		} else if (app.posts) {
			// может в коллекции он есть
			post = app.posts.get(id);			
		}
		if (post) {
			// если нашелся в кеше, то смотрим какие поля надо догружать
			options.fields = _.difference(options.fields, _.keys(post.attributes));
		}
		post = post || new Post({id: id});
		if (options.fields.length == 0) {
			d.resolve(post);
		} else {
			post.fetch({
				data: options,
				processData: true,
				success: function() {
					app.post = post; // запишем в кеш
					d.resolve(post);
				},
				error: function() {
					d.resolve(undefined);
				}
			});
		}
		return d.promise();
	};

	var loadPosts = function(variant) {
		var d = $.Deferred();
		var options = Post.prototype.variants[variant];

		app.posts = new PostCollection();
		app.posts.fetch({
			data: options,
			processData: true,
			success: function() {
				d.resolve(app.posts);
			},
			error: function() {
				d.resolve(undefined);
			}
		});
		return d.promise();
	};


	var controller = {

		showPost: function(id) {
			$.when(loadPost(id, 'whole')).done(function(post){
				// TODO if post undefined!
				var postLayout = new PostLayout();
				app.postRegion.show(postLayout);

				postLayout.managePanel.show(new PostManagePanelView({model: post}));				
				postLayout.author.show(new PostAuthorView({model: post.author}));
				postLayout.content.show(new PostContentView({model: post}));
			})
		},

		listPosts: function() {
			$.when(loadPosts('cut')).done(function(posts) {
				var postsListView = new PostListView({
					collection: posts,
				});

				app.postRegion.show(postsListView);
			})
		},

		editPost: function(id) {
			var renderPost = function(post) {
				var postEditLayout = new PostEditLayout();
				app.postRegion.show(postEditLayout);

				postEditLayout.editContent.show(new PostEditContentView({model: post}));
				postEditLayout.uploads.show(new PostUploadsListView({collection: post.uploads}));

				postEditLayout.on("form:submit", function(data){
					console.log(data);
					post.save(data, {
						success: function(model, response, options) {
							app.trigger("post:show", post.get('id'));
						},
						error: function(model, xhr, options) {
							console.log("error", model, xhr, options);
							// TODO обработка ошибок с сервера
							// if (xhr.status != 200) {
							// 	alert('Ошибка ')
							// }
							if (xhr.status == 400) {
								post.validationError = JSON.parse(xhr.responseText);
							}
							postEditLayout.triggerMethod("form:data:invalid", post.validationError);
						},
					});
				});
				$('.wysiwyg').wysiwyg({}, {
					uploadUrl: '/api/posts/'+post.id+'/upload',
					uploadDone: function(upload) {
						console.log(upload);
						post.uploads.add(upload);
					},
				});
			};

			if (id) $.when(loadPost(id, 'edit')).done(renderPost);
			else renderPost(new Post(Post.prototype.newDefaults));
		},
	}

	var app = new Backbone.Marionette.Application();

	app.addRegions({
		postRegion: "#app-content"
	});

	app.Router = Marionette.AppRouter.extend({
		appRoutes: {
			'new': 'editPost',
			'': 'listPosts',
			':id': 'showPost',
			':id/edit': 'editPost',
		}
	});

	app.addInitializer(function() {
		new app.Router({
			controller: controller,
		})
	});

	app.navigate = function(route, options){
		options || (options = {});
		Backbone.history.navigate(route, options);
	};

	app.getCurrentRoute = function(){
		return Backbone.history.fragment;
	};

	app.on("initialize:after", function(){
		if(Backbone.history)
			Backbone.history.start();
	});

	app.on("post:show", function(id){
		app.navigate(""+id);
		controller.showPost(id);
	});

	app.on("post:edit", function(id){
		app.navigate(""+id+"/edit");
		controller.editPost(id);
	});

	app.on("posts:list", function(id){
		app.navigate("");
		controller.listPosts();
	});

	$.when(getUser()).done(function(u){
		app.user = u;
		app.start();
	});
});
