from datetime import datetime

# from django.contrib.auth.models import User
from k33.accounts.models import MyUser as User
from django.template.response import TemplateResponse
from django.http import Http404
from django.db.models import Q
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_POST
from django.shortcuts import redirect, render, get_object_or_404
from django.core.urlresolvers import reverse
from django.template import Context, Template, loader
from django.http import HttpResponse, HttpResponseRedirect
from django.utils.timezone import utc

from django.forms.models import inlineformset_factory

from k33.posts.models import Post
from k33.votes.models import Vote
from k33.comments.models import Comment
from k33.votes.models import update_votes
# from k33.posts.forms import PostForm


import json


def filter_user_posts(user):
    posts = Post.objects.all()
    if user.is_anonymous():
        posts = posts.exclude(Q(published=False))  # |Q(private=True)
    else:
        # if not user.is_staff and not user.has_perm('private.view_private'):
        #     posts = posts.exclude(~Q(author=user), private=True)
        if not user.is_staff:
            posts = posts.exclude(~Q(author=user), published=False)
    return posts


def index(request):
    user = request.user
    posts = filter_user_posts(user)
    only_my = False
    if 'my' in request.GET:
        only_my = True
        if user.is_anonymous():
            from django.contrib.auth.views import redirect_to_login
            return redirect_to_login(request.path)
        posts = posts.filter(author=user)
    posts = posts.order_by('-updated_at')  # '-published_at',
    return render(request, 'posts/posts.html', locals())
    # params = { 'posts': Post.objects.all() } # 'user': user
    # return TemplateResponse(request,'posts/posts.html', params)
    # return TemplateResponse(request, 'posts/post.html')


def detail(request, post_id):
    user = request.user
    post = get_object_or_404(Post, id=post_id)
    if (not post.published and
            (not user.is_staff and user is not post.author)):
        raise Http404
    return render(request, 'posts/detail.html', locals())


@login_required
def edit(request, post_id):
    editor = request.user
    post = get_object_or_404(Post, id=post_id)

    if not editor.is_staff and post.author is not editor:
        raise Http404

    # if request.method == 'GET':

    if request.method == 'POST':
        data = {}
        if 'content' in request.POST:
            data['raw_content'] = request.POST['content']
        if 'title' in request.POST:
            data['title'] = request.POST['title']
        if 'tags' in request.POST:
            pass  # TODO!
        for f, v in data.items():
            setattr(post, f, v)
        post.save()

    return render(request, 'posts/post-edit.html', {'post': post})


@login_required
def new(request):
    poster = request.user
    if not poster.is_staff:
        raise Http404

    post = Post(author=poster)
    post.save()

    return redirect('posts:edit', post.id)


@require_POST
@login_required
def delete(request, post_id):
    deleter = request.user
    post = get_object_or_404(Post, id=post_id)

    if post.author is not deleter and not deleter.is_staff:
        raise Http404

    post.delete()
    return redirect('posts:list')


@require_POST
@login_required
def publish(request, post_id):
    publisher = request.user
    post = get_object_or_404(Post, id=post_id)

    if post.author is not publisher and not publisher.is_staff:
        raise Http404

    post.publish()
    return redirect('posts:list')


@require_POST
@login_required
def unpublish(request, post_id):
    publisher = request.user
    post = get_object_or_404(Post, id=post_id)

    if post.author is not publisher and not publisher.is_staff:
        raise Http404

    post.unpublish()
    return redirect('posts:list')


# @login_required
# def my(request):
#     user = request.user
#     posts = Post.objects.filter(author=user)
#     return render(request, 'posts/posts.html', locals())
# @login_required
# def edit(request, post_id=None):
# 	user = request.user
# 	if post_id:
# 		try:
# 			post = Post.objects.get(id=post_id)
# 		except ObjectDoesNotExist:
# 			post = Post(author=user)
# 	else:
# 		post = Post(author=user)

# 	VotesFormSet = inlineformset_factory(Post, Vote, fk_name='voted')

# 	if request.method == 'POST': # If the form has been submitted...
# 		post_form = PostForm(request.POST, instance=post) # A form bound to the POST data
# 		post_votes_formset = VotesFormSet(request.POST, instance=post)

# 		if post_form.is_valid() and post_votes_formset.is_valid(): # All validation rules pass
# 			# TODO
# 			# Process the data in form.cleaned_data
# 			# ...
# 			return HttpResponseRedirect('/thanks/') # Redirect after POST
# 	else:
# 		post_form = PostForm(instance=post) # An unbound form
# 		post_votes_formset = VotesFormSet(instance=post)

# 	return render(request, 'posts/edit_post2.html', {
# 		'post_form': post_form, 
# 		'post_votes_formset': post_votes_formset
# 	})

    # post = None
    # try:
    # 	post = Post.objects.get(id=post_id)
    # except ObjectDoesNotExist:
    # 	pass
    # params = { 'post': post }
    # return TemplateResponse(request,'posts/post-edit.html', params)


# @login_required
# def edit2(request, post_id=None):
# 	user = request.user
# 	if post_id:
# 		try:
# 			post = Post.objects.get(id=post_id)
# 		except ObjectDoesNotExist:
# 			post = Post(author=user)
# 	else:
# 		post = Post(author=user)
# 	# params = 
# 	return render(request,'posts/post-edit.html', { 'post': post })


# @login_required
# def save(request):
# 	if request.method == 'POST':
# 		user = request.user
# 		post_data = json.loads(request.POST['data'])
# 		print(post_data)
# 		isnew = False
# 		try:
# 			post = Post.objects.get(id=post_data['id'])
# 		except (ObjectDoesNotExist, ValueError, KeyError):
# 			post = Post(author=user)
# 			isnew = True
# 		if 'title' in post_data:
# 			post.title = post_data['title']
# 		if 'content' in post_data:
# 			post.raw_content = post_data['content']
# 		post.save()
# 		if 'votes' in post_data:
# 			update_votes(post, post_data['votes'])#TODO trim, check, etc...
# 		# if request.is_ajax():
# 		updates = {}
# 		if isnew: updates['id'] = post.id

# 		notifications = {}
# 		res = {
# 			'updates': updates,
# 			'notifications': notifications
# 		}
# 		return HttpResponse(json.dumps(res), mimetype="application/json")
# 		# else:
# 			# return redirect('/posts/edit/%s/' % post_id)


# 		# post_id = request.POST['post_id']
# 		# print(request.POST['votes']) 

# 		# post = None
# 		# try:
# 		# 	post = Post.objects.get(id=post_id)
# 		# except (ObjectDoesNotExist, ValueError):
# 		# 	post = Post(author=user)
# 		# 	post.published_at = datetime.utcnow().replace(tzinfo=utc)
# 		# post.title = request.POST['post_title']
# 		# post.content = request.POST['post_content']
# 		# post.save()
# 		# votes_data = json.loads(request.POST['votes'])
# 		# update_votes(post, votes_data)#TODO trim, check, etc...
# 		# post_id = post.id
# 		# if request.is_ajax():
# 		# 	return HttpResponse(json.dumps(post_id), mimetype="application/json")
# 		# else:
# 		# 	return redirect('/posts/edit/%s/' % post_id)


# @login_required
# def delete(request):
# 	user = request.user

# 	success = False
# 	if request.method == 'POST':
# 		post_data = json.loads(request.POST['data'])
# 		post_id = post_data['id']
# 		post = None
# 		try:
# 			post = Post.objects.get(id=post_id)
# 			post.delete()
# 		except (ObjectDoesNotExist, ValueError):
# 			pass
# 		success = True

# 		if request.is_ajax():
# 			return HttpResponse()
# 		else:
# 			return redirect('/posts/')