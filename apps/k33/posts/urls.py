from django.conf.urls import patterns, include, url
from rest_framework import routers
from . import views

urlpatterns = patterns(
    'k33.posts.views',
    url(r'^$', views.index, name='list'),
    url(r'^(\d+)$', views.detail, name='detail'),
    url(r'^new$', views.new, name='new'),
    url(r'^(\d+)/edit$', views.edit, name='edit'),
    url(r'^(\d+)/delete', views.delete, name='delete'),
    url(r'^(\d+)/publish', views.publish, name='publish'),
    url(r'^(\d+)/unpublish', views.unpublish, name='unpublish')
)
