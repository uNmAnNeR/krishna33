from rest_framework import permissions


class IsAuthorOrAdmin(permissions.BasePermission):

	def has_object_permission(self, request, view, obj):
		# проверить не надо ли еще тут проверять на private
		if request.method in permissions.SAFE_METHODS:
			return True
		return request.user.is_staff or obj.author == request.user
