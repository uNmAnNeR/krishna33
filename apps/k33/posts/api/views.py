import os

from django.db.models import Q
from django.http import HttpResponse, HttpResponseBadRequest
from django.core.files.base import File

from rest_framework import viewsets  # generics
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from k33.uploads.api.serializers import UploadSerializer
from k33.uploads.models import Upload

from k33.posts.models import Post, POSTS_MEDIA_DIR
from k33.posts.api.serializers import PostSerializer
from k33.posts.api.permissions import IsAuthorOrAdmin


class PostViewSet(viewsets.ModelViewSet):
    model = Post
    serializer_class = PostSerializer
    permission_classes = [IsAuthenticated, IsAuthorOrAdmin]

    def get_queryset(self):
        user = self.request.user
        print(user)
        queryset = Post.objects.all()
        if not user.is_staff and not user.has_perm('private.view_private'):
            queryset = queryset.exclude(~Q(author=user), private=True)
        if not user.is_staff:
            queryset = queryset.exclude(~Q(author=user), published=False)
        return queryset

    def get_serializer_class(self):
        # local copy to add filter_fields
        class Serializer(self.serializer_class):
            pass

        if 'fields' in self.request.QUERY_PARAMS:
            Serializer.filter_fields = self.request.QUERY_PARAMS.getlist('fields')
        return Serializer

    def pre_save(self, obj):
        if self.request.method == 'POST':
            obj.author = self.request.user

    @action(methods=['POST', 'PUT'], permission_classes=[IsAuthorOrAdmin])
    def publish(self, request, pk=None):
        post = self.get_object()
        post.publish()
        return Response({'published': post.published})

    @action(methods=['POST', 'PUT'], permission_classes=[IsAuthorOrAdmin])
    def unpublish(self, request, pk=None):
        post = self.get_object()
        post.unpublish()
        return Response({'published': post.published})

    @action(methods=['POST'], permission_classes=[IsAuthorOrAdmin])
    def upload(self, request, pk=None):
        try:
            user = request.user
            post = self.get_object()
            print('POST!', post)
        except Exception as e:
            print(e)

        if request.FILES and 'file' in request.FILES:
            uploaded_file = request.FILES['file']
            relpath = os.path.join(
                POSTS_MEDIA_DIR, str(post.id), uploaded_file.name)
            upf = File(uploaded_file)
            upf.name = relpath
            u = Upload(user=user, file=upf, container=post)
            u.save()
            print(UploadSerializer(u).data)
            return Response(UploadSerializer(u).data)

