from rest_framework import routers
from k33.posts.api.views import PostViewSet

router = routers.SimpleRouter(trailing_slash=False)
router.register(r'posts', PostViewSet)
urlpatterns = router.urls
