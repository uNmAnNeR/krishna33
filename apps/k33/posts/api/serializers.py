from rest_framework import serializers
from k33.posts.models import Post
from k33.accounts.api.serializers import AccountSerializer
from k33.uploads.api.serializers import UploadSerializer
from k33.common.api.serializers import AllowedFieldsModelSerializer

class PostSerializer(AllowedFieldsModelSerializer):

    author = AccountSerializer(read_only=True)
    uploads = UploadSerializer(many=True, read_only=True)
    # работает только так
    private = serializers.BooleanField()
    cut_content = serializers.CharField(read_only=True)

    class Meta:
        model = Post
        fields = ('id', 'private', 'author', 'title', 'uploads', 'raw_content', 'cut_content', 
            'rendered_content', 'published', 'publish_date', 'modify_date')
        read_only_fields = ('id', 'rendered_content', 'published', 
            'publish_date', 'modify_date')

    def validate_title(self, attrs, source):
        title = attrs[source]
        if len(title) < Post.TITLE_MIN_LENGHT:
            raise serializers.ValidationError(
                'Название не может быть короче {} символов'.format(Post.TITLE_MIN_LENGHT))
        elif Post.TITLE_MAX_LENGHT < len(title):
            raise serializers.ValidationError(
                'Название не может быть длиннее {} символов'.format(Post.TITLE_MAX_LENGHT))
        return attrs