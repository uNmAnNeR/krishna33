from k33.posts.models import Post
from django.contrib import admin

class PostAdmin(admin.ModelAdmin):
    list_display = ['author', 'title', 'published_at']
    exclude = ('rendered_content', 'published_at', 'published')

admin.site.register(Post, PostAdmin)