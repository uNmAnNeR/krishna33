# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Taggable'
        db.create_table('tags_taggable', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal('tags', ['Taggable'])

        # Adding model 'Tag'
        db.create_table('tags_tag', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('tagged', self.gf('django.db.models.fields.related.ForeignKey')(related_name='tags', to=orm['tags.Taggable'])),
            ('text', self.gf('django.db.models.fields.CharField')(max_length=15)),
        ))
        db.send_create_signal('tags', ['Tag'])


    def backwards(self, orm):
        # Deleting model 'Taggable'
        db.delete_table('tags_taggable')

        # Deleting model 'Tag'
        db.delete_table('tags_tag')


    models = {
        'tags.tag': {
            'Meta': {'object_name': 'Tag'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tagged': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'tags'", 'to': "orm['tags.Taggable']"}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '15'})
        },
        'tags.taggable': {
            'Meta': {'object_name': 'Taggable'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['tags']