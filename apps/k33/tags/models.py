from django.db import models
from django.contrib import admin
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned

class Taggable(models.Model):
	pass

class Tag(models.Model):
	tagged = models.ForeignKey(Taggable, related_name='tags')
	text = models.CharField(max_length=15) 
	
	def __str__(self):
		return "{}: {}".format(str(self.tagged), self.text)

class TagAdmin(admin.ModelAdmin):
	list_display = ['tagged', 'text']

admin.site.register(Tag, TagAdmin)