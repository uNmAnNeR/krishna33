from django.db import models

class Privatable(models.Model):
	private = models.BooleanField(default=True)

	class Meta:
		permissions = (
			("view_private", "Can see private content"),
		)