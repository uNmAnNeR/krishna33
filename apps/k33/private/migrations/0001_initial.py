# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Privatable'
        db.create_table('private_privatable', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('private', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal('private', ['Privatable'])


    def backwards(self, orm):
        # Deleting model 'Privatable'
        db.delete_table('private_privatable')


    models = {
        'private.privatable': {
            'Meta': {'object_name': 'Privatable'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'private': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        }
    }

    complete_apps = ['private']