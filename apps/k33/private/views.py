from datetime import datetime

from django.contrib.auth.models import User
from django.template.response import TemplateResponse
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from django.core.urlresolvers import reverse
from django.template import Context, Template, loader
from django.http import HttpResponse
from django.utils.timezone import utc

import json
