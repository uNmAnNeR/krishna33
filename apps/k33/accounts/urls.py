from django.conf.urls import patterns, include, url

from django.contrib.auth.views import login, logout

urlpatterns = patterns('',
    url(r'^login/$',  login, {'template_name': 'accounts/login.html'}, name='login'),
    url(r'^logout/$', logout, {'template_name': 'accounts/logout.html'}, name='logout'),
    url(r'^register/$', 'k33.accounts.views.register', name='register'),
)
