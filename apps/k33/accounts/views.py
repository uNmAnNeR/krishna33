from django.shortcuts import redirect, render, render_to_response
from django.forms.util import ErrorList
from django.forms.forms import NON_FIELD_ERRORS
from django.contrib.auth import authenticate, login


from k33.accounts.admin import UserCreationForm
from krishna33.settings import RECAPTCHA_PRIVATE_KEY, RECAPTCHA_PUBLIC_KEY
from recaptcha.client import captcha
from utils import get_client_ip

def register(request):
    if request.method == 'POST': # If the form has been submitted...
        form = UserCreationForm(request.POST) # A form bound to the POST data

        if form.is_valid(): # All validation rules pass
            # return redirect('/accounts/register/')
            recaptcha_response = captcha.submit(
                request.POST['recaptcha_challenge_field'],
                request.POST['recaptcha_response_field'],
                RECAPTCHA_PRIVATE_KEY,
                get_client_ip(request),
            )
            if not recaptcha_response.is_valid:
                errors = form._errors.setdefault(NON_FIELD_ERRORS, ErrorList())
                errors.append('Код подтверждения введен неверно.')
                return render(request, 'accounts/register.html', {'form': form,
                            'recaptcha_html': captcha.displayhtml(RECAPTCHA_PUBLIC_KEY)})

            # Process the data in form.cleaned_data
            # ...
            form.save()
            # login new user
            print(form)
            new_user = authenticate(username=form.data['email'],
                password=form.data['password1'])
            login(request, new_user)
            return redirect('/') # Redirect after POST
    else:
        form = UserCreationForm() # An unbound form
    return render(request, 'accounts/register.html', {
        'form': form,
        'recaptcha_html': captcha.displayhtml(RECAPTCHA_PUBLIC_KEY)
    })

