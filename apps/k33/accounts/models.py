from django.db import models
from django.contrib.auth.models import Permission
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser, PermissionsMixin
)

class MyUserManager(BaseUserManager):
    def create_user(self, email, name, password=None):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        if not name:
            raise ValueError('Users must have a name')

        user = self.model(
            email=self.normalize_email(email),
            name=name,
            is_superuser=False
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, name, password):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(email, name, password=password)
        user.is_staff = True
        user.is_superuser = True
        # TODO not unique:
        # view_private = Permission.objects.get(codename='view_private')
        # user.user_permissions.add(view_private)
        user.save(using=self._db)
        return user


class MyUser(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(
        verbose_name='адрес почты',
        max_length=255,
        unique=True,
        db_index=True,
    )
    name = models.CharField(verbose_name='Имя пользователя', max_length=64, unique=True)
    # has_private_access = models.BooleanField(default=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    owned = models.BooleanField(default=False)

    objects = MyUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['name']

    def get_full_name(self):
        # The user is identified by their email address
        return '%s [%s]' % (self.name, self.email)

    def get_short_name(self):
        # The user is identified by their email address
        return self.name

    def __str__(self):
        return self.get_full_name()

    # def has_perm(self, perm, obj=None):
    # 	"Does the user have a specific permission?"
    # 	# Simplest possible answer: Yes, always
    # 	return True

    # def has_module_perms(self, app_label):
    # 	"Does the user have permissions to view the app 'app_label'?"
    # 	# Simplest possible answer: Yes, always
    # 	return True

    # @property
    # def is_staff(self):
    # 	"Is the user a member of staff?"
    # 	# Simplest possible answer: All admins are staff
    # 	return self.is_staff