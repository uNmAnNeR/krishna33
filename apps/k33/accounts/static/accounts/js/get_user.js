function getUser() {
	var d = new $.Deferred();
	// if (localStorage['me']) {
	// 	d.resolve(new Account(JSON.parse(localStorage['me'])));
	// } else {
		$.get('/api/accounts/me/', function(data) {
			// localStorage['me'] = JSON.stringify(data);
			d.resolve(new Account(data));
		});
	// }
	return d.promise();
}