from django.shortcuts import get_object_or_404

from rest_framework import viewsets  # generics
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated


from django.contrib.auth import get_user_model
from k33.accounts.api.serializers import AccountSerializer
# from k33.posts.api.permissions import IsAuthorOrAdmin


# class SnippetList(generics.ListCreateAPIView):
# 	queryset = Post.objects.all()
# 	serializer_class = PostSerializer


# class SnippetDetail(generics.RetrieveUpdateDestroyAPIView):
# 	queryset = Post.objects.all()
# 	serializer_class = PostSerializer


class AccountViewSet(viewsets.ModelViewSet):
	model = get_user_model()
	serializer_class = AccountSerializer
	# permission_classes = [IsAuthorOrAdmin]

	def get_queryset(self):
		user = self.request.user
		queryset = get_user_model().objects.all()
		if not user.is_staff:
			queryset = queryset.filter(id=user.id)
		return queryset


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def me(request):
	user = get_object_or_404(get_user_model(), id=request.user.id)
	user_data = AccountSerializer(user).data
	if user.is_staff:
		user_data['is_staff'] = True
	return Response(user_data)
