from rest_framework import serializers
from django.contrib.auth import get_user_model


class SimpleAccountSerializer(serializers.ModelSerializer):

    class Meta:
        model = get_user_model()
        fields = ('id',)
        read_only_fields = ('id',)


class AccountSerializer(serializers.ModelSerializer):

    class Meta:
        model = get_user_model()
        fields = ('id', 'email')
        read_only_fields = ('id', 'email')
