from django.conf.urls import patterns, url

from rest_framework import routers
from k33.accounts.api.views import AccountViewSet

router = routers.SimpleRouter(trailing_slash=False)
router.register(r'accounts', AccountViewSet)
urlpatterns = router.urls
urlpatterns += patterns('',
    url(r'^accounts/me/$', 'k33.accounts.api.views.me'),
)
