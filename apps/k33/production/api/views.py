from django.db.models import Q

from rest_framework import viewsets  # generics
from rest_framework.decorators import api_view, permission_classes, action
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from .serializers import DynamicProductSerializer
from ..models import Product
from k33.common.api.permissions import IsStaffOrReadOnly


class ProductViewSet(viewsets.ModelViewSet):
    model = Product
    serializer_class = DynamicProductSerializer
    permission_classes = [IsStaffOrReadOnly]
    queryset = Product.objects.all().select_subclasses()


    # def get_queryset(self):
    #     user = self.request.user
    #     queryset = Product.objects.all().select_subclasses()
    #     # TODO privileges
    #     if not user.is_staff:
    #         queryset = queryset.exclude(
    #             ~Q(supplier=user) & ~Q(customer=user), published=False)
    #     return queryset

#     @action(methods=['POST', 'PUT'], permission_classes=[CanCancelOperationOrAdmin])
#     def cancel(self, request, pk=None):
#         op = self.get_object()
#         op.cancel()
#         return Response({'canceled': op.canceled})


# @api_view(['GET'])
# @permission_classes([IsAuthenticated])
# def handlers(request):
#     hs = [{'id': id_,
#            'name': h.name}
#           for id_, h in Operation._handlers.items()]
#     return Response(hs)
