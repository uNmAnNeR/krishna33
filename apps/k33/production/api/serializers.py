from rest_framework import serializers
from k33.common.api.serializers import DynamicModelSerializer
from ..models import Product, Book


class ProductSerializer(serializers.ModelSerializer):
    type_name = serializers.CharField(max_length=200, required=False) # only on create
    primary_image_url = serializers.URLField(read_only=True)

    class Meta:
        model = Product
        fields = ('id', 'type_name', 'title', 'abbreviation', 'description', 
            'upc', 'category', 'primary_image_url',)
        read_only_fields = ('id', )


class BookSerializer(serializers.ModelSerializer):
    type_name = serializers.CharField(max_length=200, required=False) # only on create
    primary_image_url = serializers.URLField(read_only=True)

    class Meta:
        model = Book
        fields = ('id', 'type_name', 'title', 'abbreviation', 'description', 'upc', 
            'category', 'num_pages', 'authors', 'short_description', 'publication_year', 
            'edition', 'isbn', 'cover', 'primary_image_url',)
        read_only_fields = ('id', )


class DynamicProductSerializer(DynamicModelSerializer):

    class Meta:
        types_map = {
            Product: ProductSerializer,
            Book: BookSerializer,
        }
