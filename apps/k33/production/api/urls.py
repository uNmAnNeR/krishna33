from django.conf.urls import patterns, url, include

from rest_framework import routers
from .views import ProductViewSet

router = routers.SimpleRouter(trailing_slash=False)
router.register(r'products', ProductViewSet)
urlpatterns = router.urls
