from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.contenttypes import generic

from mptt.models import MPTTModel, TreeForeignKey

from k33.common.mixins import CreatedUpdatedAtMixin, ActivatableMixin, Hidable
from k33.common.api.mixins import DynamicModelMixin
from k33.common.models import Image


class Category(MPTTModel):
    name = models.CharField(_('Name'), max_length=255)
    description = models.TextField(_('Description'), null=True, blank=True)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children')

    @property
    def full_path(self):
        return (self.parent.full_path if self.parent else '') + '/' + self.name

    def __str__(self):
        return self.full_path

# TODO ! сделать чтобы модель в post_save записывалась по id если объект еще не создан!
# и чтобы при удалении картинки, удалялся с диска!
def product_image_upload_path(instance, filename):
    # print(instance)
    if not instance.id:
        raise AttributeError('Сохраните сначала модель, потом заливайте картинки!')
    return 'products/'+str(instance.id)+'/'+filename


class Product(DynamicModelMixin, CreatedUpdatedAtMixin, ActivatableMixin, Hidable):
    title = models.CharField(_('Title'), max_length=255)
    abbreviation = models.CharField(_('Abbreviation'), max_length=31, null=True, blank=True)
    description = models.TextField(_('Description'), null=True, blank=True)
    upc = models.CharField(_("UPC"), max_length=64, blank=True, null=True)
    category = models.ForeignKey(Category, null=True, blank=True, related_name='products')

    primary_image = models.ImageField(_('Primary image'), upload_to=product_image_upload_path, null=True, blank=True)
    images = generic.GenericRelation(Image)

    @property
    def primary_image_url(self):
        if self.primary_image:
            return self.primary_image.url
        return ''

    def __str__(self):
        return (self.title +
            ' ('+(self.category.full_path if self.category else '<No category>')+')')


class Author(models.Model):
    name = models.CharField(_('name'), max_length=255)

    def __str__(self):
        return self.name

class Book(Product):
    num_pages = models.PositiveSmallIntegerField(_('Number of pages'), null=True, blank=True)
    # authors = models.CharField(_('Authors'), max_length=255)
    authors = models.ManyToManyField(Author)
    short_description = models.TextField(_('Short description'), null=True, blank=True)
    # price = models.DecimalField(_('Price'), max_digits=12, decimal_places=3)
    publication_year = models.PositiveSmallIntegerField(_('Publication year'), null=True, blank=True)
    edition = models.PositiveSmallIntegerField(_('Edition'), null=True, blank=True)
    isbn = models.CharField(_('ISBN'), max_length=13, null=True, blank=True)
    cover = models.CharField(_('Cover'), max_length=100, null=True, blank=True)

    def __str__(self):
        return (self.title +
            (' '+str(self.edition)+'-ed.' if self.edition else '') +
            ' ('+(self.category.full_path if self.category else '<No category>')+')')



