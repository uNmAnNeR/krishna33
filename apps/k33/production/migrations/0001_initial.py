# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Category'
        db.create_table('production_category', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True, null=True)),
            ('parent', self.gf('mptt.fields.TreeForeignKey')(blank=True, to=orm['production.Category'], related_name='children', null=True)),
            ('lft', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            ('rght', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            ('tree_id', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            ('level', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
        ))
        db.send_create_signal('production', ['Category'])

        # Adding model 'Product'
        db.create_table('production_product', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(blank=True, auto_now_add=True)),
            ('updated_at', self.gf('django.db.models.fields.DateTimeField')(blank=True, auto_now=True)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('hidden', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('hidden_at', self.gf('django.db.models.fields.DateTimeField')(blank=True, null=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('abbreviation', self.gf('django.db.models.fields.CharField')(blank=True, max_length=31, null=True)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True, null=True)),
            ('upc', self.gf('django.db.models.fields.CharField')(blank=True, unique=True, max_length=64, null=True)),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, to=orm['production.Category'], related_name='products', null=True)),
            ('primary_image', self.gf('django.db.models.fields.files.ImageField')(blank=True, max_length=100, null=True)),
        ))
        db.send_create_signal('production', ['Product'])

        # Adding model 'Book'
        db.create_table('production_book', (
            ('product_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['production.Product'], unique=True, primary_key=True)),
            ('num_pages', self.gf('django.db.models.fields.PositiveSmallIntegerField')(blank=True, null=True)),
            ('authors', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('short_description', self.gf('django.db.models.fields.TextField')(blank=True, null=True)),
            ('publication_year', self.gf('django.db.models.fields.PositiveSmallIntegerField')(blank=True, null=True)),
            ('edition', self.gf('django.db.models.fields.PositiveSmallIntegerField')(blank=True, null=True)),
            ('isbn', self.gf('django.db.models.fields.CharField')(blank=True, unique=True, max_length=13, null=True)),
            ('cover', self.gf('django.db.models.fields.CharField')(blank=True, max_length=100, null=True)),
        ))
        db.send_create_signal('production', ['Book'])


    def backwards(self, orm):
        # Deleting model 'Category'
        db.delete_table('production_category')

        # Deleting model 'Product'
        db.delete_table('production_product')

        # Deleting model 'Book'
        db.delete_table('production_book')


    models = {
        'production.book': {
            'Meta': {'object_name': 'Book', '_ormbases': ['production.Product']},
            'authors': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'cover': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '100', 'null': 'True'}),
            'edition': ('django.db.models.fields.PositiveSmallIntegerField', [], {'blank': 'True', 'null': 'True'}),
            'isbn': ('django.db.models.fields.CharField', [], {'blank': 'True', 'unique': 'True', 'max_length': '13', 'null': 'True'}),
            'num_pages': ('django.db.models.fields.PositiveSmallIntegerField', [], {'blank': 'True', 'null': 'True'}),
            'product_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['production.Product']", 'unique': 'True', 'primary_key': 'True'}),
            'publication_year': ('django.db.models.fields.PositiveSmallIntegerField', [], {'blank': 'True', 'null': 'True'}),
            'short_description': ('django.db.models.fields.TextField', [], {'blank': 'True', 'null': 'True'})
        },
        'production.category': {
            'Meta': {'object_name': 'Category'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'to': "orm['production.Category']", 'related_name': "'children'", 'null': 'True'}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        'production.product': {
            'Meta': {'object_name': 'Product'},
            'abbreviation': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '31', 'null': 'True'}),
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'to': "orm['production.Category']", 'related_name': "'products'", 'null': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True', 'null': 'True'}),
            'hidden': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'hidden_at': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'primary_image': ('django.db.models.fields.files.ImageField', [], {'blank': 'True', 'max_length': '100', 'null': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'upc': ('django.db.models.fields.CharField', [], {'blank': 'True', 'unique': 'True', 'max_length': '64', 'null': 'True'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'})
        }
    }

    complete_apps = ['production']