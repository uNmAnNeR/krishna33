from django.contrib import admin
from django.contrib.contenttypes import generic

from .models import Product, Book, Category, Author
from k33.common.models import Image

class ImageInline(generic.GenericTabularInline):
    model = Image

class ProductAdmin(admin.ModelAdmin):
    inlines = [
        ImageInline,
    ]
    exclude = ['active', 'hidden', 'hidden_at']

class BookAdmin(admin.ModelAdmin):
    exclude = ['active', 'hidden', 'hidden_at']

admin.site.register(Product, ProductAdmin)
admin.site.register(Book, BookAdmin)
admin.site.register(Category)
admin.site.register(Author)
