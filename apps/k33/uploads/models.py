from django.db import models
from django.contrib import admin
# from django.contrib.auth.models import User
# from k33.accounts.models import MyUser as User
from django.contrib.auth import get_user_model


def make_upload_path(instance, filename):
    return filename


class ContainsUploads(models.Model):
    pass


class Upload(models.Model):
    user = models.ForeignKey(get_user_model())
    file = models.FileField(upload_to=make_upload_path)
    date = models.DateTimeField(auto_now_add=True)  # TODO mixin createdAt
    container = models.ForeignKey(ContainsUploads, related_name='uploads',
                                  null=True, blank=True)

    def __str__(self):
        return '{}: {}'.format(self.user, self.file.name)

    @property
    def url(self):
        if self.file:
            return self.file.url
        return ''

    # def delete(self, *args, **kwargs):
    #     print('delete called')
    #     super().delete(*args, **kwargs)


from django.db.models.signals import post_delete
from django.dispatch import receiver


@receiver(post_delete, sender=Upload)
def upload_file_delete_handler(sender, **kwargs):
    upload = kwargs['instance']
    if upload.file:
        try:
            upload.file.delete(save=False)
        except:
            pass


class UploadAdmin(admin.ModelAdmin):
    pass

admin.site.register(Upload, UploadAdmin)
