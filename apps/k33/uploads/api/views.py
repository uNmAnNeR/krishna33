# import os

# from django.db.models import Q
# from django.http import HttpResponse, HttpResponseBadRequest
# from django.core.files.base import File

from rest_framework import viewsets  # generics
# from rest_framework.decorators import action
# from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

# from k33.posts.models import Post, POSTS_MEDIA_DIR
from k33.uploads.models import Upload
from k33.uploads.api.serializers import UploadSerializer
from k33.uploads.api.permissions import IsAuthorOrAdmin

class UploadViewSet(viewsets.ModelViewSet):
    model = Upload
    serializer_class = UploadSerializer
    permission_classes = [IsAuthenticated, IsAuthorOrAdmin]

    # def get_queryset(self):
    #     user = self.request.user
    #     queryset = Upload.objects.all()
    #     return queryset

    def get_serializer_class(self):
        # local copy to add filter_fields
        class Serializer(UploadSerializer):
            pass

        if 'fields' in self.request.GET:
            Serializer.filter_fields = self.request.GET.getlist('fields')
        return Serializer

    def pre_save(self, obj):
        if self.request.method == 'POST':
            obj.user = self.request.user
