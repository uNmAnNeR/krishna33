from rest_framework import serializers
from k33.uploads.models import Upload
from k33.accounts.api.serializers import AccountSerializer
from k33.common.api.serializers import AllowedFieldsModelSerializer

class UploadSerializer(AllowedFieldsModelSerializer):

    # user = AccountSerializer(read_only=True)
    # работает только так
    url = serializers.URLField(read_only=True)
    # cut_content = serializers.CharField(read_only=True)

    class Meta:
        model = Upload
        fields = ('id', 'date', 'url', ) #'user', 
        read_only_fields = ('id', 'date', )
