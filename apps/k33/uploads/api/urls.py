from rest_framework import routers
from k33.uploads.api.views import UploadViewSet

router = routers.SimpleRouter(trailing_slash=False)
router.register(r'uploads', UploadViewSet)
urlpatterns = router.urls
