var app = app || {};

(function ($) {
	'use strict';

	// Todo Item View
	// --------------

	// The DOM element for a todo item...
	app.CategoryContentView = Backbone.View.extend({

		tagName: 'li',
		template: _.template($('#category-content-template').html()),

		events: {},

		initialize: function () {
			// this.listenTo(this.model, 'change', this.render);
			this.listenTo(this.model, 'destroy', this.remove);

			this.setCategoryView('show');
			// this.listenTo(this, 'categoryViewUpdated', this.renderCategoryView);			
			this.materials = new app.MaterialList();
		},

		render: function () {
			this.$el.html(this.template(this.model.toJSON()));
			this.renderCategoryView();
			this.updateMaterials();
			return this;
		},

		setCategoryView: function(state) {
			// states: ['show', 'edit']
			// if (this.categoryView) 
			// 	this.categoryView.remove();
			if (state == 'show') {
				if (this.categoryEditView)
					this.categoryEditView.remove();
				if (!this.categoryView) {
					this.categoryView = new app.CategoryView({ model: this.model });
					this.listenTo(this.categoryView, 'edit', _.bind(this.setCategoryView, this,  'edit'));
					this.listenTo(this.categoryView, 'newMaterialSaved', this.addMaterial);
				} else {
					this.categoryView.$el.show();
					// вызов для обновления остояния
					this.renderCategoryView();					
				}
			} else if (state == 'edit') {
				this.categoryView.$el.hide();
				this.categoryEditView = new app.CategoryEditView({ model: this.model });
				this.listenTo(this.categoryEditView, 'editStopped', _.bind(this.setCategoryView, this,  'show'));
				this.$el.prepend(this.categoryEditView.render().el);
			}
			// this.categoryView.parentView = this;

			// this.trigger('categoryViewUpdated');
		},

		renderCategoryView: function() {
			this.$el.prepend(this.categoryView.render().el);
		},

		// newMaterial: function() {
		// 	this.newMaterialView = new app.MaterialEditView({model: new app.Material()});
		// 	this.newMaterialView.model.set('category_id', this.model.id, {silent: true});

		// 	this.listenTo(this.newMaterialView, 'saved', this.addMaterial);
		// 	// this.listenTo(this.newMaterialView, 'editStopped', this.newMaterialViewClose);
		// 	this.$('.category').after(this.newMaterialView.render().el);
		// },

		// newMaterialViewClose: function() {
		// 	this.newMaterialView.remove();
		// },

		// // Switch this view into `"editing"` mode, displaying the input field.
		// edit: function () {
		// 	// this.state = 'edit';
		// 	this.categoryView.remove();
		// 	this.categoryView = new app.CategoryEditView({ model: this.model });
		// 	this.categoryView.parentView = this;

		// 	this.listenToOnce(editView, 'editStopped', this.stopEdit);
		// 	this.$el.prepend(this.categoryView.render().el);
		// },

		// stopEdit: function() {
		// 	this.$('.category-edit').remove();
		// 	this.$('.category', this.$el).show();	
		// },


		clear: function () {
			this.model.destroy();
		},


		addMaterial: function(material) {
			// console.log('add Mat');
			var view = new app.MaterialView({ model: material });
			// console.log(material);
			this.$('.materials').append(view.render().el);
		},

		renderMaterials: function() {
			this.$('.materials').empty();
			this.materials.each(this.addMaterial, this);
		},

		updateMaterials: function() {
			var view = this;
			this.materials.fetch({
				data: { 
					category_id: view.model.id 
				},
				success: function() {
					view.renderMaterials();
				}
			});
		},

	});


	app.CategoryView = Backbone.View.extend({
		tagName: 'div',
		className: 'category',
		template: _.template($('#category-template').html()),

		events: {
			'click .destroy-category': 'destroy',
			'click .edit-category': 'edit',
			'click .add-material': 'newMaterial',
		},

		initialize: function () {
			this.listenTo(this.model, 'change', this.render);
			this.listenTo(this.model, 'destroy', this.remove);
		},

		render: function () {
			this.$el.html(this.template(this.model.toJSON()));
			return this;
		},

		newMaterial: function() {
			var newMaterialView = new app.MaterialEditView({model: new app.Material()});
			newMaterialView.model.set('category_id', this.model.id, {silent: true});

			this.listenTo(newMaterialView, 'saved', this.newMaterialSaved);
			this.listenTo(newMaterialView, 'editStopped', this.newMaterialEditStopped);
			this.$el.after(newMaterialView.render().el);
		},

		newMaterialEditStopped: function(mv) { 
			// console.log(mv); //, $(ev.currentTarget), $(ev.target)
			mv.remove();
		},
		newMaterialSaved: function(material) { this.trigger('newMaterialSaved', material); },
		edit: function () {	this.trigger('edit'); },
		destroy: function () { 
			if (!confirm("Удалить?")) return;
			this.model.destroy(); 
		},

	});


	app.CategoryEditView = Backbone.View.extend({
		template: _.template($('#category-edit-template').html()),
		errorTemplate: _.template($('#error-template').html()),
		tagName: 'div',
		className: 'category-edit',

		// The DOM events specific to an item.
		events: {
			'click .confirm-category': 'save',
			'click .discard-category': 'discard',
			'click .toggle-private': 'togglePrivate',
		},

		initialize: function () {
			this.listenTo(this.model, 'change', this.render);
			this.listenTo(this.model, 'destroy', this.remove);
		},

		render: function() {
			this.$el.html(this.template(this.model.toJSON()));
			return this;
		},

		discard: function () {
			this.model.fetch();
			this.trigger('discarded');
			this.trigger('editStopped');
		},

		togglePrivate: function() {
			// делаем тихо, чтобы остальные поля не сбрасывались
			this.model.set('private', !this.model.get('private'), {silent:true});
			var toggler = $('.toggle-private', this.$el);
			if (this.model.get('private')) {
				toggler.removeClass('glyphicon-eye-open');
				toggler.addClass('glyphicon-eye-close');
			} else {
				toggler.removeClass('glyphicon-eye-close');
				toggler.addClass('glyphicon-eye-open');
			}
			// this.render();
		},

		save: function () {
			// clear errors
			$('.error', this.$el).remove();

			var name = $('input[name="name"]', this.$el).val().trim();
			// var private = $('input[name="private"]', this.$el).prop("checked");

			var view = this;
			// this.model.set('name', name, {validate: true});
			this.model.save({ 'name': $.trim(name), 'private': this.model.get('private') }, {
				success: function(model, response, options) {
					// console.log('SUCCESS!!!');
					if (response) {
						//set id
						view.model.set('id', response.id, {silent:true})
					}
					view.trigger('saved', view.model);
					view.trigger('editStopped');
				},
				error: function(model, xhr, options) {
					console.log(model, xhr, options);
					var status = xhr.status;
					if (status == 406) {
						var errors = JSON.parse(xhr.responseText);
						_.forEach(['name', 'private'], function(field){
							var fieldEl = $('input[name="'+field+'"]', view.$el);
							_.forEach(errors[field], function(error){
								$(view.errorTemplate({error: error})).insertBefore(fieldEl);
							});
						});
					}
				},
			});
		},
	});





	app.MaterialView = Backbone.View.extend({
		//... is a list tag.
		tagName: 'li',
		className: 'material',
		template: _.template($('#material-template').html()),

		events: {
			'click .destroy-material': 'clear',
			'click .edit-material': 'edit',
		},

		initialize: function () {
			this.listenTo(this.model, 'change', this.render);
			this.listenTo(this.model, 'destroy', this.remove);
		},

		// Re-update the titles of the todo item.
		render: function () {
			// console.log('UPDATE!');
			// console.log(this.$el);
			this.$el.html(this.template(this.model.toJSON()));
			return this;
		},

		// Switch this view into `"editing"` mode, displaying the input field.
		edit: function () {
			// это хак :( - надо что-то делать с видами норм
			this.$el.children().hide();
			var editView = new app.MaterialEditView({ model: this.model });
			editView.model.set('category_id', this.model.id, {silent:true});

			this.listenToOnce(editView, 'editStopped', this.render);
			this.$el.append(editView.render().el);
		},


		clear: function () {
			this.model.destroy();
		}
	});

	app.MaterialEditView = Backbone.View.extend({
		template: _.template($('#material-edit-template').html()),
		errorTemplate: _.template($('#error-template').html()),
		tagName: 'div',
		className: 'material-edit',

		initialize: function () {
			this.listenTo(this.model, 'change', this.render);
			this.listenTo(this.model, 'destroy', this.remove);
			this.uploadView = new app.MaterialEditUploadView({ model: this.model });
		},

		// The DOM events specific to an item.
		events: {
			'click .save-material': 'save',
			'click .discard-material': 'discard',
		},

		render: function() {
			// console.log(this.model);
			this.$el.html(this.template(this.model.toJSON()));
			this.$el.prepend(this.uploadView.render().el);
			return this;
		},

		discard: function () {
			this.model.fetch();
			this.trigger('discarded');
			this.trigger('editStopped', this);
		},


		save: function () {
			
			console.log('save');
			if (this.uploadView.state == 'uploading') {
				alert('Необходимо подождать завершения загрузки файла.');
				return;
			}
			// clear errors
			$('.error', this.$el).remove();

			var name = $('input[name="name"]', this.$el).val().trim();
			var url = $('input[name="url"]', this.$el).val() || this.model.get('url');

			if (this.uploadView.state == 'noupload' && this.model.get('upload_id')) {
				this.model.unset('upload_id', {silent:true});
				this.model.set('url', '', {silent:true});
			}
			

			var view = this;
			console.log(this.model);
			this.model.save({ 
				name: $.trim(name), 
				url: $.trim(url), 
				category_id: $.trim(this.model.get('category_id')) }, {

				success: function(model, response, options) {
					if (response) {
						//set id
						view.model.set('id', response.id, {silent: true});
					}
					view.trigger('saved', view.model);
					view.trigger('editStopped', view);
				},
				error: function(model, xhr, options) {
					var status = xhr.status;
					if (status == 406) {
						var errors = JSON.parse(xhr.responseText);
						_.forEach(['name', 'url'], function(field){
							var fieldEl = $('input[name="'+field+'"]', view.$el);

							_.forEach(errors[field], function(error){
								$(view.errorTemplate({error: error})).insertBefore(fieldEl);
							});
						});
					}
				},
			});
		},
	});

	app.MaterialEditUploadView = Backbone.View.extend({
		errorTemplate: _.template($('#error-template').html()),
		tagName: 'div',

		templates: {
			'noupload': _.template($('#material-edit-noupload-template').html()),
			'uploading': _.template($('#material-edit-uploading-template').html()),
			'uploaded': _.template($('#material-edit-uploaded-template').html()),
		},
		
		events: {
			'click .upload-material': 'triggerUpload',
			'click .remove-upload': 'removeUpload',
			'change .upload-input': 'upload',
		},

		initialize: function () {
			this.listenTo(this.model, 'change', this.render);
			this.listenTo(this.model, 'destroy', this.remove);

			if (this.model.get('upload_id'))
				this.setState('uploaded');
			else
				this.setState('noupload');
			// console.log('MODEL', this.model, this.model.get('upload_id'));
			this.listenTo(this, 'stateChanged', this.render);
		},

		setState: function(state) {
			this.state = state;
			this.trigger('stateChanged');
		},

		triggerUpload: function() {
			this.$('.upload-input').trigger('click');
		},

		removeUpload: function() {
			// TODO
			// console.log($('input[name="url"]', this.$el));
			// $('input[name="url"]', this.$el).val('');
			this.setState('noupload');
		},

		upload: function() {
			var formData = new FormData(this.$('form')[0]);
			var csrf_token = $('input[name="csrfmiddlewaretoken"]').val();
			formData.append('category_id', this.model.get('category_id'));
			var view = this;
			var is_success = false;
			$.ajax({
				csrfmiddlewaretoken: csrf_token,
				type: 'POST',
				url: '/api/materials/upload/',
				contentType: false,
				processData: false,
				data: formData,
				beforeSend: function() {
					view.setState('uploading');
				},

				success: function(data) {
					view.model.set(data, {silent: true});
					is_success = true;
				},

				// TODO errors
				
				complete: function() {
					if (is_success) 
						view.setState('uploaded');
					else
						view.setState('noupload');
				}

			});
		},

		render: function() {
			// console.log(this.model);
			this.$el.html(this.templates[this.state](this.model.toJSON()));
			return this;
		},
	});


})(jQuery);



(function ($) {
	'use strict';

	app.AppView = Backbone.View.extend({
		el: '#materials-app',

		initialize: function () {
			this.$footer = this.$('#footer');
			this.$main = this.$('#main');

			this.initNewCategoryView();
			this.updateCategories();
			
		},

		initNewCategoryView: function() {
			var newView = new app.CategoryEditView({model: new app.Category()});
			if (!this.$newCategoryView) {
				// init
				this.$newCategoryView = newView;
				this.$('#main').prepend(this.$newCategoryView.render().el);
			} else {
				// replace on new add
				this.$newCategoryView.$el.replaceWith(newView.render().el);
				this.$newCategoryView.remove();
				this.$newCategoryView = newView;
			}
			
			this.listenTo(this.$newCategoryView, 'saved', this.addCategory);
			this.listenTo(this.$newCategoryView, 'editStopped', this.initNewCategoryView);
		},

		addCategory: function(category) {
			var view = new app.CategoryContentView({ model: category });
			this.$('#categories').append(view.render().el);
		},

		renderCategories: function() {
			this.$('#categories').html('');
			app.categories.each(this.addCategory, this);
		},

		updateCategories: function() {
			var view = this;
			app.categories.fetch({
				success: function() {
					view.renderCategories();
				}
			});
		},
	});


	// app.categoriesViews = {};
})(jQuery);