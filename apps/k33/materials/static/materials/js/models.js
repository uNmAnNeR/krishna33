var app = app || {};

(function () {
	'use strict';

	app.Category = Backbone.Model.extend({
		defaults: {
			name: '',
			parent: null,
			private: false
		},

		validate: function(attrs, options) {
			console.log(attrs);
			if (!attrs.name) {
				alert("Имя не может быть пустым");
				return "Имя не может быть пустым";
			} else if (20 < attrs.name.length) {
				alert("Имя должно содержать менее 20 символов");
				return "Имя должно содержать менее 20 символов";
			} else if (app.categories.where({'name':attrs.name}).length > 1) {
				alert("Имя должно быть уникальным");
				return "Имя должно быть уникальным";
			}
			console.log("VSE OK!");
		},

		urlRoot: '/api/materials/categories/',
	});


	app.Material = Backbone.Model.extend({
		defaults: {
			name: '',
			url: '',
			upload_id: null,
			category_id: null,
		},

		// validate: function(attrs, options) {
		// 	if (!attrs.name) {
		// 		alert("Имя не может быть пустым");
		// 		return "Имя не может быть пустым";
		// 	} else if (20 < attrs.name.length) {
		// 		alert("Имя должно содержать менее 20 символов");
		// 		return "Имя должно содержать менее 20 символов";
		// 	} else if (app.categories.findWhere({'name':attrs.name})) {
		// 		alert("Имя должно быть уникальным");
		// 		return "Имя должно быть уникальным";
		// 	}
		// 	console.log("VSE OK!");
		// },

		urlRoot: '/api/materials/',
	});

})();

(function () {
	'use strict';

	app.CategoryList = Backbone.Collection.extend({
		// Reference to this collection's model.
		model: app.Category,
		url: '/api/materials/categories/',
		comparator: function(category) {
			return category.get("name");
		},
	});

	app.categories = new app.CategoryList();


	app.MaterialList = Backbone.Collection.extend({
		model: app.Material,
		url: '/api/materials/',
		comparator: function(material) {
			return material.get("name");
		},
	});
})();