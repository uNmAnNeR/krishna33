from django.db import models
from django.contrib import admin
# from django.contrib.auth.models import User
from django.conf import settings
User = settings.AUTH_USER_MODEL
import os
import shutil

# from django.contrib.auth import get_user_model
# User = get_user_model()

from k33.private.models import Privatable
from k33.uploads.models import Upload

MATERIALS_DIR = 'materials'

# def make_upload_path(instance, filename):
#	 """Generates upload path for FileField"""
#	 return u"materials/%s/%s" % (instance.category, filename)

class Category(Privatable):
	name = models.CharField(max_length=100)
	parent = models.ForeignKey('self', null=True, blank=True)
	# creating_date = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return self.name

	@property
	def path(self):
		return os.path.join(MATERIALS_DIR, str(self.id))

	@property
	def abspath(self):
		return os.path.join(settings.MEDIA_ROOT, self.path)
	

	def delete(self, *args, **kwargs):
		folder_path = self.abspath
		super().delete(*args, **kwargs)
		shutil.rmtree(folder_path, ignore_errors=True)

# class Upload(models.Model):
# 	user = models.ForeignKey(User)
# 	category = models.ForeignKey(Category)
# 	file = models.FileField(upload_to=make_upload_path)
# 	date = models.DateTimeField(auto_now_add=True)

class Material(models.Model):
	url = models.URLField()
	upload = models.ForeignKey(Upload, null=True, blank=True, on_delete=models.SET_NULL)
	name = models.CharField(max_length=100)
	description = models.TextField(null=True, blank=True)
	category = models.ForeignKey(Category, related_name='materials')
	date = models.DateField(auto_now_add=True)
	# creating_date = models.DateTimeField(auto_now_add=True)
		
	def __str__(self):
		return "{}: {}".format(self.name, str(self.category))

	def save(self, *args, **kwargs):
		if self.upload and self.upload.file:
			print(self.upload)
			self.url = self.upload.file.url
		super().save(*args, **kwargs)

# Receive the pre_delete signal and delete the file associated with the model instance.
# from django.db.models.signals import pre_delete
# from django.dispatch.dispatcher import receiver

# @receiver(pre_delete, sender=Material)
# def mymodel_delete(sender, instance, **kwargs):
# 	# Pass false so FileField doesn't save the model.
# 	instance.upload.file.delete(False)
	


class MaterialAdmin(admin.ModelAdmin):
	pass

class CategoryAdmin(admin.ModelAdmin):
	pass

# class UploadAdmin(admin.ModelAdmin):
# 	pass

admin.site.register(Material, MaterialAdmin)
admin.site.register(Category, CategoryAdmin)
# admin.site.register(Upload, UploadAdmin)