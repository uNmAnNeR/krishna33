from django.conf.urls import patterns, include, url


urlpatterns = patterns('k33.materials.api.views',
	(r'^upload/$', 'upload'),
	(r'^$', 'materials'),
	(r'^(?P<material_id>\d+)/$', 'material'),
	(r'^categories/$', 'categories'),
	(r'^categories/(?P<category_id>\d+)/$', 'category'),
)
