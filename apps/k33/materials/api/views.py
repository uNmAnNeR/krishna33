import json
import os

from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
# from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.shortcuts import get_object_or_404

from k33.materials.models import Category, Material, MATERIALS_DIR
from k33.uploads.models import Upload
from django.core.files.base import File
from django.forms.models import model_to_dict

CATEGORY_FIELDS = ['id', 'name', 'private']
MATERIAL_FIELDS = ['id', 'url', 'name',
                   'description', 'category_id', 'upload_id']


@login_required
def upload(request):
    user = request.user
    # print(ppc)
    if 'category_id' not in request.POST:
        return HttpResponse('category id should be set')

    category_id = request.POST['category_id']
    category = Category.objects.get(id=category_id)

    if request.FILES and 'file' in request.FILES:
        uploaded_file = request.FILES['file']
        relpath = os.path.join(
            MATERIALS_DIR, str(category.id), uploaded_file.name)
        upf = File(uploaded_file)
        upf.name = relpath
        u = Upload(user=user, file=upf)
        u.save()
        return HttpResponse(json.dumps({'upload_id': u.id, 'url': u.file.url}), mimetype="application/json")

# Create your views here.


@login_required
def materials(request):
    if request.method == 'GET':
        # TODO generic filters
        if 'category_id' in request.GET:
            category_id = request.GET['category_id']
            materials = list(Material.objects.filter(
                category_id=category_id).values(*MATERIAL_FIELDS))
        else:
            materials = list(Material.objects.values(*MATERIAL_FIELDS))
        # print(materials)
        return HttpResponse(json.dumps(materials), mimetype="application/json")
    elif request.method == 'POST':
        data = json.loads(request.readlines()[0].decode('utf-8'))
        # print(data)
        material = Material(**data)
        material.save()
        return HttpResponse(json.dumps({'id': material.id}), mimetype="application/json")


@login_required
def material(request, material_id):
    user = request.user

    material = get_object_or_404(Material, id=material_id)

    # проверка привилегий
    if material.category.private and not user.is_staff and not user.has_perm('private.view_private'):
        return

    if request.method == 'GET':
        return HttpResponse(json.dumps(model_to_dict(material, fields=MATERIAL_FIELDS)), mimetype="application/json")
    elif request.method == 'DELETE':
        if material:
            material.delete()
        return HttpResponse()
    elif request.method == 'PUT':
        data = json.loads(request.readlines()[0].decode('utf-8'))
        # print(material, "SAVING DATA:", data)
        # TODO! - нельзя менять id методом setattr, т.к. рушатся внешние ключи
        del data['id']
        del data['category_id']
        for f in data:
            setattr(material, f, data[f])
        # material.name = data['name']
        # print(data['url']);
        # material.url = data['url']
        # material.description = data['description']
        # print(material.upload)
        if 'upload_id' not in data and material.upload:
            material.upload.delete()
            # Хак - не знаю почему, но upload_id остается при удалении
            del material.upload_id

        material.save()
        # TODO
        errors = {
            'name': ['pupsik ne ho4et!', 'a ya ho4u', 'i td...'], 'private': ['ne private!']}
        return HttpResponse()
        return HttpResponse(json.dumps(errors), mimetype="application/json", status=406)


@login_required
def categories(request):
    user = request.user

    # проверка привилегий
    categories_qset = Category.objects
    if not user.is_staff and not user.has_perm('private.view_private'):
        categories_qset = categories_qset.filter(private=False)
    categories = list(categories_qset.values(*CATEGORY_FIELDS))

    if request.method == 'GET':
        return HttpResponse(json.dumps(categories), mimetype="application/json")
    elif request.method == 'POST':
        data = json.loads(request.readlines()[0].decode('utf-8'))
        category = Category(**data)
        category.save()
        return HttpResponse(json.dumps({'id': category.id}), mimetype="application/json")


@login_required
def category(request, category_id):
    user = request.user
    category = get_object_or_404(Category, id=category_id)

    # проверка привилегий
    if (category.private and
        not user.is_staff and
        not user.has_perm('private.view_private')):
        return

    if request.method == 'GET':
        return HttpResponse(json.dumps(model_to_dict(category, fields=CATEGORY_FIELDS)), mimetype="application/json")
    elif request.method == 'DELETE':
        category.delete()
        return HttpResponse()
    elif request.method == 'PUT':
        data = json.loads(request.readlines()[0].decode('utf-8'))
        category.name = data['name']
        category.private = data['private']
        category.parent = data['parent']
        category.save()
        # TODO
        errors = {
            'name': ['pupsik ne ho4et!', 'a ya ho4u', 'i td...'], 'private': ['ne private!']}
        return HttpResponse()
        return HttpResponse(json.dumps(errors), mimetype="application/json", status=406)
