import os

from django.conf import settings
from django.shortcuts import redirect, render
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist

from k33.materials.models import Category, Material, MATERIALS_DIR
from k33.uploads.models import Upload


# Create your views here.
@login_required
def index(request):
	# user = request.user
	categories = Category.objects.all()

	return render(request, 'materials/all.html', locals())


@login_required
def restore(request):
	user = request.user
	if not user.is_staff:
		return
	# clear first
	# Category.objects.all().delete()

	materials_path = os.path.join(settings.MEDIA_ROOT, MATERIALS_DIR)
	categories = os.listdir(materials_path)
	for c in categories:
		materials = os.listdir(os.path.join(materials_path, c))
		category = Category.objects.get_or_create(id=c, defaults={'name': c})

		for m in materials:
			relpath = os.path.join(MATERIALS_DIR, c, m)
			try:
				# проверяем нет ли уже в базе данных такого материала
				material = Material.objects.get(
					category=category, upload__file=relpath)
			except ObjectDoesNotExist:
				upload = Upload(user=user)
				upload.file.name = relpath
						# upload.file.save(relpath, File(upload_file))
				upload.save()
				material = Material(name=m, category=category, upload=upload)
				material.save()

			# with open(os.path.join(settings.MEDIA_ROOT, relpath), 'rb') as
			# upload_file:

	return redirect('/materials/')
