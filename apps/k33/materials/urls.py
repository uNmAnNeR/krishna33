from django.conf.urls import patterns, include, url


urlpatterns = patterns('k33.materials.views',
	(r'^$', 'index'),
	(r'^restore/$', 'restore'),
	# (r'^add/$', 'add'),
	# (r'^(?P<vote_id>\d+)/$', 'vote'), #p
	# (r'^edit/(?P<voting_id>\d+)/$', 'edit'), #p
	# (r'^make/$', 'make'),
	# (r'^save/$', 'save'),
	# (r'^delete/$', 'delete'),
	# (r'^reset/$', 'reset'),
)

