from rest_framework import permissions


class CanManageTransactions(permissions.BasePermission):

    def has_permission(self, request, view, obj):
        return request.user.has_perm('bookkeeping.can_manage_transactions')
