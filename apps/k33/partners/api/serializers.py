from rest_framework import serializers
from k33.accounts.api.serializers import SimpleAccountSerializer
from ..models import Partner


class SimplePartnerSerializer(serializers.ModelSerializer):

    class Meta:
        model = Partner
        fields = ('id', 'name', )
        read_only_fields = ('id', )


class PartnerSerializer(SimplePartnerSerializer):
    users = SimpleAccountSerializer(many=True)
