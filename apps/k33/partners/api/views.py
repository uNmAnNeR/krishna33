# from django.db.models import Q

from rest_framework import viewsets  # generics
# from rest_framework.decorators import api_view, permission_classes, action
# from rest_framework.response import Response
from rest_framework.permissions import IsAdminUser

from .serializers import PartnerSerializer
from ..models import Partner
# from k33.common.api.permissions import IsStaffOrReadOnly


class PartnerViewSet(viewsets.ModelViewSet):
    model = Partner
    serializer_class = PartnerSerializer
    permission_classes = [IsAdminUser]

    # @action(methods=['POST', 'PUT'])
    # def execute(self, request, pk=None):
    #     tran = self.get_object()
    #     tran.execute()
    #     return Response({'status': 'seems_executed'})

    # @action(methods=['POST', 'PUT'])
    # def rollback(self, request, pk=None):
    #     tran = self.get_object()
    #     tran.rollback()
    #     return Response({'status': 'seems_rollbacked'})
