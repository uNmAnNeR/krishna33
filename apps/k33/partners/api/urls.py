from django.conf.urls import patterns, url

from rest_framework import routers
from .views import PartnerViewSet
router = routers.SimpleRouter(trailing_slash=False)
router.register(r'partners', PartnerViewSet)
urlpatterns = router.urls
