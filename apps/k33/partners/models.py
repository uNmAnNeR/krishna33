from django.db import models  # , transaction
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import get_user_model


class Partner(models.Model):

    """
    Компания или частное лицо
    """

    name = models.CharField(_("Name"), max_length=100, unique=True)
    users = models.ManyToManyField(
        get_user_model(), related_name="partners",
        blank=True, null=True, verbose_name=_("Users"))

    def __str__(self):
        return self.name
