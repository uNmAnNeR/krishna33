from datetime import datetime
import json

from django.template.response import TemplateResponse
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.contrib.auth.decorators import login_required
from django.template import Context, Template, loader
from django.http import HttpResponse
from django.utils.timezone import utc

from k33.comments.models import Comment, Commentable


@login_required
def comment_add(request):
	user = request.user
	if request.method == 'POST':
		# NAITI MODIFIED DATE, I esli est' novie dannie - pereslat'.
		# eshe sdelat' perehod na novii koment a ne v na4alo stranici.

		commented_id = request.POST['commented_id']
		comment_id = request.POST.get('comment_id', -1)
		commented = None
		try:
			commented = Commentable.objects.get(id=commented_id)
		except (ObjectDoesNotExist, ValueError):
			print('ERROR')
			return # TODO ERROR

		comment = None
		if comment_id >= 0:
			try:
				comment = Comment.objects.get(id=comment_id)
			except (ObjectDoesNotExist, ValueError):
				pass
		if not comment:
			comment = Comment(author=user, commented=commented)
			comment.publish_date = datetime.utcnow().replace(tzinfo=utc)
		comment.content = request.POST['comment_content']
		comment.save()
		comment_id = comment.id
		success = False
		if request.is_ajax():
			t = loader.get_template('comments/comment.html')
			comment_html = t.render(Context({'comment': comment}))
			success = True
			return HttpResponse(json.dumps({ 'result': success, 'html': comment_html }), mimetype="application/json")
		else:
			pass
			# print(request.get_full_path()) # TODO
		 	# return redirect('/posts/%s/#comment_%s' % (post_id, comment_id))

def comment(request, comment_id):
	comment = Comment.objects.get(id=comment_id)
	params = { 'comment': comment }
	
	return TemplateResponse(request,'comments/comment.html', params)