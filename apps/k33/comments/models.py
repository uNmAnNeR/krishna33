from django.db import models
from django.contrib import admin
# from django.contrib.auth.models import User
# from k33.accounts.models import MyUser as User
from django.conf import settings
from django.contrib.auth import get_user_model

class Commentable(models.Model):
	pass
	# def get_reply_comments(comments_queryset, parent_comment=None):
	# 	comments = []
	# 	reply_comments = comments_queryset.filter(parent_comment=parent_comment).order_by('publish_date')
	# 	comments_queryset.exclude(id__in=reply_comments)
	# 	for rc in reply_comments:
	# 		rc.level = 0 if not parent_comment else parent_comment.level+1
	# 		comments.append(rc)
	# 		comments.extend(Post.get_reply_comments(comments_queryset, rc))
	# 	return comments

	# @property
	# def nested_comments(self):
	# 	"""
	# 	Отсортированные по дате комментарии с учетом вложенности. -> list
	# 	"""
	# 	ncs = []
	# 	for c in self.comments.order_by('publish_date'):

	# 	return Commentable.get_reply_comments(self.comments)
	
class Comment(Commentable):
	author = models.ForeignKey(get_user_model())
	publish_date = models.DateTimeField()
	content = models.TextField()
	commented = models.ForeignKey(Commentable, related_name='comments')	

	def __str__(self):
		return '{}: {} ({})'.format(self.author, self.post.title, self.publish_date)

class CommentAdmin(admin.ModelAdmin):
	list_display = ['author', 'publish_date', 'content', 'commented']

admin.site.register(Comment, CommentAdmin)