function comment_save(event, commented_id, comment_id) {
	event.preventDefault();
	var save_button = event.target;
	var content_textarea = $(save_button).siblings('textarea[name="comment_content"]');
	var csrf_token = $(save_button).siblings('input[name="csrfmiddlewaretoken"]');
	$.ajax({
		type: 'POST',
		url: '/comments/add/',
		dataType : "json", 
		data: { 'comment_id': comment_id,
				'commented_id': commented_id, 
				'comment_content': content_textarea.val(),
				'csrfmiddlewaretoken': csrf_token.val() },
		beforeSend: function () {
			// $(quation_element).find('input, button').prop('disabled', true)
		}
	})
	.done(function(resultdict) {
		result = resultdict.result;
		if (result) {
			comment_form = $(save_button).closest("form");
			html = resultdict.html;
			$(html).insertBefore(comment_form);
			content_textarea.val('')
		}
	})
	.fail(function(response, status, error) {
	});
}