from django.contrib import admin
from .models import (Contract, Transaction, TransactionProduct, 
    AllocateTransaction, PurchaseTransaction, ReturnTransaction,
    RealizeTransaction, RefundTransaction, CreditTransaction,
    DebitTransaction)
#from mptt.admin import MPTTModelAdmin


class TransactionProductInline(admin.TabularInline):
    model = TransactionProduct


class TransactionAdmin(admin.ModelAdmin):
    exclude = ['executed_at', 'rollbacked_at', 'status']
    list_display = ['contract', 'executed_at', 'rollbacked_at', 'status']
    actions = ['execute', 'rollback']
    inlines = [TransactionProductInline]

    def execute(self, request, queryset):
        for r in queryset.select_subclasses():
            r.execute()
    execute.short_description = "Execute transactions"

    def rollback(self, request, queryset):
        for r in queryset.select_subclasses():
            r.rollback()
    rollback.short_description = "Rollback transactions"


admin.site.register(Contract)
admin.site.register(Transaction, TransactionAdmin)
admin.site.register(AllocateTransaction, TransactionAdmin)
admin.site.register(PurchaseTransaction, TransactionAdmin)
admin.site.register(ReturnTransaction, TransactionAdmin)
admin.site.register(RealizeTransaction, TransactionAdmin)
admin.site.register(RefundTransaction, TransactionAdmin)
admin.site.register(CreditTransaction, TransactionAdmin)
admin.site.register(DebitTransaction, TransactionAdmin)
admin.site.register(TransactionProduct)
