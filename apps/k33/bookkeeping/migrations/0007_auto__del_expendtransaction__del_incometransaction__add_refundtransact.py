# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'ExpendTransaction'
        db.delete_table('bookkeeping_expendtransaction')

        # Deleting model 'IncomeTransaction'
        db.delete_table('bookkeeping_incometransaction')

        # Adding model 'RefundTransaction'
        db.create_table('bookkeeping_refundtransaction', (
            ('transaction_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['bookkeeping.Transaction'], unique=True, primary_key=True)),
            ('stock', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['store.Stock'], related_name='refund_transactions')),
        ))
        db.send_create_signal('bookkeeping', ['RefundTransaction'])

        # Adding model 'DebitTransaction'
        db.create_table('bookkeeping_debittransaction', (
            ('transaction_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['bookkeeping.Transaction'], unique=True, primary_key=True)),
            ('store', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['store.Store'], related_name='debit_transactions')),
            ('funds', self.gf('django.db.models.fields.DecimalField')(blank=True, max_digits=12, decimal_places=3, null=True)),
        ))
        db.send_create_signal('bookkeeping', ['DebitTransaction'])

        # Adding model 'ReturnTransaction'
        db.create_table('bookkeeping_returntransaction', (
            ('transaction_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['bookkeeping.Transaction'], unique=True, primary_key=True)),
            ('stock', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['store.Stock'], related_name='return_transactions')),
        ))
        db.send_create_signal('bookkeeping', ['ReturnTransaction'])

        # Adding model 'CreditTransaction'
        db.create_table('bookkeeping_credittransaction', (
            ('transaction_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['bookkeeping.Transaction'], unique=True, primary_key=True)),
            ('store', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['store.Store'], related_name='credit_transactions')),
            ('funds', self.gf('django.db.models.fields.DecimalField')(blank=True, max_digits=12, decimal_places=3, null=True)),
        ))
        db.send_create_signal('bookkeeping', ['CreditTransaction'])


    def backwards(self, orm):
        # Adding model 'ExpendTransaction'
        db.create_table('bookkeeping_expendtransaction', (
            ('store', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['store.Store'], related_name='expend_transactions')),
            ('funds', self.gf('django.db.models.fields.DecimalField')(null=True, decimal_places=3, blank=True, max_digits=12)),
            ('transaction_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['bookkeeping.Transaction'], unique=True, primary_key=True)),
        ))
        db.send_create_signal('bookkeeping', ['ExpendTransaction'])

        # Adding model 'IncomeTransaction'
        db.create_table('bookkeeping_incometransaction', (
            ('store', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['store.Store'], related_name='income_transactions')),
            ('funds', self.gf('django.db.models.fields.DecimalField')(null=True, decimal_places=3, blank=True, max_digits=12)),
            ('transaction_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['bookkeeping.Transaction'], unique=True, primary_key=True)),
        ))
        db.send_create_signal('bookkeeping', ['IncomeTransaction'])

        # Deleting model 'RefundTransaction'
        db.delete_table('bookkeeping_refundtransaction')

        # Deleting model 'DebitTransaction'
        db.delete_table('bookkeeping_debittransaction')

        # Deleting model 'ReturnTransaction'
        db.delete_table('bookkeeping_returntransaction')

        # Deleting model 'CreditTransaction'
        db.delete_table('bookkeeping_credittransaction')


    models = {
        'accounts.myuser': {
            'Meta': {'object_name': 'MyUser'},
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '255', 'db_index': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'user_set'", 'blank': 'True', 'to': "orm['auth.Group']", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'owned': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'user_set'", 'blank': 'True', 'to': "orm['auth.Permission']", 'symmetrical': 'False'})
        },
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'to': "orm['auth.Permission']", 'symmetrical': 'False'})
        },
        'auth.permission': {
            'Meta': {'object_name': 'Permission', 'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)"},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'bookkeeping.allocatetransaction': {
            'Meta': {'_ormbases': ['bookkeeping.Transaction'], 'object_name': 'AllocateTransaction'},
            'stock': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['store.Stock']", 'related_name': "'allocate_transactions'"}),
            'transaction_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['bookkeeping.Transaction']", 'unique': 'True', 'primary_key': 'True'})
        },
        'bookkeeping.contract': {
            'Meta': {'object_name': 'Contract'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['partners.Partner']", 'related_name': "'owner_contracts'"}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'related_name': "'children'", 'blank': 'True', 'to': "orm['bookkeeping.Contract']", 'null': 'True'}),
            'partner': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['partners.Partner']", 'related_name': "'partner_contracts'"}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'})
        },
        'bookkeeping.credittransaction': {
            'Meta': {'_ormbases': ['bookkeeping.Transaction'], 'object_name': 'CreditTransaction'},
            'funds': ('django.db.models.fields.DecimalField', [], {'blank': 'True', 'max_digits': '12', 'decimal_places': '3', 'null': 'True'}),
            'store': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['store.Store']", 'related_name': "'credit_transactions'"}),
            'transaction_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['bookkeeping.Transaction']", 'unique': 'True', 'primary_key': 'True'})
        },
        'bookkeeping.debittransaction': {
            'Meta': {'_ormbases': ['bookkeeping.Transaction'], 'object_name': 'DebitTransaction'},
            'funds': ('django.db.models.fields.DecimalField', [], {'blank': 'True', 'max_digits': '12', 'decimal_places': '3', 'null': 'True'}),
            'store': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['store.Store']", 'related_name': "'debit_transactions'"}),
            'transaction_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['bookkeeping.Transaction']", 'unique': 'True', 'primary_key': 'True'})
        },
        'bookkeeping.purchasetransaction': {
            'Meta': {'_ormbases': ['bookkeeping.Transaction'], 'object_name': 'PurchaseTransaction'},
            'stock': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['store.Stock']", 'related_name': "'purchase_transactions'"}),
            'transaction_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['bookkeeping.Transaction']", 'unique': 'True', 'primary_key': 'True'})
        },
        'bookkeeping.realizetransaction': {
            'Meta': {'_ormbases': ['bookkeeping.Transaction'], 'object_name': 'RealizeTransaction'},
            'consume_allocation': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'stock': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['store.Stock']", 'related_name': "'realize_transactions'"}),
            'transaction_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['bookkeeping.Transaction']", 'unique': 'True', 'primary_key': 'True'})
        },
        'bookkeeping.refundtransaction': {
            'Meta': {'_ormbases': ['bookkeeping.Transaction'], 'object_name': 'RefundTransaction'},
            'stock': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['store.Stock']", 'related_name': "'refund_transactions'"}),
            'transaction_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['bookkeeping.Transaction']", 'unique': 'True', 'primary_key': 'True'})
        },
        'bookkeeping.returntransaction': {
            'Meta': {'_ormbases': ['bookkeeping.Transaction'], 'object_name': 'ReturnTransaction'},
            'stock': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['store.Stock']", 'related_name': "'return_transactions'"}),
            'transaction_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['bookkeeping.Transaction']", 'unique': 'True', 'primary_key': 'True'})
        },
        'bookkeeping.transaction': {
            'Meta': {'object_name': 'Transaction'},
            'canceled_at': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'null': 'True'}),
            'contract': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['bookkeeping.Contract']", 'related_name': "'transactions'"}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'done_at': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'null': 'True'}),
            'executed_at': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'note': ('django.db.models.fields.TextField', [], {'blank': 'True', 'null': 'True'}),
            'rollbacked_at': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'null': 'True'}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'})
        },
        'bookkeeping.transactionproduct': {
            'Meta': {'object_name': 'TransactionProduct'},
            'cost_price': ('django.db.models.fields.DecimalField', [], {'blank': 'True', 'max_digits': '12', 'decimal_places': '3', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'price': ('django.db.models.fields.DecimalField', [], {'decimal_places': '3', 'max_digits': '12'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['production.Product']"}),
            'quantity': ('django.db.models.fields.DecimalField', [], {'decimal_places': '3', 'max_digits': '12'}),
            'transaction': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['bookkeeping.Transaction']", 'related_name': "'product_records'"})
        },
        'contenttypes.contenttype': {
            'Meta': {'db_table': "'django_content_type'", 'object_name': 'ContentType', 'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'partners.partner': {
            'Meta': {'object_name': 'Partner'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'users': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'partners'", 'blank': 'True', 'to': "orm['accounts.MyUser']", 'symmetrical': 'False', 'null': 'True'})
        },
        'production.category': {
            'Meta': {'object_name': 'Category'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'related_name': "'children'", 'blank': 'True', 'to': "orm['production.Category']", 'null': 'True'}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        'production.product': {
            'Meta': {'object_name': 'Product'},
            'abbreviation': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '31', 'null': 'True'}),
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'products'", 'blank': 'True', 'to': "orm['production.Category']", 'null': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True', 'null': 'True'}),
            'hidden': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'hidden_at': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'primary_image': ('django.db.models.fields.files.ImageField', [], {'blank': 'True', 'max_length': '100', 'null': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'upc': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '64', 'null': 'True'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'})
        },
        'store.stock': {
            'Meta': {'object_name': 'Stock'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['partners.Partner']", 'related_name': "'owned_stocks'"})
        },
        'store.store': {
            'Meta': {'_ormbases': ['store.Stock'], 'object_name': 'Store'},
            'balance': ('django.db.models.fields.DecimalField', [], {'decimal_places': '3', 'default': "'0'", 'max_digits': '12'}),
            'stock_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['store.Stock']", 'unique': 'True', 'primary_key': 'True'})
        }
    }

    complete_apps = ['bookkeeping']