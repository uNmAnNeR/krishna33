# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Contract'
        db.create_table('bookkeeping_contract', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('owner', self.gf('django.db.models.fields.related.ForeignKey')(related_name='owner_contracts', to=orm['partners.Partner'])),
            ('partner', self.gf('django.db.models.fields.related.ForeignKey')(related_name='partner_contracts', to=orm['partners.Partner'])),
        ))
        db.send_create_signal('bookkeeping', ['Contract'])

        # Deleting field 'RealizeTransaction.customer'
        db.delete_column('bookkeeping_realizetransaction', 'customer_id')

        # Deleting field 'AllocateTransaction.customer'
        db.delete_column('bookkeeping_allocatetransaction', 'customer_id')

        # Adding field 'Transaction.contract'
        db.add_column('bookkeeping_transaction', 'contract',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=0, related_name='transactions', to=orm['bookkeeping.Contract']),
                      keep_default=False)

        # Adding field 'Transaction.done_at'
        db.add_column('bookkeeping_transaction', 'done_at',
                      self.gf('django.db.models.fields.DateTimeField')(blank=True, null=True),
                      keep_default=False)

        # Adding field 'Transaction.canceled_at'
        db.add_column('bookkeeping_transaction', 'canceled_at',
                      self.gf('django.db.models.fields.DateTimeField')(blank=True, null=True),
                      keep_default=False)

        # Deleting field 'TransactionProduct.unit_value'
        db.delete_column('bookkeeping_transactionproduct', 'unit_value')

        # Adding field 'TransactionProduct.cost_price'
        db.add_column('bookkeeping_transactionproduct', 'cost_price',
                      self.gf('django.db.models.fields.DecimalField')(decimal_places=3, blank=True, null=True, max_digits=12),
                      keep_default=False)

        # Deleting field 'PurchaseTransaction.supplier'
        db.delete_column('bookkeeping_purchasetransaction', 'supplier_id')


    def backwards(self, orm):
        # Deleting model 'Contract'
        db.delete_table('bookkeeping_contract')

        # Adding field 'RealizeTransaction.customer'
        db.add_column('bookkeeping_realizetransaction', 'customer',
                      self.gf('django.db.models.fields.related.ForeignKey')(related_name='consume_transactions', null=True, to=orm['accounts.MyUser'], blank=True),
                      keep_default=False)

        # Adding field 'AllocateTransaction.customer'
        db.add_column('bookkeeping_allocatetransaction', 'customer',
                      self.gf('django.db.models.fields.related.ForeignKey')(related_name='allocate_transactions', null=True, to=orm['accounts.MyUser'], blank=True),
                      keep_default=False)

        # Deleting field 'Transaction.contract'
        db.delete_column('bookkeeping_transaction', 'contract_id')

        # Deleting field 'Transaction.done_at'
        db.delete_column('bookkeeping_transaction', 'done_at')

        # Deleting field 'Transaction.canceled_at'
        db.delete_column('bookkeeping_transaction', 'canceled_at')

        # Adding field 'TransactionProduct.unit_value'
        db.add_column('bookkeeping_transactionproduct', 'unit_value',
                      self.gf('django.db.models.fields.DecimalField')(decimal_places=3, max_digits=12, null=True, blank=True),
                      keep_default=False)

        # Deleting field 'TransactionProduct.cost_price'
        db.delete_column('bookkeeping_transactionproduct', 'cost_price')

        # Adding field 'PurchaseTransaction.supplier'
        db.add_column('bookkeeping_purchasetransaction', 'supplier',
                      self.gf('django.db.models.fields.related.ForeignKey')(related_name='purchase_transactions', null=True, to=orm['accounts.MyUser'], blank=True),
                      keep_default=False)


    models = {
        'accounts.myuser': {
            'Meta': {'object_name': 'MyUser'},
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '255', 'db_index': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'user_set'", 'to': "orm['auth.Group']", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'owned': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'user_set'", 'to': "orm['auth.Permission']", 'blank': 'True'})
        },
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['auth.Permission']", 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission', 'ordering': "('content_type__app_label', 'content_type__model', 'codename')"},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'bookkeeping.allocatetransaction': {
            'Meta': {'_ormbases': ['bookkeeping.Transaction'], 'object_name': 'AllocateTransaction'},
            'stock': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'allocate_transactions'", 'to': "orm['store.Stock']"}),
            'transaction_ptr': ('django.db.models.fields.related.OneToOneField', [], {'unique': 'True', 'to': "orm['bookkeeping.Transaction']", 'primary_key': 'True'})
        },
        'bookkeeping.contract': {
            'Meta': {'object_name': 'Contract'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'owner_contracts'", 'to': "orm['partners.Partner']"}),
            'partner': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'partner_contracts'", 'to': "orm['partners.Partner']"})
        },
        'bookkeeping.purchasetransaction': {
            'Meta': {'_ormbases': ['bookkeeping.Transaction'], 'object_name': 'PurchaseTransaction'},
            'stock': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'purchase_transactions'", 'to': "orm['store.Stock']"}),
            'transaction_ptr': ('django.db.models.fields.related.OneToOneField', [], {'unique': 'True', 'to': "orm['bookkeeping.Transaction']", 'primary_key': 'True'})
        },
        'bookkeeping.realizetransaction': {
            'Meta': {'_ormbases': ['bookkeeping.Transaction'], 'object_name': 'RealizeTransaction'},
            'consume_allocation': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'stock': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'consume_transactions'", 'to': "orm['store.Stock']"}),
            'transaction_ptr': ('django.db.models.fields.related.OneToOneField', [], {'unique': 'True', 'to': "orm['bookkeeping.Transaction']", 'primary_key': 'True'})
        },
        'bookkeeping.transaction': {
            'Meta': {'object_name': 'Transaction'},
            'canceled_at': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'null': 'True'}),
            'contract': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'transactions'", 'to': "orm['bookkeeping.Contract']"}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'done_at': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'null': 'True'}),
            'executed_at': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'note': ('django.db.models.fields.TextField', [], {'blank': 'True', 'null': 'True'}),
            'rollbacked_at': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'null': 'True'}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'})
        },
        'bookkeeping.transactionproduct': {
            'Meta': {'object_name': 'TransactionProduct'},
            'cost_price': ('django.db.models.fields.DecimalField', [], {'decimal_places': '3', 'blank': 'True', 'null': 'True', 'max_digits': '12'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['production.Product']"}),
            'quantity': ('django.db.models.fields.DecimalField', [], {'decimal_places': '3', 'max_digits': '12'}),
            'sale_unit_price': ('django.db.models.fields.DecimalField', [], {'decimal_places': '3', 'max_digits': '12'}),
            'transaction': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'product_records'", 'to': "orm['bookkeeping.Transaction']"})
        },
        'contenttypes.contenttype': {
            'Meta': {'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'", 'ordering': "('name',)"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'partners.partner': {
            'Meta': {'object_name': 'Partner'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'users': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'partners'", 'null': 'True', 'to': "orm['accounts.MyUser']", 'blank': 'True'})
        },
        'production.category': {
            'Meta': {'object_name': 'Category'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'related_name': "'children'", 'null': 'True', 'to': "orm['production.Category']", 'blank': 'True'}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        'production.product': {
            'Meta': {'object_name': 'Product'},
            'abbreviation': ('django.db.models.fields.CharField', [], {'max_length': '31', 'blank': 'True', 'null': 'True'}),
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'products'", 'null': 'True', 'to': "orm['production.Category']", 'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now_add': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True', 'null': 'True'}),
            'hidden': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'hidden_at': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'primary_image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True', 'null': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'upc': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True', 'null': 'True'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'})
        },
        'store.stock': {
            'Meta': {'object_name': 'Stock'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'owned_stocks'", 'to': "orm['partners.Partner']"})
        }
    }

    complete_apps = ['bookkeeping']