# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Contract.created_at'
        db.add_column('bookkeeping_contract', 'created_at',
                      self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, default=datetime.datetime(2013, 12, 23, 0, 0), blank=True),
                      keep_default=False)

        # Adding field 'Contract.updated_at'
        db.add_column('bookkeeping_contract', 'updated_at',
                      self.gf('django.db.models.fields.DateTimeField')(auto_now=True, default=datetime.datetime(2013, 12, 23, 0, 0), blank=True),
                      keep_default=False)

        # Adding field 'Contract.associate_contract'
        db.add_column('bookkeeping_contract', 'associate_contract',
                      self.gf('mptt.fields.TreeForeignKey')(blank=True, related_name='subcontracts', null=True, to=orm['bookkeeping.Contract']),
                      keep_default=False)

        # Adding field 'Contract.lft'
        db.add_column('bookkeeping_contract', 'lft',
                      self.gf('django.db.models.fields.PositiveIntegerField')(default=0, db_index=True),
                      keep_default=False)

        # Adding field 'Contract.rght'
        db.add_column('bookkeeping_contract', 'rght',
                      self.gf('django.db.models.fields.PositiveIntegerField')(default=0, db_index=True),
                      keep_default=False)

        # Adding field 'Contract.tree_id'
        db.add_column('bookkeeping_contract', 'tree_id',
                      self.gf('django.db.models.fields.PositiveIntegerField')(default=0, db_index=True),
                      keep_default=False)

        # Adding field 'Contract.level'
        db.add_column('bookkeeping_contract', 'level',
                      self.gf('django.db.models.fields.PositiveIntegerField')(default=0, db_index=True),
                      keep_default=False)

        # Deleting field 'TransactionProduct.sale_unit_price'
        db.delete_column('bookkeeping_transactionproduct', 'sale_unit_price')

        # Adding field 'TransactionProduct.price'
        db.add_column('bookkeeping_transactionproduct', 'price',
                      self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=12, decimal_places=3),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Contract.created_at'
        db.delete_column('bookkeeping_contract', 'created_at')

        # Deleting field 'Contract.updated_at'
        db.delete_column('bookkeeping_contract', 'updated_at')

        # Deleting field 'Contract.associate_contract'
        db.delete_column('bookkeeping_contract', 'associate_contract_id')

        # Deleting field 'Contract.lft'
        db.delete_column('bookkeeping_contract', 'lft')

        # Deleting field 'Contract.rght'
        db.delete_column('bookkeeping_contract', 'rght')

        # Deleting field 'Contract.tree_id'
        db.delete_column('bookkeeping_contract', 'tree_id')

        # Deleting field 'Contract.level'
        db.delete_column('bookkeeping_contract', 'level')

        # Adding field 'TransactionProduct.sale_unit_price'
        db.add_column('bookkeeping_transactionproduct', 'sale_unit_price',
                      self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=12, decimal_places=3),
                      keep_default=False)

        # Deleting field 'TransactionProduct.price'
        db.delete_column('bookkeeping_transactionproduct', 'price')


    models = {
        'accounts.myuser': {
            'Meta': {'object_name': 'MyUser'},
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '255', 'unique': 'True', 'db_index': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'user_set'", 'symmetrical': 'False', 'to': "orm['auth.Group']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'owned': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'user_set'", 'symmetrical': 'False', 'to': "orm['auth.Permission']"})
        },
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '80', 'unique': 'True'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'blank': 'True', 'symmetrical': 'False'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'bookkeeping.allocatetransaction': {
            'Meta': {'object_name': 'AllocateTransaction', '_ormbases': ['bookkeeping.Transaction']},
            'stock': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'allocate_transactions'", 'to': "orm['store.Stock']"}),
            'transaction_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['bookkeeping.Transaction']", 'unique': 'True', 'primary_key': 'True'})
        },
        'bookkeeping.contract': {
            'Meta': {'object_name': 'Contract'},
            'associate_contract': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'related_name': "'subcontracts'", 'null': 'True', 'to': "orm['bookkeeping.Contract']"}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'owner_contracts'", 'to': "orm['partners.Partner']"}),
            'partner': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'partner_contracts'", 'to': "orm['partners.Partner']"}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        'bookkeeping.purchasetransaction': {
            'Meta': {'object_name': 'PurchaseTransaction', '_ormbases': ['bookkeeping.Transaction']},
            'stock': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'purchase_transactions'", 'to': "orm['store.Stock']"}),
            'transaction_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['bookkeeping.Transaction']", 'unique': 'True', 'primary_key': 'True'})
        },
        'bookkeeping.realizetransaction': {
            'Meta': {'object_name': 'RealizeTransaction', '_ormbases': ['bookkeeping.Transaction']},
            'consume_allocation': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'stock': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'consume_transactions'", 'to': "orm['store.Stock']"}),
            'transaction_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['bookkeeping.Transaction']", 'unique': 'True', 'primary_key': 'True'})
        },
        'bookkeeping.transaction': {
            'Meta': {'object_name': 'Transaction'},
            'canceled_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'contract': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'transactions'", 'to': "orm['bookkeeping.Contract']"}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'done_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'executed_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'note': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'rollbacked_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        'bookkeeping.transactionproduct': {
            'Meta': {'object_name': 'TransactionProduct'},
            'cost_price': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '12', 'decimal_places': '3', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'price': ('django.db.models.fields.DecimalField', [], {'max_digits': '12', 'decimal_places': '3'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['production.Product']"}),
            'quantity': ('django.db.models.fields.DecimalField', [], {'max_digits': '12', 'decimal_places': '3'}),
            'transaction': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'product_records'", 'to': "orm['bookkeeping.Transaction']"})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'partners.partner': {
            'Meta': {'object_name': 'Partner'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'unique': 'True'}),
            'users': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'partners'", 'null': 'True', 'blank': 'True', 'to': "orm['accounts.MyUser']"})
        },
        'production.category': {
            'Meta': {'object_name': 'Category'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': "orm['production.Category']"}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        'production.product': {
            'Meta': {'object_name': 'Product'},
            'abbreviation': ('django.db.models.fields.CharField', [], {'null': 'True', 'max_length': '31', 'blank': 'True'}),
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'products'", 'null': 'True', 'to': "orm['production.Category']"}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'hidden': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'hidden_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'primary_image': ('django.db.models.fields.files.ImageField', [], {'null': 'True', 'max_length': '100', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'upc': ('django.db.models.fields.CharField', [], {'null': 'True', 'max_length': '64', 'blank': 'True'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        'store.stock': {
            'Meta': {'object_name': 'Stock'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'unique': 'True'}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'owned_stocks'", 'to': "orm['partners.Partner']"})
        }
    }

    complete_apps = ['bookkeeping']