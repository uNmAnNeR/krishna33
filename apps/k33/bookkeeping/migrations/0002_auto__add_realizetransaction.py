# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'RealizeTransaction'
        db.create_table('bookkeeping_realizetransaction', (
            ('transaction_ptr', self.gf('django.db.models.fields.related.OneToOneField')(unique=True, primary_key=True, to=orm['bookkeeping.Transaction'])),
            ('stock', self.gf('django.db.models.fields.related.ForeignKey')(related_name='consume_transactions', to=orm['store.Stock'])),
            ('customer', self.gf('django.db.models.fields.related.ForeignKey')(null=True, related_name='consume_transactions', to=orm['accounts.MyUser'], blank=True)),
            ('consume_allocation', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('bookkeeping', ['RealizeTransaction'])


    def backwards(self, orm):
        # Deleting model 'RealizeTransaction'
        db.delete_table('bookkeeping_realizetransaction')


    models = {
        'accounts.myuser': {
            'Meta': {'object_name': 'MyUser'},
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '255', 'db_index': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['auth.Group']", 'related_name': "'user_set'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'owned': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['auth.Permission']", 'related_name': "'user_set'", 'blank': 'True'})
        },
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'object_name': 'Permission', 'unique_together': "(('content_type', 'codename'),)", 'ordering': "('content_type__app_label', 'content_type__model', 'codename')"},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'bookkeeping.allocatetransaction': {
            'Meta': {'_ormbases': ['bookkeeping.Transaction'], 'object_name': 'AllocateTransaction'},
            'customer': ('django.db.models.fields.related.ForeignKey', [], {'null': 'True', 'related_name': "'allocate_transactions'", 'to': "orm['accounts.MyUser']", 'blank': 'True'}),
            'stock': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'allocate_transactions'", 'to': "orm['store.Stock']"}),
            'transaction_ptr': ('django.db.models.fields.related.OneToOneField', [], {'unique': 'True', 'primary_key': 'True', 'to': "orm['bookkeeping.Transaction']"})
        },
        'bookkeeping.purchasetransaction': {
            'Meta': {'_ormbases': ['bookkeeping.Transaction'], 'object_name': 'PurchaseTransaction'},
            'stock': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'purchase_transactions'", 'to': "orm['store.Stock']"}),
            'supplier': ('django.db.models.fields.related.ForeignKey', [], {'null': 'True', 'related_name': "'purchase_transactions'", 'to': "orm['accounts.MyUser']", 'blank': 'True'}),
            'transaction_ptr': ('django.db.models.fields.related.OneToOneField', [], {'unique': 'True', 'primary_key': 'True', 'to': "orm['bookkeeping.Transaction']"})
        },
        'bookkeeping.realizetransaction': {
            'Meta': {'_ormbases': ['bookkeeping.Transaction'], 'object_name': 'RealizeTransaction'},
            'consume_allocation': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'customer': ('django.db.models.fields.related.ForeignKey', [], {'null': 'True', 'related_name': "'consume_transactions'", 'to': "orm['accounts.MyUser']", 'blank': 'True'}),
            'stock': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'consume_transactions'", 'to': "orm['store.Stock']"}),
            'transaction_ptr': ('django.db.models.fields.related.OneToOneField', [], {'unique': 'True', 'primary_key': 'True', 'to': "orm['bookkeeping.Transaction']"})
        },
        'bookkeeping.transaction': {
            'Meta': {'object_name': 'Transaction'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'executed_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'note': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'rollbacked_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        'bookkeeping.transactionproduct': {
            'Meta': {'object_name': 'TransactionProduct'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['production.Product']"}),
            'quantity': ('django.db.models.fields.DecimalField', [], {'decimal_places': '3', 'max_digits': '12'}),
            'sale_unit_price': ('django.db.models.fields.DecimalField', [], {'decimal_places': '3', 'max_digits': '12'}),
            'transaction': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'product_records'", 'to': "orm['bookkeeping.Transaction']"}),
            'unit_value': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '12', 'decimal_places': '3', 'blank': 'True'})
        },
        'contenttypes.contenttype': {
            'Meta': {'db_table': "'django_content_type'", 'object_name': 'ContentType', 'unique_together': "(('app_label', 'model'),)", 'ordering': "('name',)"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'production.category': {
            'Meta': {'object_name': 'Category'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'null': 'True', 'related_name': "'children'", 'to': "orm['production.Category']", 'blank': 'True'}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        'production.product': {
            'Meta': {'object_name': 'Product'},
            'abbreviation': ('django.db.models.fields.CharField', [], {'null': 'True', 'max_length': '31', 'blank': 'True'}),
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'null': 'True', 'related_name': "'products'", 'to': "orm['production.Category']", 'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'hidden': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'hidden_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'primary_image': ('django.db.models.fields.files.ImageField', [], {'null': 'True', 'max_length': '100', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'upc': ('django.db.models.fields.CharField', [], {'null': 'True', 'max_length': '64', 'blank': 'True'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        'store.stock': {
            'Meta': {'object_name': 'Stock'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'owned_stocks'", 'to': "orm['accounts.MyUser']"})
        }
    }

    complete_apps = ['bookkeeping']