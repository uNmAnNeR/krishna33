from rest_framework import serializers
from k33.accounts.api.serializers import SimpleAccountSerializer
from k33.partners.api.serializers import SimplePartnerSerializer
from k33.common.api.serializers import (AllowedFieldsModelSerializer, 
    DynamicModelSerializer)
from k33.production.api.serializers import ProductSerializer
from k33.store.api.serializers import StockSerializer, StoreSerializer
from ..models import (Contract, Transaction, AllocateTransaction,
    PurchaseTransaction, ReturnTransaction, RealizeTransaction,
    RefundTransaction, CreditTransaction, DebitTransaction,
    TransactionProduct)


class ContractPlainSerializer(AllowedFieldsModelSerializer):
    owner = SimplePartnerSerializer()
    partner = SimplePartnerSerializer()

    class Meta:
        model = Contract
        fields = ('id', 'owner', 'partner', 'created_at', 'updated_at', 'note', )
        read_only_fields = ('id', )


class ContractSerializer(ContractPlainSerializer):

    class Meta:
        model = Contract
        fields = ContractPlainSerializer.Meta.fields + ('children', )
        read_only_fields = ('id', )

ContractSerializer.base_fields['children'] = ContractSerializer()


class TransactionSerializer(serializers.ModelSerializer):
    type_name = serializers.CharField(max_length=200, required=False)  # only on create
    verbose_type_name = serializers.CharField(max_length=200, read_only=True)

    class Meta:
        model = Transaction
        fields = ('id', 'type_name', 'verbose_type_name', 'status', 
            'note', 'done_at', 'canceled_at', 'executed_at', 'rollbacked_at',
            'created_at', 'updated_at')
        read_only_fields = ('id', 'status', 'executed_at', 'rollbacked_at', 
            'created_at', 'updated_at')


class TransactionProductSerializer(AllowedFieldsModelSerializer):
    product = ProductSerializer()
    transaction = TransactionSerializer()

    class Meta:
        model = TransactionProduct
        fields = ('id', 'product', 'transaction', 'quantity', 'price', 'cost_price', )
        read_only_fields = ('id', )


class FlatTransactionProductSerializer(AllowedFieldsModelSerializer):

    class Meta:
        model = TransactionProduct
        fields = ('id', 'product', 'transaction', 'quantity', 'price', 'cost_price', )
        read_only_fields = ('id', )


class TransactionOnStockSerializer(serializers.ModelSerializer):
    type_name = serializers.CharField(max_length=200, required=False)  # only on create
    verbose_type_name = serializers.CharField(max_length=200, read_only=True)
    stock = StockSerializer()
    product_records = TransactionProductSerializer(many=True, required=False)

    class Meta:
        fields = ('id', 'type_name', 'verbose_type_name', 'note', 'stock', 
            'product_records', 'contract', 
            'status', 'done_at', 'canceled_at', 'executed_at', 
            'rollbacked_at', 'created_at', 'updated_at')
        read_only_fields = ('id', 'status', 'executed_at', 'rollbacked_at',
            'created_at', 'updated_at')


class TransactionOnStoreSerializer(serializers.ModelSerializer):
    type_name = serializers.CharField(max_length=200, required=False)  # only on create
    verbose_type_name = serializers.CharField(max_length=200, read_only=True)
    store = StoreSerializer()
    product_records = TransactionProductSerializer(many=True, required=False)

    class Meta:
        fields = ('id', 'type_name',  'verbose_type_name', 'note', 'store', 
            'product_records', 'contract', 'funds', 'status', 
            'done_at', 'canceled_at', 'executed_at', 'rollbacked_at', 
            'created_at', 'updated_at')
        read_only_fields = ('id', 'status', 'executed_at', 'rollbacked_at',
            'created_at', 'updated_at')


class AllocateTransactionSerializer(TransactionOnStockSerializer):

    class Meta:
        model = AllocateTransaction
        fields = TransactionOnStockSerializer.Meta.fields
        read_only_fields = TransactionOnStockSerializer.Meta.read_only_fields


class PurchaseTransactionSerializer(TransactionOnStockSerializer):

    class Meta:
        model = PurchaseTransaction
        fields = TransactionOnStockSerializer.Meta.fields
        read_only_fields = TransactionOnStockSerializer.Meta.read_only_fields


class ReturnTransactionSerializer(TransactionOnStockSerializer):

    class Meta:
        model = ReturnTransaction
        fields = TransactionOnStockSerializer.Meta.fields
        read_only_fields = TransactionOnStockSerializer.Meta.read_only_fields


class RealizeTransactionSerializer(TransactionOnStockSerializer):

    class Meta:
        model = RealizeTransaction
        fields = TransactionOnStockSerializer.Meta.fields + ('consume_allocation',)
        read_only_fields = TransactionOnStockSerializer.Meta.read_only_fields
        # ('id', 'type_name', 'stock', 'consume_allocation',
        #     'status', 'note', 'done_at', 
        #     'canceled_at', 'executed_at', 'rollbacked_at',)


class RefundTransactionSerializer(TransactionOnStockSerializer):

    class Meta:
        model = RefundTransaction
        fields = TransactionOnStockSerializer.Meta.fields
        read_only_fields = TransactionOnStockSerializer.Meta.read_only_fields


class CreditTransactionSerializer(TransactionOnStoreSerializer):

    class Meta:
        model = CreditTransaction
        fields = TransactionOnStoreSerializer.Meta.fields
        read_only_fields = TransactionOnStoreSerializer.Meta.read_only_fields


class DebitTransactionSerializer(TransactionOnStoreSerializer):

    class Meta:
        model = DebitTransaction
        fields = TransactionOnStoreSerializer.Meta.fields
        read_only_fields = TransactionOnStoreSerializer.Meta.read_only_fields


class TransactionDynamicSerializer(DynamicModelSerializer):

    class Meta:
        types_map = {
            Transaction: TransactionSerializer,
            AllocateTransaction: AllocateTransactionSerializer,
            PurchaseTransaction: PurchaseTransactionSerializer,
            ReturnTransaction: ReturnTransactionSerializer,
            RealizeTransaction: RealizeTransactionSerializer,
            RefundTransaction: RefundTransactionSerializer,
            CreditTransaction: CreditTransactionSerializer,
            DebitTransaction: DebitTransactionSerializer,
        }


class FlatTransactionOnStockSerializer(serializers.ModelSerializer):
    type_name = serializers.CharField(max_length=200, required=False)  # only on create
    verbose_type_name = serializers.CharField(max_length=200, read_only=True)

    class Meta(TransactionOnStockSerializer.Meta):
        pass

class FlatTransactionOnStoreSerializer(serializers.ModelSerializer):
    type_name = serializers.CharField(max_length=200, required=False)  # only on create
    verbose_type_name = serializers.CharField(max_length=200, read_only=True)

    class Meta(TransactionOnStoreSerializer.Meta):
        pass


class FlatAllocateTransactionSerializer(FlatTransactionOnStockSerializer):

    class Meta (AllocateTransactionSerializer.Meta):
        pass


class FlatPurchaseTransactionSerializer(FlatTransactionOnStockSerializer):

    class Meta (PurchaseTransactionSerializer.Meta):
        pass


class FlatReturnTransactionSerializer(FlatTransactionOnStockSerializer):

    class Meta (ReturnTransactionSerializer.Meta):
        pass


class FlatRealizeTransactionSerializer(FlatTransactionOnStockSerializer):

    class Meta (RealizeTransactionSerializer.Meta):
        pass


class FlatRefundTransactionSerializer(FlatTransactionOnStockSerializer):

    class Meta (RefundTransactionSerializer.Meta):
        pass


class FlatCreditTransactionSerializer(FlatTransactionOnStoreSerializer):

    class Meta (CreditTransactionSerializer.Meta):
        pass


class FlatDebitTransactionSerializer(FlatTransactionOnStoreSerializer):

    class Meta (DebitTransactionSerializer.Meta):
        pass


class FlatTransactionDynamicSerializer(DynamicModelSerializer):

    class Meta:
        types_map = {
            Transaction: TransactionSerializer,
            AllocateTransaction: FlatAllocateTransactionSerializer,
            PurchaseTransaction: FlatPurchaseTransactionSerializer,
            ReturnTransaction: FlatReturnTransactionSerializer,
            RealizeTransaction: FlatRealizeTransactionSerializer,
            RefundTransaction: FlatRefundTransactionSerializer,
            CreditTransaction: FlatCreditTransactionSerializer,
            DebitTransaction: FlatDebitTransactionSerializer,
        }
