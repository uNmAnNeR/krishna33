# from django.conf.urls import patterns, url
from rest_framework import routers
from .views import ContractViewSet, TransactionViewSet

contracts_router = routers.SimpleRouter(trailing_slash=False)
transactions_router = routers.SimpleRouter(trailing_slash=False)

contracts_router.register(r'contracts', ContractViewSet)
transactions_router.register(r'transactions', TransactionViewSet)

urlpatterns = contracts_router.urls
urlpatterns += transactions_router.urls
