from django.db.models import Q

from rest_framework import viewsets, status  # generics
from rest_framework.decorators import api_view, permission_classes, action, link
from rest_framework.response import Response
from rest_framework.permissions import IsAdminUser

from .serializers import (ContractSerializer, ContractPlainSerializer, 
    TransactionDynamicSerializer, FlatTransactionDynamicSerializer, 
    TransactionProductSerializer, FlatTransactionProductSerializer)
from ..models import TransactionProduct, Contract, Transaction, AllocateTransaction
from .permissions import CanManageTransactions


class ContractViewSet(viewsets.ModelViewSet):
    model = Contract
    serializer_class = ContractSerializer
    permission_classes = [IsAdminUser,]

    def get_serializer_class(self):
        # local copy to add filter_fields
        class Serializer(self.serializer_class):
            pass

        if 'fields' in self.request.QUERY_PARAMS:
            Serializer.filter_fields = self.request.QUERY_PARAMS.getlist('fields')
        return Serializer

    def list(self, request):
        if 'plain' in request.QUERY_PARAMS:
            # если нужен сквозной список контрактов
            queryset = Contract.objects.all()
            self.serializer_class = ContractPlainSerializer
        else:
            # если нужен иерархичный список контрактов
            queryset = Contract.objects.filter(parent=None)
            self.serializer_class = ContractSerializer
        return Response(self.get_serializer_class()(queryset, many=True).data)

    @link()
    def transactions(self, request, pk=None):
        contract = self.get_object()
        transactions = contract.transactions.all().select_subclasses()
        return Response(TransactionDynamicSerializer(transactions, many=True).data)

    def _create_transaction(self, data):
        contract = self.get_object()
        data['contract'] = contract.id
        transaction = TransactionDynamicSerializer(data=data)
        return transaction

    @action()
    def allocate(self, request, pk=None):
        data = request.QUERY_PARAMS.dict()
        data['type_name'] = AllocateTransaction.type_name()
        transaction = self._create_transaction(data)
        return Response()


class TransactionViewSet(viewsets.ModelViewSet):
    model = Transaction
    serializer_class = TransactionDynamicSerializer
    permission_classes = [IsAdminUser,]
    queryset = Transaction.objects.all().select_subclasses()

    def sync(self, request, pk=None):
        if pk:
            # TODO CHECK
            transaction = self.model.objects.get_subclass(pk=pk)
            type_name = transaction.type_name()
        else:
            transaction = None
            if 'type_name' not in self.request.DATA:
                return Response('type_name should be set', status=status.HTTP_400_BAD_REQUEST)
            type_name = request.DATA.pop('type_name')

        # read product recs
        product_records = request.DATA.pop('product_records', [])

        # init serializer
        serializer_class = FlatTransactionDynamicSerializer.get_serializer_by_type_name(type_name)
        serializer = serializer_class(transaction, data=request.DATA)

        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        serializer.save()
        transaction = serializer.object

        for pr in product_records:
            pr['transaction'] = transaction.id
        product_records_serializer = FlatTransactionProductSerializer(transaction.product_records.all(),
                                                                      product_records,
                                                                      many=True, 
                                                                      allow_add_remove=True)
        if not product_records_serializer.is_valid():
            serializer.errors['product_records'] = product_records_serializer.errors
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        
        product_records_serializer.save()
        transaction_data = serializer.data
        transaction_data['product_records'] = product_records_serializer.data
        return Response(transaction_data, status=status.HTTP_201_CREATED) 

    def create(self, request):
        return self.sync(request)

    def update(self, request, pk):
        return self.sync(request, pk)

    @link()
    def products(self, request, pk=None):
        transaction = self.model.objects.get_subclass(pk=pk)
        print(transaction)
        products = transaction.product_records.all()
        print(products)

        class ProductSerializer(TransactionProductSerializer):
            filter_fields = ('id', 'product', 'quantity', 'price', 'cost_price',)

        return Response(ProductSerializer(products, many=True).data)

    @action()
    def execute(self, request, pk=None):
        tran = self.get_object()
        tran.execute()
        return Response({'status': tran.status, 'executed_at': tran.executed_at})

    @action()
    def rollback(self, request, pk=None):
        tran = self.get_object()
        tran.rollback()
        return Response({'status': tran.status, 'rollbacked_at': tran.rollbacked_at})
