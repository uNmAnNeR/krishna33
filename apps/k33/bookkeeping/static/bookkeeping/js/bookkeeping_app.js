(function () {
    'use strict';

    var serializeForm = function(form, names){
        // var _controls = ['input', 'select', 'textarea'];
        var data = {};
        _.each(names, function(k){
            var c = form.find('[name="'+k+'"]')
            if (c.length) {
                if (c.attr('type') == 'checkbox')
                    data[k] = c.prop('checked');
                else
                    data[k] = c.val();
            }
        });
        console.log('serialized:', data, names);
        return data;
    };

    var SortedCollectionMixin = {
      appendHtml: function(collectionView, itemView, index){
        var childrenContainer = collectionView.itemViewContainer ? collectionView.$(collectionView.itemViewContainer) : collectionView.$el;
        var children = childrenContainer.children();
        if (children.size() <= index) { // new
          childrenContainer.append(itemView.el);
        } else {
          children.eq(index).before(itemView.el);
        }
      }
    };


    var SelectOptionView = Marionette.ItemView.extend({
        tagName: 'option',
        attributes: function() {
            return {
                value: this.model.get('id'),
            }
        }
    });

    var SelectView = Marionette.CollectionView.extend({
        itemView: SelectOptionView,
        tagName: 'select',
        className: 'form-control',

        events: {
            'change': 'valueChanged',
        },

        initialize: function(opts) {
            if (opts)
                this.selectedValue = opts.selectedValue;
        },

        onRender: function() {
            this.$el.val(this.selectedValue);
        },

        valueChanged: function() {
            this.selectedValue = this.$el.val();
            this.trigger('select', this.selectedValue);
        },

        onSelectValue: function(val) {
            this.$el.val(val);
            this.selectedValue = val; // если еще не вызван render не можем вызывать valueChanged
            this.trigger('select', this.selectedValue);
        },
    });

    var Stock = Backbone.Model.extend({
        urlRoot: '/api/store/stocks',

        initialize: function() {
            this.on('sync change:product_records', this.onProductRecordsChanged, this);
        },

        onProductRecordsChanged: function() {
            var productRecords = this.get('product_records');
            if (productRecords) {
                this.productRecords = new SimpleStockProductRecordCollection(productRecords);
                this.unset('product_records');
            }
        },

        loadProductRecords: function() {
            // нужно отдельно использовать отложенные вызов, т.к. 
            // onProductRecordsChanged не успевает отработать и this.productRecords
            // на момент выполнения еще не устанавливается.
            var _this = this;
            var d = new $.Deferred();
            if (_this.productRecords) {
                d.resolve(_this.productRecords);
            } else {
                _this.fetch({
                    data: {
                        fields: 'product_records',
                    },
                    success: function() {
                        d.resolve(_this.productRecords);
                    }
                });
            }
            return d.promise();
        },
    });

    var StockCollection = Backbone.Collection.extend({
        model: Stock,
        url: Stock.prototype.urlRoot,
    });

    var Store = Stock.extend({
        urlRoot: '/api/store/stores',
    });

    var StoreCollection = Backbone.Collection.extend({
        model: Store,
        url: Store.prototype.urlRoot,
    });

    var Product = Backbone.Model.extend({
        urlRoot: '/api/production/products',
    });

    var ProductCollection = Backbone.Collection.extend({
        model: Product,
        url: Product.prototype.urlRoot,
    });

    var SimpleStockProductRecord = Backbone.Model.extend({
        urlRoot: '/api/store/simple-stock-products',

        defaults: {
            quantity: 0,
            available_quantity: 0,
            cost_price: 0.00,
        }
    });

    var SimpleStoreProductRecord = Backbone.Model.extend({
        urlRoot: '/api/store/simple-store-products',

        defaults: {
            quantity: 0,
            available_quantity: 0,
            cost_price: 0.00,
            price: 0.00
        }
    });

    var SimpleStockProductRecordCollection = Backbone.Collection.extend({
        model: SimpleStockProductRecord,
        url: SimpleStockProductRecord.prototype.urlRoot,
    });

    function capitalize(s) {
        return s[0].toUpperCase() + s.slice(1);
    };

    function formatDate(d) {
        if (typeof(d) === 'string' || d instanceof String) {
            d = new Date(d);
        }
        var format2digits = function(val) {
            return val < 10 ? '0' + val : val;
        }
        var date = d.getDate();
        var month = d.getMonth() + 1; //Months are zero based
        var year = d.getFullYear();
        var hours = d.getHours();
        var minutes = d.getMinutes();
        return ('' + date + "." + format2digits(month) + "." + year + 
            ' (' + format2digits(hours) + ':' + format2digits(minutes) + ')');
    };

    var Contract = Backbone.Model.extend({
        urlRoot: '/api/bookkeeping/contracts',

        // TOOD all on sync ?

        initialize: function() {
            this.on('sync change:updated_at', this.onUpdatedAtChanged, this);
            this.on('sync change:created_at', this.onCreatedAtChanged, this);
            this.on('sync change:children', this.onChildrenChanged, this);
            
            this.onUpdatedAtChanged();
            this.onCreatedAtChanged();
            this.onChildrenChanged();
        },

        onUpdatedAtChanged: function() {
            this.set('updated_at', new Date(this.get('updated_at')), {silent:true});
        },

        onCreatedAtChanged: function() {
            this.set('created_at', new Date(this.get('created_at')), {silent:true});
        },

        onChildrenChanged: function() {
            var children = this.get('children');
            if (children){
                this.children = new ContractCollection(children);
                this.unset('children');
            }
        },

        loadTransactions: function() {
            var _this = this;
            var transactions = new TransactionCollection();
            transactions.url = this.url()+'/transactions';
            return transactions.fetch().done(function() {
                _this.transactions = transactions;
            });
        },
    });

    var TransactionProductRecord = Backbone.Model.extend({
        urlRoot: '/api/bookkeeping/transaction-products',

        defaults: {
            price: 0.00,
            quantity: 0,
        }
    });

    var Transaction = Backbone.Model.extend({
        urlRoot: '/api/bookkeeping/transactions',

        defaults: {
            note: '',
        },

        initialize: function() {
            this.on('sync change:updated_at', this.onUpdatedAtChanged, this);
            this.on('sync change:created_at', this.onCreatedAtChanged, this);
            
            this.onUpdatedAtChanged();
            this.onCreatedAtChanged();
        },

        onUpdatedAtChanged: function() {
            this.set('updated_at', new Date(this.get('updated_at')), {silent:true});
            // console.log('update of update', this.get('updated_at'));
        },

        onCreatedAtChanged: function() {
            this.set('created_at', new Date(this.get('created_at')), {silent:true});
        },

        loadProducts: function() {
            var _this = this;
            var productRecords = new TransactionProductRecordCollection();
            productRecords.url = this.url()+'/products';
            return productRecords.fetch().done(function() {
                _this.productRecords = productRecords;
            });
        },

        execute: function() {
            var _this = this;
            var executeUrl = _this.url() + '/execute';
            return $.post(executeUrl, function(data) {
                _this.set(data);
            });
        },

        rollback: function() {
            var _this = this;
            var executeUrl = _this.url() + '/rollback';
            return $.post(executeUrl, function(data) {
                _this.set(data);
            });
        },
    });

    var TransactionOnStock = Transaction.extend({
        defaults: {
            note: '',
            stock: -1
        }
    });

    var RealizeTransaction = Transaction.extend({
        defaults: {
            note: '',
            consume_allocation: false,
            stock: -1
        }
    });

    var TransactionOnStore = Transaction.extend({
        defaults: {
            note: '',
            funds: 0.00,
            store: -1
        }
    });

    var ContractCollection = Backbone.Collection.extend({
        model: Contract,
        url: Contract.prototype.urlRoot,
    });

    var TransactionProductRecordCollection = Backbone.Collection.extend({
        model: TransactionProductRecord,
        url: TransactionProductRecord.prototype.urlRoot,
    });

    var TransactionCollection = Backbone.Collection.extend({
        model: Transaction,
        url: Transaction.prototype.urlRoot,

        comparator: function(transaction) {
            return -transaction.get('updated_at');
        },
    });

    // var ContractCutContentView = Marionette.ItemView.extend({
    //     template: '#contract-cut-template',
    //     className: 'contract',
    //     tagName: 'ul',
    // });

    var ContractLayout = Marionette.Layout.extend({
        template: '#contract-template',
        className: 'contract',

        events: {
            'click .new-transaction': 'newTransaction',
        },

        regions: {
            newTransactionContainer: '.new-transaction-contrainer',
            transactions: '.transactions',
        },

        templateHelpers: {
            formatDate: formatDate,
        },

        newTransaction: function(e) {
            e.preventDefault();
            e.stopPropagation();

            var _this = this;
            var $clicked = $(e.target);
            var command = $clicked.attr('href').slice(1);
            var type_name = capitalize(command) + 'Transaction';
            var containerType = transactionProductsContainer[type_name];
            var transactionModelClass = transactionModelClasses[type_name];

            var newModel = new transactionModelClass({
                'type_name': type_name,
                'contract': this.model.get('id')
            });

            var newTransactionView = new TransactionEditView({
                model: newModel,
            });

            // UI
            newModel.once('sync destroy', function() {
                // первый раз при сохранении или при отмене
                // восстанавливаем состояние панели
                $clicked.closest('.btn-toolbar').find('a').removeClass('disabled');
                $clicked.removeClass('btn-primary').addClass('btn-default');
            });

            newModel.once('sync', function() {
                // подготовим транзакцию к виду, нужному для ее отображения:
                newModel.fetch({silent:true}).done(function() {
                    _this.model.transactions.add(newModel);
                });
                newTransactionView.close();
            });

            // TODO SPINNER

            newTransactionView.loader.done(function() {
                // UI
                // блокируем панель
                $clicked.closest('.btn-toolbar').find('a').addClass('disabled');
                $clicked.removeClass('btn-default').addClass('btn-primary');
                _this.newTransactionContainer.show(newTransactionView);
            });
        },
        
    });

    var TransactionProductView = Marionette.ItemView.extend({
        template: '#transaction-product-template',
        className: 'transaction-product',
        tagName: 'tr',
    });

    var TransactionOnStockProductEditView = Marionette.Layout.extend({
        template: '#transaction-on-stock-product-edit-template',
        className: 'transaction-product-edit',
        tagName: 'tr',

        events: {
            "click a.delete": "deleteClicked"
        },

        regions: {
            products: '.product-container'
        },

        modelEvents: {
            'change:product change:containerInfo': 'updateRequested'
        },

        updateRequested: function() {
            this.trigger('update');
        },

        templateHelpers: function() {
            return {
                'containerInfo': this.model.containerInfo,
            }
        },

        deleteClicked: function(e){
            e.preventDefault();
            e.stopPropagation();
            this.trigger("transactionProduct:delete", this.model);
        },
    });

    var TransactionOnStoreProductEditView = TransactionOnStockProductEditView.extend({
        template: '#transaction-on-store-product-edit-template',
    });

    var TransactionView = Marionette.CompositeView.extend({
        template: '#transaction-template',
        className: 'transaction list-group-item',
        itemView: TransactionProductView,
        itemViewContainer: '.details tbody',
        tagName: 'a',

        events: {
            'click .execute': 'execute',
            'click .rollback': 'rollback',
            'click': 'toggleDetails',
        },

        modelEvents: {
            'change': 'render'
        },

        templateHelpers: {
            formatDate: formatDate,
        },

        execute: function(e) {
            e.stopPropagation();
            e.preventDefault();
            // TODO LOCK AND SPIN
            this.trigger('transaction:execute', this.model);
            this.model.execute();
        },

        rollback: function(e) {
            console.log('rollback!', e);
            e.stopPropagation();
            e.preventDefault();
            // TODO LOCK AND SPIN
            this.trigger('transaction:rollback', this.model);
            this.model.rollback();
        },

        toggleDetails: function() {
            console.log('toggle!');
            var _this = this;
            var transaction = this.model;

            var updateView = function(render) {
                if (render) {
                    _this.render();
                    var specViewClass = transactionSpecificDetailsViews[transaction.get('type_name')];
                    var specView = new specViewClass({
                        model: transaction,
                    });
                    _this.$el.find('.specific-details').html(specView.render().$el);
                }
                _this.$el.find('.details').slideToggle();
                _this.$el.find('.toggle-caret').toggleClass('fa-caret-down').toggleClass('fa-caret-up');
            };

            if (!transaction.productRecords) {
                $.when(transaction.loadProducts()).done(function() {
                    console.log('tr recs:', transaction.productRecords);
                    _this.collection = transaction.productRecords;
                    updateView(true);
                });
            } else {
                updateView();
            }
        },
    });

    var TransactionOnStockProductCollectionEditView = Marionette.CompositeView.extend({
        template: '#transaction-on-stock-products-edit-template',
        itemView: TransactionOnStockProductEditView,
        itemViewContainer: 'tbody',
    });

    var TransactionOnStoreProductCollectionEditView = Marionette.CompositeView.extend({
        template: '#transaction-on-store-products-edit-template',
        itemView: TransactionOnStoreProductEditView,
        itemViewContainer: 'tbody',
    });

    var TransactionEditView = Marionette.Layout.extend(_.extend(FormMixin, {
        template: '#transaction-edit-template',
        className: 'transaction-edit form-horizontal',

        regions: {
            specificDetails: '.specific-details',
            products: '.products',
        },

        events: {
            'click [href="add-product"]': 'addProduct',
            'click [href="delete"]': 'deleteClicked',
            'click [href="save"]': 'saveClicked',
            'click [href="save-and-execute"]': 'saveAndExecuteClicked',
        },

        modelEvents: {
            'destroy': 'onModelDestroyed',
        },

        onModelDestroyed: function() {
            // TODO move to CONTROLLER!
            this.close();
        },

        onRender: function() {
            console.log('on render!');
            var _this = this;
            this.loader.done(function() {
                _this.specificDetails.show(_this.specificDetailsView);
                _this.specificDetailsView.containerSelect.show(_this.containerSelectView);
                if (_this.balanceView)
                    _this.specificDetailsView.balance.show(_this.balanceView);
                _this.products.show(_this.productsView);
            });
        },

        initialize: function() {
            var _this = this;
            var type_name = this.model.get('type_name');
            var containerType = transactionProductsContainer[type_name];
            var newSpecificViewClass = transactionSpecificDetailsEditViews[type_name];

            var loadContainers = (containerType === 'stock' ? 
                                  app.data.loadStocks : 
                                  app.data.loadStores);
           
            _this.loader = new $.Deferred();

            // когда загрузятся список товаров и список складов
            $.when(app.data.loadProducts(), loadContainers()).done(function(products, containers) {

                var selectedContainer = containers.first();

                _this.specificDetailsView = new newSpecificViewClass({
                    model: _this.model,
                });


                
                var containerSelectViewClass = (containerType === 'stock' ? 
                                                StockSelectView : 
                                                StoreSelectView);
                _this.containerSelectView = new containerSelectViewClass({
                    collection: containers,
                    selectedValue: selectedContainer.get('id')
                });
                _this.model.set(containerType, selectedContainer.get('id'));

                _this.containerSelectView.on('select', function(containerId) {
                    _this.model.set(containerType, containerId);
                });

                console.log('type', containerType);
                if (containerType == 'store') {
                    _this.balanceView = new TransactionOnStoreBalanceView({
                        templateHelpers: function() {
                            console.log('RUN HELPERS', selectedContainer);
                            return {
                                container: selectedContainer.attributes
                            }
                        }
                    });
                }




                var productRecords = _this.collection || new TransactionProductRecordCollection();
                var transactionProductCollectionEditViewClass = (containerType === 'stock' ? 
                                                   TransactionOnStockProductCollectionEditView : 
                                                   TransactionOnStoreProductCollectionEditView);
                // console.log(containerType, transactionProductViewClass);
                _this.productsView = new transactionProductCollectionEditViewClass({
                    collection: productRecords,
                    // itemView: transactionProductViewClass,
                });

                _this.productsView.on('itemview:update', function(childView) {
                    var data = serializeForm(childView.$el, ['price', 'quantity']);
                    childView.model.set(data);
                    console.log('childview', childView.model, data);
                    childView.render();
                });

                _this.productsView.on('itemview:render', function(childView) {
                    var productSelectView = new ProductSelectView({
                        collection: app.data.products,
                        selectedValue: childView.model.get('product'),
                    });
                    productSelectView.on('select', function(product) {
                        childView.model.set('product', product);
                    });
                    childView.products.show(productSelectView);
                });

                _this.productsView.on('itemview:transactionProduct:delete', function(childView, model) {
                    model.destroy();
                });




                _this.on('transaction:delete', function(model){
                    model.destroy();
                });

                var readUserInput = function() {
                    var productRecords = 
                    _this.productsView.children.map(function(transactionProductView){
                        transactionProductView.trigger('update');
                        console.log('transactionProductView', transactionProductView.model);
                        return transactionProductView.model.toJSON();
                    });
                    var data = serializeForm(_this.$el, _this.model.keys());
                    _.extend(data, {'product_records': productRecords});
                    _this.model.set(data, {silent:true});
                }

                _this.on('transaction:save', function(){
                    var view = this;
                    readUserInput();
                    view.model.save().done(function(){
                        productRecords.reset(view.model.get('product_records'));
                    });
                });

                _this.on('transaction:saveAndExecute', function(){
                    var view = this;
                    readUserInput();
                    view.model.save().done(function(){
                        productRecords.reset(view.model.get('product_records'));
                    }).then(function() { view.model.execute(); });
                });



                _this.transactionProducts = productRecords;
                // console.log('stock loaded:', stockId);

                // var selectedStock = app.data.stocks.findWhere({id: parseInt(stockId)});

                // БЕДА !!!!! МЕНЯЙ ТУТ ВСЕ
                var addContainerInfoToTransactionProduct = function(transactionProduct, containerRecords) {
                    console.log('hello add');
                    var containerRecord = containerRecords.findWhere({product: parseInt(transactionProduct.get('product'))});
                    containerRecord || (containerRecord = (containerType == 'stock' ? 
                                                           new SimpleStockProductRecord() :
                                                           new SimpleStoreProductRecord())
                        );
                    transactionProduct.containerInfo = containerRecord.attributes;
                    transactionProduct.trigger('change:containerInfo');
                };

                var addContainerInfoToTransactionProductCollection = function(transactionProducts, containerRecords) {
                    transactionProducts.each(function(tp){
                        addContainerInfoToTransactionProduct(tp, containerRecords);
                    });
                };

                var containerChanged = function(containerId) {
                    var _this = this;
                    var d = new $.Deferred();
                    // загружаем инфу о товарах на складе
                    selectedContainer = containers.findWhere({id: parseInt(containerId)});
                    selectedContainer.loadProductRecords().done(function(containerRecords) {
                        // устанавливаем ее на товарах транзакции
                        addContainerInfoToTransactionProductCollection(productRecords, containerRecords);
                        d.resolve();
                    });
                    return d.promise();
                };

                _this.on('transactionProduct:add', function(transactionProduct) {
                    // временно добавляем пустое containerInfo, необходимое только чтобы сработал render
                    // а потом произойдет изменение продукта, т.к. обновится список продуктов, 
                    // сработает событие, установится правильное containerInfo.
                    console.log('ADD!');
                    transactionProduct.on('change:product', function() {
                        addContainerInfoToTransactionProduct(transactionProduct, selectedContainer.productRecords);
                    });
                    if (!transactionProduct.get('product'))
                        transactionProduct.set('product', app.data.products.first().get('id'));
                    // addContainerInfoToTransactionProduct(transactionProduct, selectedStock.productRecords);
                    productRecords.add(transactionProduct);
                });

                // productRecords.on('reset', function(transactionProducts) {
                //     console.log('RESETED!');
                //     addContainerInfoToTransactionProductCollection(transactionProducts, selectedStock);
                // });


                // при изменении склада надо подгружать все товары, находящиеся на нем
                containerChanged(selectedContainer.get('id')).done(function(){
                    _this.loader.resolve();
                });
                _this.model.on('change:stock change:store', function() {
                    // При смене склада обновляем параметры наличия, доступности и т.д. для 
                    // существующих товаров
                    containerChanged(_this.model.get(containerType));
                    // _this.specificDetailsView.render();
                    // _this.specificDetails.show(_this.specificDetailsView);
                    if (_this.balanceView)
                        _this.specificDetailsView.balance.show(_this.balanceView);
                });
            });
        },

        addProduct: function(e) {
            console.log('add prod!', e);
            e.stopPropagation();
            e.preventDefault();
            this.trigger('transactionProduct:add', new TransactionProductRecord);
        },

        deleteClicked: function(e) {
            console.log('delete!', e);
            e.stopPropagation();
            e.preventDefault();
            this.trigger('transaction:delete', this.model);
        },
        saveClicked: function(e) {
            console.log('save!', e);
            e.stopPropagation();
            e.preventDefault();

            this.trigger('transaction:save');
        },

        saveAndExecuteClicked: function(e) {
            console.log('save!', e);
            e.stopPropagation();
            e.preventDefault();

            this.trigger('transaction:saveAndExecute', this);
        },
    }));

    var TransactionListView = Marionette.CollectionView.extend(_.extend(SortedCollectionMixin, { //SortedCollectionView Backbone.Marionette.CollectionView
        itemView: TransactionView,
        className: 'list-group',
    }));

    var TransactionProductListView = Marionette.CollectionView.extend({
        itemView: TransactionProductView,
    });

    var TransactionSpecificDetailsView = Marionette.ItemView.extend({
        template: '#transaction-specific-details-template',
    });

    var TransactionOnStockSpecificDetailsView = Marionette.ItemView.extend({
        template: '#transaction-on-stock-specific-details-template',
    });

    var RealizeTransactionSpecificDetailsView = Marionette.ItemView.extend({
        template: '#realize-transaction-specific-details-template',
    });

    var TransactionOnStoreSpecificDetailsView = Marionette.ItemView.extend({
        template: '#transaction-on-store-specific-details-template',
    });

    var transactionSpecificDetailsViews = {
        'Transaction': TransactionSpecificDetailsView,
        'AllocateTransaction': TransactionOnStockSpecificDetailsView,
        'PurchaseTransaction': TransactionOnStockSpecificDetailsView,
        'ReturnTransaction': TransactionOnStockSpecificDetailsView,
        'RealizeTransaction': RealizeTransactionSpecificDetailsView,
        'RefundTransaction': TransactionOnStockSpecificDetailsView,
        'CreditTransaction': TransactionOnStoreSpecificDetailsView,
        'DebitTransaction': TransactionOnStoreSpecificDetailsView,
    };

    var transactionModelClasses = {
        'Transaction': Transaction,
        'AllocateTransaction': TransactionOnStock,
        'PurchaseTransaction': TransactionOnStock,
        'ReturnTransaction': TransactionOnStock,
        'RealizeTransaction': RealizeTransaction,
        'RefundTransaction': TransactionOnStock,
        'CreditTransaction': TransactionOnStore,
        'DebitTransaction': TransactionOnStore,
    };

    var transactionProductsContainer = {
        'AllocateTransaction': 'stock',
        'PurchaseTransaction': 'stock',
        'ReturnTransaction': 'stock',
        'RealizeTransaction': 'stock',
        'RefundTransaction': 'stock',
        'CreditTransaction': 'store',
        'DebitTransaction': 'store',
    };

    var TransactionSpecificDetailsEditView = Marionette.ItemView.extend({
        template: '#transaction-specific-details-edit-template',
    });

    var ContainerSelectOptionView = SelectOptionView.extend({
        template: '#container-select-option-template',
    });

    var StockSelectView = SelectView.extend({
        itemView: ContainerSelectOptionView,
        attributes: {
            name: 'stock',
        },
    });

    var StoreSelectView = StockSelectView.extend({
        attributes: {
            name: 'store',
        },
    });

    var ProductSelectOptionView = SelectOptionView.extend({
        template: '#product-select-option-template',
    });

    var ProductSelectView = SelectView.extend({
        itemView: ProductSelectOptionView,
        attributes: {
            name: 'product',
        },
    });    

    var TransactionOnStockSpecificDetailsEditView = Marionette.Layout.extend({
        template: '#transaction-on-stock-specific-details-edit-template',
        regions: {
            containerSelect: '.container-container'
        },
    });

    var RealizeTransactionSpecificDetailsEditView = Marionette.Layout.extend({
        template: '#realize-transaction-specific-details-edit-template',
        regions: {
            containerSelect: '.container-container'
        },
    });

    var TransactionOnStoreBalanceView = Marionette.ItemView.extend({
        template: '#transaction-on-store-balance-template',
        tagName: 'span'
    });

    var TransactionOnStoreSpecificDetailsEditView = TransactionOnStockSpecificDetailsEditView.extend({
        template: '#transaction-on-store-specific-details-edit-template',
        regions: {
            containerSelect: '.container-container',
            balance: '.balance-container'
        }
    });

    var transactionSpecificDetailsEditViews = {
        'Transaction': TransactionSpecificDetailsEditView,
        'AllocateTransaction': TransactionOnStockSpecificDetailsEditView,
        'PurchaseTransaction': TransactionOnStockSpecificDetailsEditView,
        'ReturnTransaction': TransactionOnStockSpecificDetailsEditView,
        'RealizeTransaction': RealizeTransactionSpecificDetailsEditView,
        'RefundTransaction': TransactionOnStockSpecificDetailsEditView,
        'CreditTransaction': TransactionOnStoreSpecificDetailsEditView,
        'DebitTransaction': TransactionOnStoreSpecificDetailsEditView,
    };

    var ContractListView = Marionette.CompositeView.extend({
        template: '#contract-cut-template',
        tagName: "ul",
        
        initialize: function(){
            this.collection = this.model.children;
        },

        templateHelpers: {
            formatDate: formatDate,
        },
    });

    var ContractListViewRoot = Marionette.CollectionView.extend({
        itemView: ContractListView,
    });

    var contractController = {

        show: function(id) {
            $.when(loadModel(Contract, id, null, app.contracts)).done(function(contract) {
                // TODO CHECK UPDATE contracts
                $.when(contract.loadTransactions()).done(function() {
                    console.log(contract.transactions);
                    var contractLayout = new ContractLayout({
                        model: contract,
                    });
                    app.contentRegion.show(contractLayout);
                    contractLayout.transactions.show(new TransactionListView({
                        collection: contract.transactions,
                    }))
                });
                
            });
        },

        list: function() {
            $.when(loadCollection(ContractCollection, null, app.contracts)).done(function(contracts) {
                console.log(contracts);
                app.contracts = contracts;
                var contractListView = new ContractListViewRoot({
                    collection: contracts,
                });
                app.contentRegion.show(contractListView);
            });
        },
    };

    var app = new Backbone.Marionette.Application();

    app.addRegions({
        contentRegion: '#app-content'
    });

    app.Router = Marionette.AppRouter.extend({
        appRoutes: {
            '': 'list',
            ':id': 'show',
        }
    });

    app.addInitializer(function() {
        new app.Router({
            controller: contractController,
        })
    });

    app.navigate = function(route, options){
        options || (options = {});
        Backbone.history.navigate(route, options);
    };

    app.getCurrentRoute = function(){
        return Backbone.history.fragment;
    };

    app.on('initialize:after', function(){
        if(Backbone.history)
            Backbone.history.start();
    });

    app.on('contract:show', function(id){
        app.navigate(''+id);
        contractController.show(id);
    });

    app.on('contract:list', function(id){
        app.navigate('');
        contractController.list();
    });

    // var fetchCollection = function(cache)

    var AppData = function() {
        var _this = this;
        this.loadStocks = function() {
            return loadCollection(StockCollection, null, _this.stocks, {
                exclude_fields: 'product_records',
            }).done(function(stocks) {
                console.log('stocks loaded', stocks);
                _this.stocks = stocks;
            });
        };

        this.loadStores = function() {
            return loadCollection(StoreCollection, null, _this.stores, {
                exclude_fields: 'product_records',
            }).done(function(stores) {
                console.log('stores loaded', stores);
                _this.stores = stores;
            });
        };

        this.loadProducts = function() {
            return loadCollection(ProductCollection, null, _this.products).done(function(products) {
                _this.products = products;
            });
        };
    };

    app.data = new AppData();

    app.start();
})();
