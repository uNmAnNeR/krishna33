from decimal import Decimal as D
from django.db import models, transaction
from django.utils.translation import ugettext_lazy as _
from django.utils.translation import ugettext as gt
from django.contrib.auth import get_user_model
import datetime
# from django.db.models.signals import post_save
# from django.dispatch import receiver
from mptt.models import MPTTModel, TreeForeignKey
# from django.utils.functional import lazy

from k33.production.models import Product
from k33.store.models import Stock, Store
from k33.common.mixins import (CreatedUpdatedAtMixin,
    Performable, Cancelable, Freezable, GetOrNoneManager)
from k33.common.api.mixins import DynamicModelMixin
from k33.partners.models import Partner


class Contract(CreatedUpdatedAtMixin, MPTTModel):

    """

        make - заключать
        debt - долг
        disolve, terminate, cancel - расторгать
        sign - подписывать
        duration - срок действия
        expire - истекает
        effective date - дата вступления в силу
    """

    owner = models.ForeignKey(Partner, related_name='owner_contracts')
    partner = models.ForeignKey(Partner, related_name='partner_contracts')
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children')
    note = models.TextField(_('Note'), null=True, blank=True)
    # children
    # transactions

    def __str__(self):
        return '#%i: %s <-> %s' % (self.id, self.owner, self.partner)

    class Meta:
        verbose_name = 'Договор'
        verbose_name_plural = 'Договоры'


class Transaction(DynamicModelMixin, CreatedUpdatedAtMixin):

    """
    Класс для описание абстрактной операции над товарами
    между двумя лицами.
    Например: заказ, покупка, расчет, обмен и т.д.
    Операцию после выполнения следует считать замороженной
    """

    # parent = TreeForeignKey('self', null=True, blank=True, related_name='children')
    # supplier = models.ForeignKey(get_user_model(), null=True, blank=True, related_name='supplier_operations')
    # customer = models.ForeignKey(get_user_model(), null=True, blank=True, related_name='customer_operations')
    # cost = models.DecimalField(max_digits=12, decimal_places=3, default=D(0))
    # note = models.TextField(_('Note'), null=True, blank=True)
    # handler = models.IntegerField(_('Handler'), choices=[]) # TODO rename to _handler_id

    STATELESS, EXECUTED, ROLLBACKED = range(3)

    STATUS_CHOICES = (
        (STATELESS, 'Stateless'),
        (EXECUTED, 'Executed'),
        (ROLLBACKED, 'Rollbacked'),
    )

    note = models.TextField(_('Note'), null=True, blank=True)
    status = models.PositiveSmallIntegerField(_('Status'), choices=STATUS_CHOICES, default=STATELESS)
    contract = models.ForeignKey(Contract, related_name='transactions')
    done_at = models.DateTimeField(null=True, blank=True)
    canceled_at = models.DateTimeField(null=True, blank=True)

    # auto fields
    executed_at = models.DateTimeField(null=True, blank=True)
    rollbacked_at = models.DateTimeField(null=True, blank=True)

    class Meta:
        permissions = (("can_manage_transactions", "Can manage transactions"),)
        verbose_name = 'Транзакция'
        verbose_name_plural = 'Транзакции'

    def __str__(self):
        return '%s: %s %s (%s)' % (self.contract, self.type_name(), self.note, self.STATUS_CHOICES[self.status][1])

    @classmethod
    def verbose_type_name(cls):
        return cls.type_name()

    def execute(self):
        if self.status != self.STATELESS:
            return
        with transaction.atomic():
            self._execute()
            self.status = self.EXECUTED
            self.executed_at = datetime.datetime.utcnow()
            self.save()
            if self.contract:
                # update date
                self.contract.save()

    def rollback(self):
        if self.status != self.EXECUTED:
            return
        with transaction.atomic():
            self._rollback()
            self.status = self.ROLLBACKED
            self.rollbacked_at = datetime.datetime.utcnow()
            self.save()
            if self.contract:
                # update date
                self.contract.save()

    def _execute(self):
        raise NotImplementedError

    def _rollback(self):
        raise NotImplementedError

    # TODO !!! done > cancel
    # def save


class TransactionProduct(models.Model):

    """
    Запись в операции о товаре:
        - товар
        - кол-во
        - цена за единицу
    """

    product = models.ForeignKey(Product)
    transaction = models.ForeignKey(Transaction, related_name='product_records')
    quantity = models.DecimalField(max_digits=12, decimal_places=3)
    # сколько денег реально было в обороте:
    price = models.DecimalField(max_digits=12, decimal_places=3)
    # цена продажи:
    cost_price = models.DecimalField(max_digits=12, decimal_places=3, null=True, blank=True)

    def __str__(self):
        return '%s...: %s' % (str(self.transaction)[:30], self.product)

    class Meta:
        verbose_name = 'Товар транзакции'
        verbose_name_plural = 'Товары транзакции'


class AllocateTransaction(Transaction):

    """
    Операция резервирования товара.
    """

    stock = models.ForeignKey(Stock, related_name='allocate_transactions')
    # customer = models.ForeignKey(get_user_model(), null=True, blank=True, related_name='allocate_transactions')
    
    class Meta:
        verbose_name = 'Т Зарезервировать товар'
        verbose_name_plural = 'Т Зарезервировать товары'

    @classmethod
    def verbose_type_name(cls):
        return 'Резервирование'

    def _execute(self):
        for pr in self.product_records.all():
            self.stock.allocate(pr.product, pr.quantity)

    def _rollback(self):
        for pr in self.product_records.all():
            self.stock.deallocate(pr.product, pr.quantity)




class PurchaseTransaction(Transaction):

    """
    Операция закупки (от поставщика в магазин) товара.
    """

    stock = models.ForeignKey(Stock, related_name='purchase_transactions')
    # supplier = models.ForeignKey(get_user_model(), null=True, blank=True, related_name='purchase_transactions')

    @classmethod
    def verbose_type_name(cls):
        return 'Закупка'

    def _execute(self):
        for pr in self.product_records.all():
            self.stock.move(pr.product, pr.quantity, pr.price)

    def _rollback(self):
        for pr in self.product_records.all():
            self.stock.move(pr.product, -pr.quantity, pr.price)

    class Meta:
        verbose_name = 'Т Закупить товар'
        verbose_name_plural = 'Т Закупить товары'


class ReturnTransaction(Transaction):

    """
    Операция отмены закупки.
    """

    stock = models.ForeignKey(Stock, related_name='return_transactions')
    # supplier = models.ForeignKey(get_user_model(), null=True, blank=True, related_name='purchase_transactions')

    @classmethod
    def verbose_type_name(cls):
        return 'Возврат поставщику'

    def _execute(self):
        for pr in self.product_records.all():
            self.stock.move(pr.product, -pr.quantity, pr.price)

    def _rollback(self):
        for pr in self.product_records.all():
            self.stock.move(pr.product, pr.quantity, pr.price)

    class Meta:
        verbose_name = 'Т Вернуть товар с закупки'
        verbose_name_plural = 'Т Вернуть товары с закупки'


class RealizeTransaction(Transaction):

    """
    Операция вывода товара со склада.
    """

    stock = models.ForeignKey(Stock, related_name='realize_transactions')
    # customer = models.ForeignKey(get_user_model(), null=True, blank=True, related_name='consume_transactions')
    consume_allocation = models.BooleanField(_('Consume allocation'), default=False)

    @classmethod
    def verbose_type_name(cls):
        return 'Реализация'

    def _execute(self):
        for pr in self.product_records.all():
            self.stock.realize(pr.product, pr.quantity, self.consume_allocation)

    def _rollback(self):
        for pr in self.product_records.all():
            self.stock.refund(pr.product, pr.quantity, pr.cost_price)

    class Meta:
        verbose_name = 'Т Реализовать товар'
        verbose_name_plural = 'Т Реализовать товары'


class RefundTransaction(Transaction):

    """
    Операция возврата товара покупателем.
    """

    stock = models.ForeignKey(Stock, related_name='refund_transactions')
    # supplier = models.ForeignKey(get_user_model(), null=True, blank=True, related_name='purchase_transactions')

    @classmethod
    def verbose_type_name(cls):
        return 'Возврат'

    def _execute(self):
        for pr in self.product_records.all():
            self.stock.refund(pr.product, pr.quantity, pr.cost_price)

    def _rollback(self):
        for pr in self.product_records.all():
            self.stock.realize(pr.product, pr.quantity, False)

    class Meta:
        verbose_name = 'Т Принять возврат товара'
        verbose_name_plural = 'Т Принять возврат товаров'


class CreditTransaction(Transaction):

    """
    Операция зачисления денег на счет.
    """

    store = models.ForeignKey(Store, related_name='credit_transactions')
    funds = models.DecimalField(max_digits=12, decimal_places=3, null=True, blank=True)

    @classmethod
    def verbose_type_name(cls):
        return 'Начисление'

    def _execute(self):
        if not self.funds:
            self.funds = sum(pr.price for pr in self.product_records.all())
        self.store.credit(self.funds)

    def _rollback(self):
        self.store.debit(self.funds)

    class Meta:
        verbose_name = 'Т Начислить средства на счет'
        verbose_name_plural = 'Т Начислить средства на счет'


class DebitTransaction(Transaction):

    """
    Операция списания денег на счет.
    """

    store = models.ForeignKey(Store, related_name='debit_transactions')
    funds = models.DecimalField(max_digits=12, decimal_places=3, null=True, blank=True)

    @classmethod
    def verbose_type_name(cls):
        return 'Списание'

    def _execute(self):
        if not self.funds:
            self.funds = sum(pr.price for pr in self.product_records.all())
        self.store.debit(self.funds)

    def _rollback(self):
        self.store.credit(self.funds)

    class Meta:
        verbose_name = 'Т Списать средства со счета'
        verbose_name_plural = 'Т Списать средства со счета'

# class SaleTransaction(Transaction):

#     """
#     Операция продажи товара.
#     """

#     def execute(self):
#         with transaction.atomic():
#             for pr in self.operation.product_records.all():
#                 pb, created = StockProduct.objects.get_or_create(
#                     supplier=self.operation.supplier, product=pr.product)
#                 pb.realize(pr.quantity)

#     def rollback(self):
#         with transaction.atomic():
#             for pr in self.operation.product_records.all():
#                 pb = StockProduct.objects.get_or_none(
#                     supplier=self.operation.supplier, product=pr.product)
#                 if pb:
#                     pb.refund(pr.quantity, pr.cost_price)


# class AbstractOperationHandler(object):

#     """
#     Базовый класс для определения действий над операциями,
#     такими как резервирование, оплата, отгрузка и т.д.
#     """

#     name = 'No name'

#     def __init__(self, op):
#         self.operation = op

#     @classmethod
#     def id(cls):
#         from zlib import crc32
#         return crc32(cls.__qualname__.encode())

#     def execute(self):
#         raise NotImplementedError()

#     def rollback(self):
#         raise NotImplementedError()


# class DefaultOperationHandler(AbstractOperationHandler):

#     """
#     Обработчик, который ничего не делает, а только морозит :)
#     """

#     name = 'Default'

#     def __init__(self, op):
#         super().__init__(op)

#     def execute(self):
#         pass
#         # self.operation.perform(self.operation.SUCCESS, save=False)

#     def rollback(self):
#         pass
#         # self.operation.reset()


# class AllocateOperationHandler(AbstractOperationHandler):

#     """
#     Операция резервирования товара.
#     """

#     name = 'Allocate'

#     def __init__(self, op):
#         super().__init__(op)

#     def execute(self):
#         with transaction.atomic():
#             for pr in self.operation.product_records.all():
#                 pb, created = StockProduct.objects.get_or_create(
#                     supplier=self.operation.supplier, product=pr.product)
#                 pb.allocate(pr.quantity)

#     def rollback(self):
#         with transaction.atomic():
#             for pr in self.operation.product_records.all():
#                 pb = StockProduct.objects.get_or_none(
#                     supplier=self.operation.supplier, product=pr.product)
#                 if pb:
#                     pb.deallocate(pr.quantity)


# class PurchaseOperationHandler(AbstractOperationHandler):

#     """
#     Операция закупки товара.
#     """

#     name = 'Purchase'

#     def __init__(self, op):
#         super().__init__(op)

#     def execute(self):
#         print('purchase execute start')
#         with transaction.atomic():
#             print('atomic')
#             print('recs', self.operation.product_records.all())
#             for pr in self.operation.product_records.all():
#                 pb, created = StockProduct.objects.get_or_create(
#                     supplier=self.operation.customer, product=pr.product)
#                 pb.move(pr.quantity, pr.price)
#                 print(pb)

#     def rollback(self):
#         with transaction.atomic():
#             for pr in self.operation.product_records.all():
#                 pb = StockProduct.objects.get_or_none(
#                     supplier=self.operation.customer, product=pr.product)
#                 if pb:
#                     pb.move(-pr.quantity, pr.price)


# class SaleOperationHandler(AbstractOperationHandler):

#     """
#     Операция продажи товара.
#     """

#     name = 'Sale'

#     def __init__(self, op):
#         super().__init__(op)

#     def execute(self):
#         with transaction.atomic():
#             for pr in self.operation.product_records.all():
#                 pb, created = StockProduct.objects.get_or_create(
#                     supplier=self.operation.supplier, product=pr.product)
#                 pb.realize(pr.quantity)

#     def rollback(self):
#         with transaction.atomic():
#             for pr in self.operation.product_records.all():
#                 pb = StockProduct.objects.get_or_none(
#                     supplier=self.operation.supplier, product=pr.product)
#                 if pb:
#                     pb.refund(pr.quantity, pr.cost_price)


# Operation.register_handler(AbstractOperationHandler)
# Operation.register_handler(DefaultOperationHandler)
# Operation.register_handler(AllocateOperationHandler)
# Operation.register_handler(PurchaseOperationHandler)
# Operation.register_handler(SaleOperationHandler)
