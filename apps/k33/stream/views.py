from django.template.response import TemplateResponse
from django.shortcuts import redirect
from django.views.decorators.http import require_POST
from django.contrib.admin.views.decorators import staff_member_required
from .models import State


TEMPLATES = {
    State.STOPPED: 'stream/nostream.html',
    State.RUNNING: 'stream/stream.html',
}

def state():
    # берем первый объект из базы данных
    return State.objects.all().first()

def status():
    stream_state = state()
    return stream_state.status if stream_state else State.STOPPED

def index(request):
    return TemplateResponse(request, TEMPLATES[status()])

@require_POST
@staff_member_required
def start(request):
    stream_state = state()
    stream_state.status = True
    stream_state.save()
    return redirect('/stream/')

@require_POST
@staff_member_required
def stop(request):
    stream_state = state()
    stream_state.status = False
    stream_state.save()
    return redirect('/stream/')
