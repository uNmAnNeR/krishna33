from django.db import models  # , transaction
from django.utils.translation import ugettext_lazy as _


class State(models.Model):

    """
    Состояние трансляции.
    """

    STOPPED, RUNNING = range(2)

    STATUS_CHOICES = (
        (STOPPED, 'Stopped'),
        (RUNNING, 'Running'),
    )

    status = models.PositiveSmallIntegerField(_('Status'), choices=STATUS_CHOICES, default=STOPPED)
