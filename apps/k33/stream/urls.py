from django.conf.urls import patterns, include, url
from rest_framework import routers

urlpatterns = patterns('k33.stream.views',
    (r'^$', 'index'),
    (r'^start$', 'start'),
    (r'^stop$', 'stop'),
)
