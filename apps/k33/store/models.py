from decimal import Decimal as D
from django.db import models  # , transaction
from django.utils.translation import ugettext_lazy as _
# from django.utils.translation import ugettext as gt
from django.contrib.auth import get_user_model
# import datetime
# from django.db.models.signals import post_save
# from django.dispatch import receiver
# from mptt.models import MPTTModel, TreeForeignKey
# from django.utils.functional import lazy
# from model_utils.managers import InheritanceManager

from k33.production.models import Product
from k33.common.mixins import (CreatedUpdatedAtMixin,
    Performable, Cancelable, Freezable, GetOrNoneManager)
from k33.common.api.mixins import DynamicModelMixin
from k33.partners.models import Partner


class Stock(models.Model):

    """
    Склад.
    """

    name = models.CharField(max_length=100, unique=True)
    owner = models.ForeignKey(Partner, related_name='owned_stocks')

    def __str__(self):
        return '%s (%s)' % (self.name, self.owner)

    def _product_record(self, product):
        pr, created = StockProduct.objects.get_or_create(
                stock=self, product=product)
        return pr

    def available_quantity(self, product):
        pr = self._product_record(product)
        return pr.available_quantity

    def can_consume_allocation(self, product, quantity):

        """
        Зарезирвировано ли место и можно ли его отдать.
        """

        pr = self._product_record(product)
        return quantity <= min(pr.allocated_quantity, pr.quantity)

    def is_available(self, product, quantity, consider_allocated=True):

        """
        Проверка доступности кол-ва.
        """

        pr = self._product_record(product)
        if consider_allocated:
            return quantity <= pr.available_quantity
        else:
            return quantity <= pr.quantity

    def allocate(self, product, quantity):
        pr = self._product_record(product)
        pr.allocated_quantity += quantity
        pr.save()

    def deallocate(self, product, quantity):

        """
        Если кол-во больше, чем доступное, то уменьшаем до 0.
        """

        pr = self._product_record(product)
        pr.allocated_quantity -= min(pr.allocated_quantity, quantity)
        pr.save()

    def realize(self, product, quantity, consume_allocation=True):

        """
        Отгрузка указанного кол-ва товара.
        """

        pr = self._product_record(product)
        if not self.is_available(product, quantity, not consume_allocation):
            raise Exception(_('Invalid realize request (Product <{}> quantity is not available). Available: {}.'.format(
                product.title, self.available_quantity(product))))
        if consume_allocation:
            if not self.can_consume_allocation(product, quantity):
                raise Exception(_('Invalid realize consumption request.'))
            pr.allocated_quantity -= quantity
        pr.quantity -= quantity
        pr.save()


    def refund(self, product, quantity, cost_price):

        """

        """

        pr = self._product_record(product)
        if cost_price:
            # если вернули и цена указана, то надо изменить цену закупки,
            # чтобы в минусе не оказаться
            self.move(product, quantity, cost_price)
        else:
            # иначе меняем только кол-во
            pr.quantity += quantity
        pr.save()

    def move(self, product, quantity, cost_price):

        """
        Закупка (если кол-во положительно)/отмена (если кол-во отрицательное)
        товара по указанной цене.
        """

        if not quantity:
            return

        pr = self._product_record(product)
        if quantity + pr.available_quantity < 0:
            raise Exception(_('Invalid move request'))
        elif quantity + pr.available_quantity > 0:
            pr.cost_price = (
                pr.cost_price * pr.available_quantity +
                quantity * cost_price) / (pr.available_quantity + quantity)
        pr.quantity += quantity
        pr.save()


class Store(Stock):

    """
    Магазин.
    """

    balance = models.DecimalField(max_digits=12, decimal_places=3, default=D(0))

    def credit(self, value):
        self.balance += value
        self.save()

    def debit(self, value):
        self.balance -= value
        self.save()


class StockProduct(DynamicModelMixin, CreatedUpdatedAtMixin):

    """
    Содержит текущую информацию о балансе конкретного товара
    на складе.
    Параметры должны пересчитываться при совершении транзакции,
    в которой участвует склад.
    """

    # supplier = models.ForeignKey(get_user_model(), related_name='supplier_stock_record')
    stock = models.ForeignKey(Stock, related_name='product_records')
    product = models.ForeignKey(Product, related_name='stock_records')

    # ценность штуки товара:
    cost_price = models.DecimalField(max_digits=12, decimal_places=3, default=D(0))
    quantity = models.DecimalField(max_digits=12, decimal_places=3, default=D(0))
    allocated_quantity = models.DecimalField(max_digits=12, decimal_places=3, default=D(0))

    # Сюда можно добавить общую стоимость товаров и цену за все, надо подумать

    class Meta:
        unique_together = ('stock', 'product')

    def __str__(self):
        return '%s: %s' % (self.stock, self.product)

    @property
    def available_quantity(self):
        return max(0, self.quantity - self.allocated_quantity)


class StoreProduct(StockProduct):
    price = models.DecimalField(_('Price'), max_digits=12, decimal_places=3)

    @property
    def store(self):
        return self.stock.store
