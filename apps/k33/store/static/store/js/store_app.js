(function () {
    'use strict';

    var ProductRecord = Backbone.Model.extend({
        urlRoot: '/api/store/store_products'
    });


    var Store = Backbone.Model.extend({
        urlRoot: '/api/store/stores',
    });

    var StoreCollection = Backbone.Collection.extend({
        model: Store,
        url: Store.prototype.urlRoot,
    });


    var ProductRecordCollection = Backbone.Collection.extend({
        model: ProductRecord,
        url: function(){
            return '/api/store/store_products?store='+app.store.id; //+'&fuzzy_title=Java';
            //return '/api/store/stores/'+app.store.id+'/product_records?title=1';  
        },

        comparator: function(product) {
            return [
                product.get('available_quantity') > 0 ? -1 : 1,
                product.get('product').title
            ];
        },
    });

    var BookCutContentView = Marionette.ItemView.extend({
        template: '#store-book-cut-template',
        className: 'row book',
    });

    var ProductCutContentView = Marionette.ItemView.extend({
        template: '#store-product-cut-template',
    });

    var productViews = {
        'Book': BookCutContentView,
    };

    var ProductRecordsListView = Marionette.CollectionView.extend({
        // template: "#store-product-list-template",

        getItemView: function(item) {
            // item view на основании type_name
            var type_name = item.get('product').type_name;
            var itemView = (productViews[type_name] || ProductCutContentView);
            return itemView;
        },
    });

    var controller = {

        showProduct: function(store_id, product_id) {
            
        },

        listProducts: function(store_id) {
            // загружаем данные магазина

            var loadProductRecords = function() {
                $.when(loadCollection(ProductRecordCollection, null, app.productRecords)).done(function(productRecords) {
                    console.log('loaded records:', productRecords);
                    app.productRecords = productRecords;
                    var productRecordsListView = new ProductRecordsListView({
                        collection: productRecords,
                    });
                    app.contentRegion.show(productRecordsListView);
                });
            }

            if (!store_id) {
                if (app.store) {
                    loadProductRecords();                    
                } else {
                    $.when(loadCollection(StoreCollection)).done(function(stores) {
                        app.store = stores.first();

                        // загружаем список продуктов
                        loadProductRecords();
                    });
                }
            } else {
                $.when(loadModel(Store, store_id, null, app.store)).done(function(store) {
                    app.store = store;

                    // загружаем список продуктов
                    loadProductRecords();
                });
            }
        },
    }

    var app = new Backbone.Marionette.Application();

    app.addRegions({
        contentRegion: "#app-content"
    });

    app.Router = Marionette.AppRouter.extend({
        appRoutes: {
            'products': 'listProducts',
            ':store_id/products': 'listProducts',
            ':store_id/products/:product_id': 'showProduct',
        }
    });

    app.addInitializer(function() {
        new app.Router({
            controller: controller,
        })
    });

    app.navigate = function(route, options){
        options || (options = {});
        Backbone.history.navigate(route, options);
    };

    app.getCurrentRoute = function(){
        return Backbone.history.fragment;
    };

    app.on("initialize:after", function(){
        if(Backbone.history)
            Backbone.history.start();
    });

    app.on("store:product_record:show", function(store_id, product_id){
        app.navigate(''+store_id+'/products/'+product_id);
        controller.show(id);
    });

    app.on("store:product_records:list", function(id){
        app.navigate(''+store_id+'/products');
        controller.list();
    });

    app.start();
})();
