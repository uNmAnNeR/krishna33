# from django.template.response import TemplateResponse

import itertools

from django.shortcuts import render, get_object_or_404
from .models import Store, StoreProduct
from ..production.models import Product, Author

def str2int(s):
    try:
        return int(s)
    except (TypeError, ValueError):
        return None


def is_any_available(product):
    return any(sr.available_quantity for sr in product.stock_records.all())

def key_sort_by(book, sort_by):
    if sort_by == 'title':
        return book.title
    elif sort_by == 'price_up':
        return min(sr.price for sr in book.store_records)
    elif sort_by == 'price_down':
        return -min(sr.price for sr in book.store_records)
    elif sort_by == 'novelty':
        return -(book.publication_year or 0)

def index(request):
    # TODO optimize очень много запросов к БД

    filter_available = 'available' in request.GET  # filter only available?
    filter_store = str2int(request.GET.get('store'))
    filter_author = str2int(request.GET.get('author'))
    filter_min_price = str2int(request.GET.get('min_price'))
    filter_max_price = str2int(request.GET.get('max_price'))
    sort_by = request.GET.get('sort_by', 'title')  # title|price_up|price_down|novelty

    store_products = StoreProduct.objects.all()
    if filter_store:  # filter by store if needed
        store_products = store_products.filter(stock=filter_store)
    if filter_author:
        store_products = store_products.filter(product__book__authors=filter_author)
    if filter_min_price:
        store_products = store_products.filter(price__gte=filter_min_price)
    if filter_max_price:
        store_products = store_products.filter(price__lte=filter_max_price)
    store_products_ids = store_products.values_list('product__id', flat=True).distinct()  # filter(stock=store)

    # get products from store records
    products = (Product.objects.select_related('stock_records').get_subclass(id=sp_id)
                for sp_id in store_products_ids)

    if filter_available:  # filter only available if needed
        products = (p for p in products
                    if is_any_available(p))

    # building products is finished, convert to list
    products = list(products)

    # get store records from products again (stupid...)
    for p in products:
        store_records = StoreProduct.objects.filter(product=p)
        if filter_store:
            store_records = store_records.filter(stock=filter_store)
        if filter_author:
            store_records = store_records.filter(product__book__authors=filter_author)
        if filter_min_price:
            store_records = store_records.filter(price__gte=filter_min_price)
        if filter_max_price:
            store_records = store_records.filter(price__lte=filter_max_price)
        if filter_available:
            store_records = [sr for sr in store_records if sr.available_quantity]
        p.store_records = store_records

    # sort
    products = sorted(products, key=lambda p: key_sort_by(p, sort_by))

    # authors
    # authors = list(set(itertools.chain.from_iterable(p.authors.all() for p in products)))
    authors = Author.objects.all()

    # stores
    stores = Store.objects.all()
    # stores = list(set(sr.store
    #                   for p in products
    #                   for sr in p.store_records))

    return render(request, 'store/store.html', {'products': products,
                                                'authors': authors,
                                                'stores': stores,
                                                'filter_store': filter_store,
                                                'filter_author': filter_author,
                                                'filter_available': filter_available,
                                                'filter_min_price': filter_min_price,
                                                'filter_max_price': filter_max_price,
                                                'sort_by': sort_by})


def detail(request, product_id):
    product = Product.objects.get_subclass(id=product_id)
    product.store_records = StoreProduct.objects.filter(product=product)
    return render(request, 'store/full.html', {'product':product})