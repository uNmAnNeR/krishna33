from django.contrib import admin
from .models import Stock, Store, StockProduct, StoreProduct


class StockProductInline(admin.TabularInline):
    model = StockProduct


class StoreProductInline(admin.TabularInline):
    model = StoreProduct


class StockAdmin(admin.ModelAdmin):
    inlines = [StockProductInline]


class StoreAdmin(admin.ModelAdmin):
    inlines = [StoreProductInline]


admin.site.register(Stock, StockAdmin)
admin.site.register(Store, StoreAdmin)
admin.site.register(StockProduct)
admin.site.register(StoreProduct)
