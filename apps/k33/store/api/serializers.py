from rest_framework import serializers
# from k33.accounts.api.serializers import SimpleAccountSerializer
from k33.partners.api.serializers import SimplePartnerSerializer
from ..models import Stock, Store, StockProduct, StoreProduct
from k33.common.api.serializers import (DynamicModelSerializer, 
    AllowedFieldsModelSerializer)
from k33.production.api.serializers import DynamicProductSerializer
from k33.production.models import Product


class SimpleStockProductSerializer(serializers.ModelSerializer):
    available_quantity = serializers.IntegerField(read_only=True)

    class Meta:
        model = StockProduct
        fields = ('id', 'product', 'cost_price', 'quantity', 
            'allocated_quantity', 'available_quantity', )
        read_only_fields = ('id', 'allocated_quantity', 'cost_price')


class StockProductSerializer(SimpleStockProductSerializer):
    # pass
    product = DynamicProductSerializer()#serializers.SerializerMethodField('get_product')# ProductSerializer()

    # def get_product(self, obj):
        # return DynamicProductSerializer(Product.objects.get_subclass(id=obj.id)).data


class SimpleStoreProductSerializer(SimpleStockProductSerializer):
    class Meta:
        model = StoreProduct
        fields = SimpleStockProductSerializer.Meta.fields + ('price',)
        read_only_fields = SimpleStockProductSerializer.Meta.read_only_fields


class StoreProductSerializer(SimpleStoreProductSerializer):
    # pass
    product = DynamicProductSerializer() #serializers.SerializerMethodField('get_product')# ProductSerializer()

    # def get_product(self, obj):
    #     return DynamicProductSerializer(Product.objects.get_subclass(id=obj.id)).data

    # product = serializers.SerializerMethodField('get_product')# ProductSerializer()
    # available_quantity = serializers.IntegerField(read_only=True)


    # class Meta:
    #     model = StoreProduct
    #     fields = ('id', 'product', 'cost_price', 'quantity', 
    #         'allocated_quantity', 'available_quantity', 'price', )
    #     read_only_fields = ('id', )


    # def get_product(self, obj):
    #     return DynamicProductSerializer(Product.objects.get_subclass(id=obj.id)).data


class SimpleStockProductDynamicSerializer(DynamicModelSerializer):

    class Meta:
        types_map = {
            StockProduct: SimpleStockProductSerializer,
            StoreProduct: SimpleStoreProductSerializer,
        }


class StockProductDynamicSerializer(DynamicModelSerializer):

    class Meta:
        types_map = {
            StockProduct: StockProductSerializer,
            StoreProduct: StoreProductSerializer,
        }


class StockSerializer(AllowedFieldsModelSerializer):
    owner = SimplePartnerSerializer()
    product_records = SimpleStockProductDynamicSerializer(many=True)

    class Meta:
        model = Stock
        fields = ('id', 'name', 'owner', 'product_records')
        read_only_fields = ('id', )


class StoreSerializer(StockSerializer):
    # product_records = SimpleStoreProductSerializer(many=True)
    # owner = SimplePartnerSerializer()

    class Meta(StockSerializer.Meta):
        model = Store
        fields = StockSerializer.Meta.fields + ('balance',)
    #     fields = ('id', 'name', 'owner', )
    #     read_only_fields = ('id', )

class StockDynamicSerializer(DynamicModelSerializer):

    class Meta:
        types_map = {
            Stock: StockSerializer,
            Store: StoreSerializer,
        }

# class StockProductSerializer(serializers.ModelSerializer):
#     stock = StockSerializer()
#     product = ProductSerializer()

#     class Meta:
#         model = StockProduct
#         fields = ('id', 'stock', 'product', 'cost_price', 'quantity', 'allocated_quantity', )
#         read_only_fields = ('id', )


# class StoreProductSerializer(serializers.ModelSerializer):
#     store = StoreSerializer()
#     product = ProductSerializer()

#     class Meta:
#         model = StoreProduct
#         fields = ('id', 'store', 'product', 'cost_price', 'quantity', 'allocated_quantity', 
#             'price', )
#         read_only_fields = ('id', )
