from django.conf.urls import patterns, url

from rest_framework import routers
from .views import (StockViewSet, StoreViewSet, 
    SimpleStockProductViewSet, StockProductViewSet, StoreProductViewSet)


stocks_router = routers.SimpleRouter(trailing_slash=False)
stores_router = routers.SimpleRouter(trailing_slash=False)
stock_products_router = routers.SimpleRouter(trailing_slash=False)
simple_stock_products_router = routers.SimpleRouter(trailing_slash=False)
store_products_router = routers.SimpleRouter(trailing_slash=False)

stocks_router.register(r'stocks', StockViewSet)
stores_router.register(r'stores', StoreViewSet)
stock_products_router.register(r'stock_products', StockProductViewSet)
simple_stock_products_router.register(r'simple-stock-products', SimpleStockProductViewSet)
store_products_router.register(r'store_products', StoreProductViewSet)

urlpatterns = stocks_router.urls
urlpatterns += stores_router.urls
urlpatterns += stock_products_router.urls
urlpatterns += simple_stock_products_router.urls
urlpatterns += store_products_router.urls
