from rest_framework.response import Response
from rest_framework.decorators import action, link
from rest_framework import viewsets

from ..models import Stock, Store, StockProduct, StoreProduct
from .serializers import (StockSerializer, StoreSerializer, 
    StockProductSerializer, StoreProductSerializer,
    SimpleStockProductSerializer)
from k33.common.api.permissions import IsStaffOrReadOnly

import django_filters


# Just a regular FilterSet
class StockProductFilter(django_filters.FilterSet):
    title = django_filters.CharFilter(name='product__title')

    class Meta:
        model = StockProduct
        fields = ['stock', 'title']


class StoreProductFilter(django_filters.FilterSet):
    fuzzy_title = django_filters.CharFilter(name='product__title', lookup_type='icontains')
    store = django_filters.NumberFilter(name='stock__store')

    class Meta:
        model = StoreProduct
        fields = ['store', 'fuzzy_title']


class StockViewSet(viewsets.ModelViewSet):
    model = Stock
    serializer_class = StockSerializer
    permission_classes = [IsStaffOrReadOnly]

    def get_serializer_class(self):
        # local copy to add filter_fields

        SerializerClass = type(
            'SerializerClass',
            (self.serializer_class,),
            {}
        )
        # class Serializer(self.serializer_class):
        #     pass


        if 'fields' in self.request.QUERY_PARAMS:
            SerializerClass.filter_fields = self.request.QUERY_PARAMS.getlist('fields')
        if 'exclude_fields' in self.request.QUERY_PARAMS:
            SerializerClass.exclude_fields = self.request.QUERY_PARAMS.getlist('exclude_fields')
        return SerializerClass

    @link()
    def product_records(self, request, pk=None):
        stock = self.get_object()
        product_records = stock.product_records.all()
        return Response(StockProductSerializer(product_records).data)


class StoreViewSet(viewsets.ModelViewSet):
    model = Store
    serializer_class = StoreSerializer
    permission_classes = [IsStaffOrReadOnly]

    @link()
    def product_records(self, request, pk=None):
        store = self.get_object()
        product_records = store.product_records.all().select_subclasses()
        return Response(StoreProductSerializer(product_records, many=True).data)
    # permission_classes = [IsAuthenticated, IsAuthorOrAdmin]


class StockProductViewSet(viewsets.ModelViewSet):
    model = StockProduct
    serializer_class = StockProductSerializer
    permission_classes = [IsStaffOrReadOnly]

    filter_class = StockProductFilter


class SimpleStockProductViewSet(viewsets.ModelViewSet):
    model = StockProduct
    serializer_class = SimpleStockProductSerializer
    permission_classes = [IsStaffOrReadOnly]

    filter_class = StockProductFilter


class StoreProductViewSet(viewsets.ModelViewSet):
    model = StoreProduct
    serializer_class = StoreProductSerializer
    permission_classes = [IsStaffOrReadOnly]

    filter_class = StoreProductFilter
