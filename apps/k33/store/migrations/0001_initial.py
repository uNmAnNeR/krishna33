# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Stock'
        db.create_table('store_stock', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=100)),
            ('owner', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['accounts.MyUser'], related_name='owned_stocks')),
        ))
        db.send_create_signal('store', ['Stock'])

        # Adding model 'Store'
        db.create_table('store_store', (
            ('stock_ptr', self.gf('django.db.models.fields.related.OneToOneField')(unique=True, to=orm['store.Stock'], primary_key=True)),
        ))
        db.send_create_signal('store', ['Store'])

        # Adding model 'StockProduct'
        db.create_table('store_stockproduct', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('stock', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['store.Stock'], related_name='product_records')),
            ('product', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['production.Product'], related_name='stock_records')),
            ('unit_value', self.gf('django.db.models.fields.DecimalField')(default='0', decimal_places=3, max_digits=12)),
            ('quantity', self.gf('django.db.models.fields.DecimalField')(default='0', decimal_places=3, max_digits=12)),
            ('allocated_quantity', self.gf('django.db.models.fields.DecimalField')(default='0', decimal_places=3, max_digits=12)),
        ))
        db.send_create_signal('store', ['StockProduct'])

        # Adding unique constraint on 'StockProduct', fields ['stock', 'product']
        db.create_unique('store_stockproduct', ['stock_id', 'product_id'])

        # Adding model 'StoreProduct'
        db.create_table('store_storeproduct', (
            ('stockproduct_ptr', self.gf('django.db.models.fields.related.OneToOneField')(unique=True, to=orm['store.StockProduct'], primary_key=True)),
            ('price', self.gf('django.db.models.fields.DecimalField')(decimal_places=3, max_digits=12)),
        ))
        db.send_create_signal('store', ['StoreProduct'])


    def backwards(self, orm):
        # Removing unique constraint on 'StockProduct', fields ['stock', 'product']
        db.delete_unique('store_stockproduct', ['stock_id', 'product_id'])

        # Deleting model 'Stock'
        db.delete_table('store_stock')

        # Deleting model 'Store'
        db.delete_table('store_store')

        # Deleting model 'StockProduct'
        db.delete_table('store_stockproduct')

        # Deleting model 'StoreProduct'
        db.delete_table('store_storeproduct')


    models = {
        'accounts.myuser': {
            'Meta': {'object_name': 'MyUser'},
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'db_index': 'True', 'max_length': '255'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'user_set'", 'symmetrical': 'False', 'blank': 'True', 'to': "orm['auth.Group']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'owned': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'user_set'", 'symmetrical': 'False', 'blank': 'True', 'to': "orm['auth.Permission']"})
        },
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'blank': 'True', 'to': "orm['auth.Permission']"})
        },
        'auth.permission': {
            'Meta': {'object_name': 'Permission', 'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)"},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'contenttypes.contenttype': {
            'Meta': {'object_name': 'ContentType', 'db_table': "'django_content_type'", 'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'production.category': {
            'Meta': {'object_name': 'Category'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'null': 'True', 'to': "orm['production.Category']", 'blank': 'True', 'related_name': "'children'"}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        'production.product': {
            'Meta': {'object_name': 'Product'},
            'abbreviation': ('django.db.models.fields.CharField', [], {'null': 'True', 'blank': 'True', 'max_length': '31'}),
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'null': 'True', 'to': "orm['production.Category']", 'blank': 'True', 'related_name': "'products'"}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'hidden': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'hidden_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'primary_image': ('django.db.models.fields.files.ImageField', [], {'null': 'True', 'blank': 'True', 'max_length': '100'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'upc': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True', 'unique': 'True'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        'store.stock': {
            'Meta': {'object_name': 'Stock'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['accounts.MyUser']", 'related_name': "'owned_stocks'"})
        },
        'store.stockproduct': {
            'Meta': {'object_name': 'StockProduct', 'unique_together': "(('stock', 'product'),)"},
            'allocated_quantity': ('django.db.models.fields.DecimalField', [], {'default': "'0'", 'decimal_places': '3', 'max_digits': '12'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['production.Product']", 'related_name': "'stock_records'"}),
            'quantity': ('django.db.models.fields.DecimalField', [], {'default': "'0'", 'decimal_places': '3', 'max_digits': '12'}),
            'stock': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['store.Stock']", 'related_name': "'product_records'"}),
            'unit_value': ('django.db.models.fields.DecimalField', [], {'default': "'0'", 'decimal_places': '3', 'max_digits': '12'})
        },
        'store.store': {
            'Meta': {'object_name': 'Store', '_ormbases': ['store.Stock']},
            'stock_ptr': ('django.db.models.fields.related.OneToOneField', [], {'unique': 'True', 'to': "orm['store.Stock']", 'primary_key': 'True'})
        },
        'store.storeproduct': {
            'Meta': {'object_name': 'StoreProduct', '_ormbases': ['store.StockProduct']},
            'price': ('django.db.models.fields.DecimalField', [], {'decimal_places': '3', 'max_digits': '12'}),
            'stockproduct_ptr': ('django.db.models.fields.related.OneToOneField', [], {'unique': 'True', 'to': "orm['store.StockProduct']", 'primary_key': 'True'})
        }
    }

    complete_apps = ['store']