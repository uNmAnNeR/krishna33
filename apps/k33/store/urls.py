from django.conf.urls import patterns, include, url
from rest_framework import routers

urlpatterns = patterns('k33.store.views',
    (r'^$', 'index'),
    (r'^(\d+)$', 'detail')
)
