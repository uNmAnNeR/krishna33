from django.db import models
from django.contrib.auth import get_user_model
import datetime

from k33.posts.models import Post
from k33.common.mixins import (CreatedUpdatedAtMixin,
                               Publishable,
                               ModifiedAfterPublishingHelper)


class Event(CreatedUpdatedAtMixin, Publishable, ModifiedAfterPublishingHelper):
    begin_date = models.DateField()
    begin_time = models.TimeField(null=True, blank=True)
    end_date = models.DateField(null=True, blank=True)
    end_time = models.TimeField(null=True, blank=True)

    author = models.ForeignKey(get_user_model())
    title = models.CharField(max_length=100)

    announcement = models.ForeignKey(Post, null=True, blank=True, related_name='event_announcements')
    report = models.ForeignKey(Post, null=True, blank=True, related_name='event_reports')

    def __str__(self):
        return '{} ({})'.format(self.title, self.begin_date)

    @property
    def expired_at(self):
        date = self.end_date if self.end_date else self.begin_date
        time = datetime.time()
        if self.end_time:
            time = self.end_time
        elif self.begin_time:
            time = self.begin_time
        return datetime.datetime.combine(date, time)


    @property
    def expired(self):
        return datetime.datetime.utcnow() > self.expired_at

