from datetime import date, time, datetime

from django.db.models import Q
from django.http import Http404
from django.template.response import TemplateResponse
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_POST
from django.shortcuts import redirect, render, get_object_or_404
from django.core.urlresolvers import reverse
from django.template import Context, Template, loader
from django.http import HttpResponse
from django.utils.timezone import utc

from k33.events.models import Event
from k33.posts.models import Post

import json


def filter_user_events(user):
    events = Event.objects.all()
    if user.is_anonymous():
        events = events.exclude(Q(published=False))  # |Q(private=True)
    else:
        # if not user.is_staff and not user.has_perm('private.view_private'):
        #     events = events.exclude(~Q(author=user), private=True)
        if not user.is_staff:
            events = events.exclude(~Q(author=user), published=False)
    return events


def index(request):
    user = request.user
    events = filter_user_events(user)
    only_my = False
    if 'my' in request.GET:
        only_my = True
        if user.is_anonymous():
            from django.contrib.auth.views import redirect_to_login
            return redirect_to_login(request.path)
        posts = events.filter(author=user)
    events = events.order_by('-updated_at')  # '-publish_date',
    return render(request, 'events/events.html', locals())


def detail(request, event_id, document='auto'):
    print(document)
    user = request.user
    event = get_object_or_404(Event, id=event_id)

    if (not event.published and
            (not user.is_staff and user is not event.author)):
        raise Http404
    return render(request, 'events/detail.html', locals())


@login_required
def new(request):
    poster = request.user
    if not poster.is_staff:
        raise Http404

    event = Event(author=poster, begin_date=date.today())
    event.save()

    return redirect('events:edit', event.id)


@login_required
def edit(request, event_id):
    editor = request.user
    event = get_object_or_404(Event, id=event_id)

    if not editor.is_staff and event.author is not editor:
        raise Http404

    # if request.method == 'GET':

    if request.method == 'POST':
        # TODO!
        # data = {}
        # if 'content' in request.POST:
        #     data['raw_content'] = request.POST['content']
        # if 'title' in request.POST:
        #     data['title'] = request.POST['title']
        # if 'tags' in request.POST:
        #     pass  # TODO!
        # for f, v in data.items():
        #     setattr(event, f, v)
        event.save()

    return render(request, 'events/edit.html', {'event': event})


@require_POST
@login_required
def delete(request, event_id):
    deleter = request.user
    event = get_object_or_404(Event, id=event_id)

    if event.author is not deleter and not deleter.is_staff:
        raise Http404

    event.delete()
    return redirect('events:list')


@require_POST
@login_required
def publish(request, event_id):
    publisher = request.user
    event = get_object_or_404(Event, id=event_id)

    if event.author is not publisher and not publisher.is_staff:
        raise Http404

    event.publish()
    return redirect('events:list')


@require_POST
@login_required
def unpublish(request, event_id):
    publisher = request.user
    event = get_object_or_404(Event, id=event_id)

    if event.author is not publisher and not publisher.is_staff:
        raise Http404

    event.unpublish()
    return redirect('events:list')



# @login_required
# def save(request):
# 	if request.method == 'POST':
# 		user = request.user
#
# 		event_id = request.POST['id']
# 		event_date = request.POST['date']
# 		event_time = request.POST['time']
# 		event_title = request.POST['title']
# 		event_content = request.POST['content']
# 		event_tags = request.POST['tags']
#
# 		# затем добавим событие
# 		event = None
#
# 		try:
# 			event = Event.objects.get(id=event_id)
# 		except (ObjectDoesNotExist, ValueError):
# 			event = Event(date=event_date, author=user)
# 		event.date = event_date
# 		event.time = event_time
# 		event.title = event_title
# 		event.content = event_content
# 		event.save()
#
# 		if request.is_ajax():
# 			return HttpResponse(json.dumps(event.id), mimetype="application/json")
# 		else:
# 			return redirect('/events/edit/%s/' % event.id)


# @login_required
# def publish(request):
# 	if request.method == 'POST':
# 		user = request.user
#
# 		event_id = request.POST['id']
# 		event_date = request.POST['date']
# 		event_time = request.POST['time']
# 		event_title = request.POST['title']
# 		event_content = request.POST['content']
# 		event_tags = request.POST['tags']
#
# 		# затем добавим событие
# 		event = None
#
# 		try:
# 			event = Event.objects.get(id=event_id)
# 		except (ObjectDoesNotExist, ValueError):
# 			event = Event(date=event_date, author=user)
# 		event.date = event_date
# 		event.time = event_time
# 		event.title = event_title
# 		event.content = event_content
#
# 		event.published = True
# 		event.publish_date = datetime.utcnow().replace(tzinfo=utc)
# 		event.save()


# @login_required
# def delete(request):
# 	user = request.user
#
# 	success = False
# 	if request.method == 'POST':
# 		event_id = request.POST['event_id']
# 		event = None
# 		try:
# 			event = Event.objects.get(id=event_id)
# 			event.delete()
# 		except (ObjectDoesNotExist, ValueError):
# 			pass
# 		success = True
#
# 		if request.is_ajax():
# 			return HttpResponse(json.dumps(success), mimetype="application/json")
# 		else:
# 			return redirect('/events/')



