# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'EventAnnouncement'
        db.create_table('events_eventannouncement', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('raw_content', self.gf('django.db.models.fields.TextField')(blank=True, null=True)),
            ('rendered_content', self.gf('django.db.models.fields.TextField')(blank=True, null=True)),
        ))
        db.send_create_signal('events', ['EventAnnouncement'])

        # Adding model 'Event'
        db.create_table('events_event', (
            ('post_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['posts.Post'], unique=True, primary_key=True)),
            ('date', self.gf('django.db.models.fields.DateField')()),
            ('time', self.gf('django.db.models.fields.TimeField')(blank=True, null=True)),
            ('announcement', self.gf('django.db.models.fields.related.OneToOneField')(blank=True, to=orm['events.EventAnnouncement'], unique=True, null=True)),
        ))
        db.send_create_signal('events', ['Event'])


    def backwards(self, orm):
        # Deleting model 'EventAnnouncement'
        db.delete_table('events_eventannouncement')

        # Deleting model 'Event'
        db.delete_table('events_event')


    models = {
        'accounts.myuser': {
            'Meta': {'object_name': 'MyUser'},
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '255', 'db_index': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'to': "orm['auth.Group']", 'related_name': "'user_set'", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'owned': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'to': "orm['auth.Permission']", 'related_name': "'user_set'", 'symmetrical': 'False'})
        },
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'to': "orm['auth.Permission']", 'symmetrical': 'False'})
        },
        'auth.permission': {
            'Meta': {'object_name': 'Permission', 'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)"},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'comments.commentable': {
            'Meta': {'object_name': 'Commentable'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'contenttypes.contenttype': {
            'Meta': {'db_table': "'django_content_type'", 'object_name': 'ContentType', 'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'events.event': {
            'Meta': {'_ormbases': ['posts.Post'], 'object_name': 'Event'},
            'announcement': ('django.db.models.fields.related.OneToOneField', [], {'blank': 'True', 'to': "orm['events.EventAnnouncement']", 'unique': 'True', 'null': 'True'}),
            'date': ('django.db.models.fields.DateField', [], {}),
            'post_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['posts.Post']", 'unique': 'True', 'primary_key': 'True'}),
            'time': ('django.db.models.fields.TimeField', [], {'blank': 'True', 'null': 'True'})
        },
        'events.eventannouncement': {
            'Meta': {'object_name': 'EventAnnouncement'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'raw_content': ('django.db.models.fields.TextField', [], {'blank': 'True', 'null': 'True'}),
            'rendered_content': ('django.db.models.fields.TextField', [], {'blank': 'True', 'null': 'True'})
        },
        'posts.post': {
            'Meta': {'_ormbases': ['comments.Commentable', 'votes.Votable', 'private.Privatable', 'tags.Taggable', 'uploads.ContainsUploads'], 'object_name': 'Post'},
            'author': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['accounts.MyUser']"}),
            'commentable_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['comments.Commentable']", 'unique': 'True', 'primary_key': 'True'}),
            'containsuploads_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['uploads.ContainsUploads']", 'unique': 'True'}),
            'modify_date': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'auto_now': 'True'}),
            'privatable_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['private.Privatable']", 'unique': 'True'}),
            'publish_date': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'null': 'True'}),
            'published': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'raw_content': ('django.db.models.fields.TextField', [], {'blank': 'True', 'null': 'True'}),
            'rendered_content': ('django.db.models.fields.TextField', [], {'blank': 'True', 'null': 'True'}),
            'taggable_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['tags.Taggable']", 'unique': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'votable_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['votes.Votable']", 'unique': 'True'})
        },
        'private.privatable': {
            'Meta': {'object_name': 'Privatable'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'private': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        'tags.taggable': {
            'Meta': {'object_name': 'Taggable'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'uploads.containsuploads': {
            'Meta': {'object_name': 'ContainsUploads'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'votes.votable': {
            'Meta': {'object_name': 'Votable'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['events']