# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'Event'
        db.delete_table('events_event')


    def backwards(self, orm):
        # Adding model 'Event'
        db.create_table('events_event', (
            ('time', self.gf('django.db.models.fields.TimeField')(blank=True, null=True)),
            ('post_ptr', self.gf('django.db.models.fields.related.OneToOneField')(primary_key=True, to=orm['posts.Post'], unique=True)),
            ('date', self.gf('django.db.models.fields.DateField')()),
            ('announcement', self.gf('django.db.models.fields.related.OneToOneField')(null=True, blank=True, unique=True, to=orm['events.EventAnnouncement'])),
        ))
        db.send_create_signal('events', ['Event'])


    models = {
        'events.eventannouncement': {
            'Meta': {'object_name': 'EventAnnouncement'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'raw_content': ('django.db.models.fields.TextField', [], {'blank': 'True', 'null': 'True'}),
            'rendered_content': ('django.db.models.fields.TextField', [], {'blank': 'True', 'null': 'True'})
        }
    }

    complete_apps = ['events']