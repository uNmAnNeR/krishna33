from django.conf.urls import patterns, include, url
from . import views

urlpatterns = patterns(
    'k33.events.views',
    url(r'^$', views.index, name='list'),
    url(r'^(\d+)$', views.detail, name='detail'),
    url(r'^(\d+)/(announcement|report)', views.detail, name='announcement'),
    url(r'^new$', views.new, name='new'),
    url(r'^(\d+)/edit$', views.edit, name='edit'),
    url(r'^(\d+)/delete', views.delete, name='delete'),
    url(r'^(\d+)/publish', views.publish, name='publish'),
    url(r'^(\d+)/unpublish', views.unpublish, name='unpublish')
	# (r'^$', 'index'), #p
	# (r'^(\d+)$', 'event'), #p
	# (r'^new$', 'new'), #a
	# (r'^save/$', 'save'), #a
	# (r'^delete/$', 'delete'), #a
	# (r'^edit/(?P<event_id>\d+)/$', 'edit'), #p
    # (r'^comment/add/$', 'comment_add'), #a
    # (r'^comment/(?P<comment_id>\d+)/$', 'comment'), #a
)

