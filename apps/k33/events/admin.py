from django.contrib import admin
from k33.events.models import Event

class EventAdmin(admin.ModelAdmin):
	list_display = ['begin_date', 'begin_time']

admin.site.register(Event, EventAdmin)