function event_save(event) {
	event.preventDefault();
	var save_button = event.target;
	var event_form = $('#event_form');
	var event_id_input = $('input[name="id"]', event_form);
	var event_date = $('input[name="date"]', event_form);
	var event_time = $('input[name="time"]', event_form);
	var post_title = $('input[name="title"]', event_form);
	var post_content = $('textarea[name="content"]', event_form);
	var post_tags = $('input[name="tags"]', event_form);
	var votes_data = [];
	var votes = $('.vote', event_form).each(function(){
		data = vote_data($(this));
		if (data) 
			votes_data.push(data);
	});

	$.ajax({
		type: 'POST',
		url: '/events/save/',
		dataType : "json", 
		data: { 
			'id': event_id_input.val(),
			'date': event_date.val(),
			'time': event_time.val(),
			'title': post_title.val(), 
			'content': post_content.val(), 
			'tags': post_tags.val(),
			'votes': JSON.stringify(votes_data)
		},
		beforeSend: function () {
			// $(quation_element).find('input, button').prop('disabled', true)
		}
	})
	.done(function(event_id) {
		event_id_input.prop("value", event_id);
		//alert($(event_id).prop("value"));
	})
	.fail(function(response, status, error) {
		alert('fail!');
	});
};

function event_delete(event, event_id) {
	event.preventDefault();
	var delete_button = event.target;
	var event_form = $('#event_'+event_id);
	// var post_id = post_form.prop("data-id")
	// alert(post_id)
	$.ajax({
		type: 'POST',
		url: '/events/delete/',
		dataType : "json", 
		data: { 
			'event_id': event_id 
		},
		beforeSend: function () {
			// $(quation_element).find('input, button').prop('disabled', true)
		}
	})
	.done(function(result) {
		if (result) event_form.remove()
	})
	.fail(function(response, status, error) {
		alert(error);
	});
}

// function comment_save(event, event_id, comment_id) {
// 	event.preventDefault();
// 	var save_button = event.target;
// 	var content_textarea = $(save_button).siblings('textarea[name="comment_content"]');
// 	var csrf_token = $(save_button).siblings('input[name="csrfmiddlewaretoken"]');
// 	$.ajax({
// 		type: 'POST',
// 		url: '/comments/add/',
// 		dataType : "json", 
// 		data: { 'comment_id': comment_id,
// 				'event_id': event_id, 
// 				'comment_content': content_textarea.val(),
// 				'csrfmiddlewaretoken': csrf_token.val() },
// 		beforeSend: function () {
// 			// $(quation_element).find('input, button').prop('disabled', true)
// 		}
// 	})
// 	.done(function(resultdict) {
// 		result = resultdict.result;
// 		if (result) {
// 			comment_form = $(save_button).closest("form");
// 			html = resultdict.html;
// 			$(html).insertBefore(comment_form);
// 			content_textarea.val('')
// 		}
// 	})
// 	.fail(function(response, status, error) {
// 	});
// }