# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Votable'
        db.create_table('votes_votable', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal('votes', ['Votable'])

        # Adding model 'Vote'
        db.create_table('votes_vote', (
            ('privatable_ptr', self.gf('django.db.models.fields.related.OneToOneField')(primary_key=True, to=orm['private.Privatable'], unique=True)),
            ('voted', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['votes.Votable'], related_name='votes')),
            ('quation', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('multicheck', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('votes', ['Vote'])

        # Adding model 'Option'
        db.create_table('votes_option', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('text', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('vote', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['votes.Vote'], related_name='options')),
        ))
        db.send_create_signal('votes', ['Option'])

        # Adding model 'Answer'
        db.create_table('votes_answer', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['accounts.MyUser'])),
            ('option', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['votes.Option'], related_name='answers')),
        ))
        db.send_create_signal('votes', ['Answer'])


    def backwards(self, orm):
        # Deleting model 'Votable'
        db.delete_table('votes_votable')

        # Deleting model 'Vote'
        db.delete_table('votes_vote')

        # Deleting model 'Option'
        db.delete_table('votes_option')

        # Deleting model 'Answer'
        db.delete_table('votes_answer')


    models = {
        'accounts.myuser': {
            'Meta': {'object_name': 'MyUser'},
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '255', 'unique': 'True', 'db_index': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['auth.Group']", 'blank': 'True', 'related_name': "'user_set'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'owned': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['auth.Permission']", 'blank': 'True', 'related_name': "'user_set'"})
        },
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '80', 'unique': 'True'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['auth.Permission']", 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'unique_together': "(('content_type', 'codename'),)", 'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'contenttypes.contenttype': {
            'Meta': {'unique_together': "(('app_label', 'model'),)", 'ordering': "('name',)", 'db_table': "'django_content_type'", 'object_name': 'ContentType'},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'private.privatable': {
            'Meta': {'object_name': 'Privatable'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'private': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        'votes.answer': {
            'Meta': {'object_name': 'Answer'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'option': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['votes.Option']", 'related_name': "'answers'"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['accounts.MyUser']"})
        },
        'votes.option': {
            'Meta': {'object_name': 'Option'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'vote': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['votes.Vote']", 'related_name': "'options'"})
        },
        'votes.votable': {
            'Meta': {'object_name': 'Votable'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'votes.vote': {
            'Meta': {'_ormbases': ['private.Privatable'], 'object_name': 'Vote'},
            'multicheck': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'privatable_ptr': ('django.db.models.fields.related.OneToOneField', [], {'primary_key': 'True', 'to': "orm['private.Privatable']", 'unique': 'True'}),
            'quation': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'voted': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['votes.Votable']", 'related_name': "'votes'"})
        }
    }

    complete_apps = ['votes']