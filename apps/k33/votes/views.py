from django.core.urlresolvers import reverse
from django.contrib.admin.views.decorators import staff_member_required
# from django.contrib.auth.models import User
from k33.accounts.models import MyUser as User
from django.http import HttpResponse
from k33.votes.models import Vote, Option, Answer
from django.template.response import TemplateResponse
from django.shortcuts import redirect
from django.template import Context, Template, loader
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.core import serializers

import json

# Create your views here.
@login_required
def index(request):
	user = request.user

	print('index')
	votes = Vote.objects.all()
	# votes_data = []
	# for vote in votes:
		# options_data= []
		# vote_options = vote.options.all()
		# for o in vote_options:
			# voted_users = User.objects.filter(id__in=o.answers.all().values_list("user", flat=True))
			# options_data.append((o, voted_users)) #, any(answers_user)
		# answers = { o: (Answer.objects.filter(option=o).count, any(Answer.objects.filter(option=o, user=user))) for o in options }
		# user_has_voted = Answer.objects.filter(user=user, option__in=vote_options).count()
		# votes_data.append((vote, options_data)) # user_has_voted,
	params =  { 'votes': votes, 'user': user } # { 'votes_data': votes_data, 'user': user }
    
	return TemplateResponse(request,'votes/votes.html', params)

@login_required
def save(request):
	print('check')
	user = request.user

	if request.method == 'POST':
		if request.is_ajax():
			option_id = request.POST['option']
			vote_id = request.POST['vote']
			vote = Vote.objects.get(id=vote_id)
			voted = True if request.POST['voted'] == u'true' else False
			option = vote.options.get(id=option_id)

			options_users = {}
			if option:
				try:
					answer = Answer.objects.get(option=option, user=user)
					if not voted:
						answer.delete()
				except (ObjectDoesNotExist, MultipleObjectsReturned) as e:
					if isinstance(e, MultipleObjectsReturned):
						Answer.objects.filter(option=option, user=user).delete()
					if voted:
						answer = Answer(option=option, user=user)
						answer.save()
			answers = Answer.objects.filter(option=option)

			voted_users = list(User.objects.filter(id__in=answers.values_list("user", flat=True)).values_list("username", flat=True))

			options_users[option_id] = voted_users


			if not vote.multicheck:
				# get all options where vote = vote
				other_options = vote.options.exclude(id=option_id)
				other_answers = Answer.objects.filter(option__in=other_options, user=user)
				changed_options = list(Option.objects.filter(id__in=other_answers.values_list("option", flat=True)).distinct())
				other_answers.delete()
				for o in changed_options:
					changed_answers = Answer.objects.filter(option=o)
					changed_voted_users = list(User.objects.filter(id__in=changed_answers.values_list("user", flat=True)).values_list("username", flat=True))
					options_users[o.id] = changed_voted_users
				
			return HttpResponse(json.dumps(options_users), mimetype="application/json")

@login_required
def delete(request):
	user = request.user

	success = False
	if request.method == 'POST':
		vote_id = request.POST['vote_id']
		vote = None
		try:
			vote = Vote.objects.get(id=vote_id)
			vote.delete()
		except (ObjectDoesNotExist, ValueError):
			pass
		success = True
		if request.is_ajax():
			return HttpResponse(json.dumps(success), mimetype="application/json")
		else:
			return redirect(request.path_info)

@login_required
def reset(request):
	user = request.user
	success = False
	if request.method == 'POST':
		vote_id = request.POST['vote_id']
		vote = Vote.objects.get(id=vote_id)
		vote.answers.delete()
		if request.is_ajax():
			success = True
			return HttpResponse(json.dumps(success), mimetype="application/json")
		else:
			# TODO посмореть откуда пользователь пришел
			return redirect('%s#vote_%s' % (request.path_info, vote.id))

@login_required
def vote(request, vote_id):
	print('vote')
	user = request.user
	vote = Vote.objects.get(id=vote_id)

	if request.is_ajax():
		if request.method == 'GET':
			vote_id = vote_id or request.GET['vote_id']
			try:
				vote = Vote.objects.get(id=vote_id)
				params = {'vote': vote, 'user': user }
				return TemplateResponse(request, 'votes/vote_data.html', params)
			except:
				return HttpResponse('')
	else:
		params = { 'vote': vote, 'user': user }
		return TemplateResponse(request,'votes/vote_page.html', params)

@login_required
def make(request):
	if request.method == 'POST' and request.is_ajax():
		user = request.user
		print(user)
		vote_id = request.POST['vote_id']
		vote = Vote.objects.get(id=vote_id)
		# очистим предыдущее значение
		user_vote_answers = Answer.objects.filter(option__in=vote.options.all, user__exact=user)
		user_vote_answers.delete()
		vote_type = request.POST['type']
		if vote_type == 'vote':
			# пользователь проголосовал
			options_ids = request.POST.getlist('options_ids')
			for o_id in options_ids:
				o = Option.objects.get(id__exact=o_id)
				a = Answer(user=user, option=o)
				a.save()
		elif vote_type == 'decline':
			# пользователь воздержался TODO
			pass 
			# a = Answer(user=user)
			# a.save()
		
		if request.is_ajax():
			t = loader.get_template('votes/vote_results.html')
			vote_html = t.render(Context({ 'vote': vote }))
			success = True # TODO
			return HttpResponse(json.dumps({ 'result': success, 'html': vote_html }), mimetype="application/json")
		else:
			return redirect('%s#vote_%s' % (request.path_info, vote.id))

		# TODO
		# if request.is_ajax():
		# 	option_id = request.POST['option']
			
			
		# 	voted = True if request.POST['voted'] == u'true' else False
		# 	option = vote.options.get(id=option_id)

		# 	options_users = {}
		# 	if option:
		# 		try:
		# 			answer = Answer.objects.get(option=option, user=user)
		# 			if not voted:
		# 				answer.delete()
		# 		except (ObjectDoesNotExist, MultipleObjectsReturned) as e:
		# 			if isinstance(e, MultipleObjectsReturned):
		# 				print('kuku!')
		# 				Answer.objects.filter(option=option, user=user).delete()
		# 			if voted:
		# 				answer = Answer(option=option, user=user)
		# 				answer.save()
		# 	answers = Answer.objects.filter(option=option)

		# 	if not vote.multicheck:
		# 		# get all options where vote = vote
		# 		other_options = vote.options.exclude(id=option_id)
		# 		other_answers = Answer.objects.filter(option__in=other_options, user=user)
		# 		changed_options = list(Option.objects.filter(id__in=other_answers.values_list("option", flat=True)).distinct())
		# 		other_answers.delete()
		# 		for o in changed_options:
		# 			changed_answers = Answer.objects.filter(option=o)
		# 			changed_voted_users = list(User.objects.filter(id__in=changed_answers.values_list("user", flat=True)).values_list("username", flat=True))
		# 			options_users[o.id] = changed_voted_users
				
		# 	return HttpResponse(json.dumps(options_users), mimetype="application/json")

@login_required
def edit(request, vote_id):
	print('vote-edit')
	user = request.user

	if request.is_ajax() and user.is_staff:
		if request.method == 'GET':
			vote_id = request.GET['vote']
			vote = None
			vote_options = None
			try:
				vote = Vote.objects.get(id=vote_id)
				vote_options = vote.options.all()
			except ObjectDoesNotExist:
				pass
			params = {'vote': vote, 'options': vote_options }
			return TemplateResponse(request, 'votes/vote-edit.html', params)

		elif request.method == 'POST':
			vote_id = -1
			vote_text = request.POST['vote_text']
			options = json.loads(request.POST['options'])
			real_options_count = len(set(o['text'] for o in options if len(o['text']) > 0))
			if len(vote_text) > 0 and real_options_count > 0:
				vote_id = request.POST['vote']
				vote_multicheck = True if request.POST['vote_multicheck'] == u'true' else False
				vote = None
				# print(vote_id, vote_text)
				try:
					vote = Vote.objects.get(id=vote_id)
				except (ObjectDoesNotExist, ValueError):
					vote = Vote(author=user, status=0)
				vote.text = vote_text
				vote.multicheck = vote_multicheck
				vote.save()
				vote_id = vote.id


				# mb use bulk_create ?
				vote_options = vote.options
				

				# print(options)
				options_texts = []
				for o in options:
					option_id = o['id']
					option_text = o['text']
					if len(option_text) > 0 and option_text not in options_texts:
						option = None
						if (option_id >= 0):
							# mb option exists in db
							try:
								option = vote_options.get(id=option_id)
							except ObjectDoesNotExist:
								pass
						if option is None:
							option = Option(vote=vote)
						option.text = option_text
						option.save()
						vote_options = vote_options.exclude(id=option.id)
						options_texts.append(option_text)

				# all old options text was changed, other should be deleted 
				vote_options.delete()
					
			return HttpResponse(json.dumps(vote_id), mimetype="application/json")

	else:
		vote = None
		try:
			vote = Vote.objects.get(id=vote_id)
		except ObjectDoesNotExist:
			pass
		params = { 'vote': vote }
		return TemplateResponse(request,'votes/vote_edit_page.html', params)
