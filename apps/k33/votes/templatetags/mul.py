from django import template
register = template.Library()


@register.filter
def mul(value, arg):
	'''
	Multiplies the value by argument.
	Returns empty string on any error.
	'''
	try:
		value = float(value)
		arg = float(arg)
		return value * arg
	except:
		return ''