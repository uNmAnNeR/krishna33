function Option(el) {
	this.el = el;
	this.name = 'option';
	this.fields = ['text'];
	this.load();
}
extend(Option, Model);

function Vote(el) {
	this.el = el;
	this.name = 'vote';
	this.fields = ['id', 'quation'];
	this.arrayFields = { 'options': { 'class': 'option', 'type': Option } };
	this.load();
}
extend(Vote, Model);

function vote_data(vote_container) {
	var vote_quation_input = $('input[name="vote_quation"]', vote_container);
	var vote_quation = $.trim(vote_quation_input.val()).replace(/\s+/g, ' ');
	$(vote_quation_input).val(vote_quation);
	var vote_options = [];
	$('input[name="vote_options"]', vote_container).each(function(){
		option = $.trim($(this).val()).replace(/\s+/g, ' ');
		$(this).val(option);
		if (option)
			vote_options.push(option);
	});
	if (!vote_quation || vote_options.length < 2)
		return null;
	data = {
		'id': vote_container.prop('id').replace('vote_', ''),
		'quation': vote_quation,
		'options': vote_options
	}
	return data;
}

function vote_add(event) {
	event.preventDefault();
	var add_button = event.target;
	$(add_button).before(
		'<div class="vote" id=""> \
			<div><span>Введите вопрос:</span><br> \
			<input type="hidden" name="vote-id"> \
			<input type="text" name="vote-quation"" /> \
			<a class="btn" name="vote_delete" value="delete" onclick="vote_delete(event, -1)">Удалить голосование</a> \
			<br><span>Введите варианты ответов:</span> \
			<div class="option"><input type="hidden" name="option-id" /><input type="text" name="option-text" /><a class="btn" name="vote_option_delete" value="delete" onclick="vote_option_delete(event)">Удалить вариант ответа</a></div> \
			<div class="option"><input type="hidden" name="option-id" /><input type="text" name="option-text" /><a class="btn" name="vote_option_delete" value="delete" onclick="vote_option_delete(event)">Удалить вариант ответа</a></div> \
			<a class="btn" onclick="vote_option_add(event)">Добавить вариант ответа</a> \
			</div> \
		</div>'
	);

	

}

function vote_make(event, vote_id) {
	event.preventDefault();
	var vote_button = event.target;
	var options_ids = [];
	$(vote_button).siblings('input[name="options_ids"]:checked').each(function(){
		options_ids.push($(this).val());
	});
	var csrf_token = $(vote_button).siblings('input[name="csrfmiddlewaretoken"]');
	$.ajax({
		traditional: true,
		type: 'POST',
		url: '/votes/make/',
		dataType : "json", 
		data: { 'type': 'vote',
				'vote_id': vote_id,
				'options_ids': options_ids, 
				'csrfmiddlewaretoken': csrf_token.val() },
		beforeSend: function () {
			// $(quation_element).find('input, button').prop('disabled', true)
		}
	})
	.done(function(resultdict) {
		if (resultdict.result) {
			$(vote_button).closest("form").replaceWith(resultdict.html);
		}
	})
	.fail(function(response, status, error) {
	});
}

function vote_decline(event, vote_id) {
	event.preventDefault();
	var decline_button = event.target;
	var csrf_token = $(decline_button).siblings('input[name="csrfmiddlewaretoken"]');

	$.ajax({
		traditional: true,
		type: 'POST',
		url: '/votes/vote/',
		dataType : "json", 
		data: { 'type': 'decline',
				'vote_id': vote_id,
				'csrfmiddlewaretoken': csrf_token.val() },
		beforeSend: function () {
			// $(quation_element).find('input, button').prop('disabled', true)
		}
	})
	.done(function(resultdict) {
		if (resultdict.result) {
			$(decline_button).closest("form").replaceWith(resultdict.html);
		}
	})
	.fail(function(response, status, error) {
	});
}



function vote_reset(event, vote_id) {
	event.preventDefault();
	if (!confirm("Вы уверены?")) return;
	reset_button = event.target

	$.ajax({
		type: 'POST',
	    url: 'votes/reset/',             // указываем URL изменить //
	    dataType : "json",                     // тип загружаемых данных
	    data: { 
	    	'vote_id': vote_id 
	    },
	    beforeSend: function () {
	    	
	    }
	})
	.done(function() {

	})
	.fail(function(response, status, error) {
		
	})
	.always(function() {
	    
	});
};


function vote_option_add(event) {
	event.preventDefault();
	var add_button = event.target;
	vote = $(add_button).closest('.vote'); //
	options = $('.option', vote);
	if (options.length < 10) {
		$(add_button).before('<div class="option"><input type="hidden" name="option-id" /><input type="text" name="option-text" /> \
				<a class="btn" name="vote_option_delete" value="delete" onclick="vote_option_delete(event)">Удалить вариант ответа</a> \
				</div>');
	} else {
		alert('Максимум возможно 10 вариантов ответов.');
	}
};

function vote_option_delete(event) {
	event.preventDefault();
	var delete_button = event.target;
	vote = $(add_button).closest('.vote'); //
	options = $('.option', vote);
	if (options.length > 2) {
		$(delete_button).closest('.option').remove();
	} else {
		$(delete_button).siblings('input[name="option-text"]').val("");
		alert("В голосовании должно быть минимум 2 варианта ответа.");
	}
};


function vote_delete(event, vote_id) {
	event.preventDefault();
	// TODO если данные не заполнены - то спрашивать не надо.
	if (!confirm("Вы уверены?")) return; 
	var delete_button = event.target;
	$(delete_button).closest('div.vote').remove();
};
// $(document).ready(function(){
// 	$('button[data-id="quation-delete"]').click(
// 	});
// });

// function quation_confirm(event) {
// 	var confirm_button = event.target;
// 	var quation_element = $(confirm_button).closest('[data-id="quation"]');
// 	var quation = $(quation_element).data('quation');
// 	var quation_text = $(quation_element).find('[data-id="quation-text"]').prop('value')
// 	var quation_multicheck = $(quation_element).find('[data-id="quation-type"]').prop('checked')

// 	var options = []
// 	$(quation_element).children('[data-id="quation-option"]').each(function(index){
// 		option_id = $(this).data('option') || -1
// 		text = $(this).children('[data-id="option-text"]:first').prop('value')
// 		options.push({ 'id': option_id, 'text': text });
// 	});

// 	$.ajax({
// 		type: 'POST',
// 	    url: 'quation.edit/',
// 	    dataType : "json", 
// 	    data: { 'quation': quation, 'quation_text': quation_text, 'quation_multicheck': quation_multicheck, 'options': JSON.stringify(options) }, //, 'options': options
// 	    beforeSend: function () {
// 	    	$(quation_element).find('input, button').prop('disabled', true)
// 	    }
// 	})
// 	.done(function(quation_id) {
// 		$.get('quation/', { 'quation': quation_id }, function(html){
// 			// var quation = $(html)
// 			// add_hover_event(quation)
// 			$(quation_element).replaceWith(prepare_quation_html(html));
// 		});
// 	})
// 	.always(function() {

// 	});
// };