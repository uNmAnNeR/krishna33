from django.db import models
from django.contrib import admin
# from django.contrib.auth.models import User
# from k33.accounts.models import MyUser as User
from django.conf import settings
User = settings.AUTH_USER_MODEL

from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned

from k33.private.models import Privatable

class Votable(models.Model):
	pass

def update_votes(voted, votes_data):
	voted.votes.all().delete() #TODO optimize! передается же id -может его использовать?
	for vd in votes_data:
		# TODO check all data
		vote = None
		try:
			# без оптимизации этот код никогда не выполнится без выброса исключения
			vote = Vote.objects.get(id__exact=vd['id'])
			vote.options.all().delete()
			vote.quation = vd['quation']
		except (ObjectDoesNotExist, ValueError):
			vote = Vote(voted=voted, quation=vd['quation'])
		vote.save()
		for vod in vd['options']:
			vote.options.create(text=vod['text'])

class Vote(Privatable):
	voted = models.ForeignKey(Votable, related_name='votes')
	quation = models.CharField(max_length=100) 
	# author = models.ForeignKey(User)
	multicheck = models.BooleanField(default=False)

	# _hidden = models.BooleanField(default=False)
	# def hidden():
	# 	doc = "The hidden property."
	# 	def fget(self):
	# 		print(self.voted)
	# 		return self._hidden #True if (voted and voted.hidden) else 
	# 	def fset(self, value):
	# 		self._hidden = value
	# 	return locals()
	# hidden = property(**hidden())

	@property
	def users_voted(self):
		voted_users_ids = Answer.objects.filter(option__in=self.options.all()).values_list('user', flat=True)
		return User.objects.filter(id__in=voted_users_ids)
		# return User.objects.filter(id__in=changed_answers.values_list("user", flat=True)).values_list("username", flat=True)

	@property
	def answers(self):
		return Answer.objects.filter(option__in=self.options.all())
	
	def __str__(self):
		return "{}: {}".format(str(self.voted), self.quation)

	# @property
	# def test(self):
	# 	return User.objects.filter(id__in=Answer.objects.filter(option__in=self.options.all())) #TODO id__in

class Option(models.Model):
	text = models.CharField(max_length=100)
	vote = models.ForeignKey(Vote, related_name='options')

	@property
	def users_voted(self):
		voted_users_ids = Answer.objects.filter(option__exact=self).values_list('user', flat=True)
		return User.objects.filter(id__in=voted_users_ids)

	def __str__(self):
		return '{}'.format(self.text)

class Answer(models.Model):
	user = models.ForeignKey(User)
	# если опция не выбрана (=null) - пользователь воздержался.
	option = models.ForeignKey(Option, related_name='answers')

	@property
	def vote(self):
		return self.option.vote  #if self.option else None

	def __str__(self):
		return '{} - {}: {}'.format(self.vote, self.user, self.option)


	

class VoteAdmin(admin.ModelAdmin):
	list_display = ['voted', 'multicheck', 'options']

	def options(self, obj):
		return ', '.join(str(o) for o in obj.options.all())

class OptionAdmin(admin.ModelAdmin):
	list_display = ['vote', 'text']

class AnswerAdmin(admin.ModelAdmin):
	list_display = ['vote', 'user', 'option']

admin.site.register(Vote, VoteAdmin)
admin.site.register(Option, OptionAdmin)
admin.site.register(Answer, AnswerAdmin)
