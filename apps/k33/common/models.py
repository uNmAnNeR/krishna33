from django.db import models
from django.utils.translation import ugettext_lazy as _
from .mixins import GenericMixin


# def get_generic_upload_path(instance, filename, subdir='any'):
#     'generics'
#     model_name = instance.content_type.model_class().__name__



# def get_generic_image_upload_path(instance, filename):
#     return get_generic_upload_path(instance, filename, 'images')

def make_upload_path(instance, filename):
    return filename

class Image(GenericMixin):

    image = models.ImageField(_('Image'), upload_to=make_upload_path)
    title = models.CharField(_('Title'), max_length=255, blank=True, null=True)


class Text(GenericMixin):

    text = models.TextField(_('Note'), null=True, blank=True)
