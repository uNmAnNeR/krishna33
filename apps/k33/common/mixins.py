from datetime import timedelta, datetime

from django.db import models
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError, SuspiciousOperation
from django.dispatch import Signal
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic
from model_utils.managers import InheritanceManager


class GenericMixin(models.Model):
    content_type = models.ForeignKey(
        ContentType, related_name="%(app_label)s_%(class)s_related")
    object_id = models.PositiveIntegerField()
    content_object = generic.GenericForeignKey("content_type", "object_id")

    class Meta:
        abstract = True



class CreatedAtMixin(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        abstract = True


class UpdatedAtMixin(models.Model):
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class CreatedUpdatedAtMixin(CreatedAtMixin, UpdatedAtMixin):

    class Meta:
        abstract = True


class Publishable(models.Model):
    published = models.BooleanField(default=False)
    published_at = models.DateTimeField(null=True, blank=True)

    class Meta:
        abstract = True

    def publish(self):
        self.published_at = datetime.utcnow()
        self.published = True
        self.save()

    def unpublish(self):
        self.published = False
        self.save()


class ModifiedAfterPublishingHelper(object):
    MODIFIED_TIMEOUT = timedelta(minutes=5)

    def modified_after_publish(self):
        if (self.publish_date and self.modify_date):
            return (self.modify_date - self.publish_date) > self.MODIFIED_TIMEOUT
        return False


class ActivatableMixin(models.Model):
    active = models.BooleanField(default=True)

    def activate(self, save=True):
        self.active = True
        if save:
            self.save()

    def deactivate(self, save=True):
        self.active = False
        if save:
            self.save()

    class Meta:
        abstract = True


# class PerformerMixin(models.Model):
#     performer = models.ForeignKey(get_user_model(), related_name='performers')

#     class Meta:
#         abstract = True


class Succeedable(models.Model):
    succeed = models.BooleanField(default=False)
    succeed_note = models.TextField(null=True, blank=True)
    succeed_at = models.DateTimeField(null=True, blank=True)

    def save(self, *args, **kwargs):
        if self.succeed:
            if not self.succeed_at:
                # если прервали, но даты еще нет
                import datetime
                self.succeed_at = datetime.datetime.utcnow()
        else:
            # если не прервано снимаем дату
            self.succeed_at = None
        super(Succeedable, self).save(*args, **kwargs)

    def succeed(self, note=None, save=True):
        self.succeed = True
        self.succeed_note = note
        if save:
            self.save()

    class Meta:
        abstract = True


class Cancelable(models.Model):
    canceled_note = models.TextField(null=True, blank=True)
    canceled_at = models.DateTimeField(null=True, blank=True)
    canceler = models.ForeignKey(get_user_model(), related_name='cancelables', 
        null=True, blank=True)

    @property
    def canceled(self):
        return self.canceled_at is not None

    def cancel(self, note=None):
        self.canceled = True
        self.canceled_note = note
        self.save()

    def reset(self):
        if not self.canceled:
            return
        self.canceled_at = None
        self.canceled_note = None
        self.canceler = None
        self.save()

    def save(self, *args, **kwargs):
        if self.canceled:
            if not self.canceler:
                raise ValidationError('Canceler should be set.')
            if not self.canceled_at:
                # если прервали, но даты еще нет
                import datetime
                self.canceled_at = datetime.datetime.utcnow()
        super(Cancelable, self).save(*args, **kwargs)

    class Meta:
        abstract = True


class Performable(models.Model):
    performed_note = models.TextField(null=True, blank=True)
    performed_at = models.DateTimeField(null=True, blank=True)
    performer = models.ForeignKey(get_user_model(), related_name='performables', 
        null=True, blank=True)

    @property
    def performed(self):
        return self.performed_at is not None

    def perform(self, note=None):
        if self.performed:
            return
        self.note = note
        self.save()

    def reset(self, note=None):
        if not self.performed:
            return
        self.performed_at = None
        self.performed_note = None
        self.performer = None
        self.save()

    def save(self, *args, **kwargs):
        if self.performed:
            if not self.performer:
                raise ValidationError('Performer should be set when perform.')
            if not self.performed_at:
                # если выполнили, но даты еще нет
                import datetime
                self.performed_at = datetime.datetime.utcnow()
        super().save(*args, **kwargs)

    class Meta:
        abstract = True


class Hidable(models.Model):
    hidden = models.BooleanField(default=False)
    hidden_at = models.DateTimeField(null=True, blank=True)

    @property
    def shown(self):
        return not self.hidden

    def save(self, *args, **kwargs):
        if self.hidden:
            if not self.hidden_at:
                # если прервали, но даты еще нет
                import datetime
                self.hidden_at = datetime.datetime.utcnow()
        super(Hidable, self).save(*args, **kwargs)

    def hide(self):
        if self.hidden:
            return
        self.hidden = True
        self.save()

    def show(self):
        if not self.hidden:
            return
        self.hidden = False
        self.hidden_at = None
        self.save()

    class Meta:
        abstract = True


class Freezable(models.Model):
    frozen = models.BooleanField(default=False)
    frozen_at = models.DateTimeField(null=True, blank=True)

    def save(self, *args, **kwargs):
        if self.frozen:
            if not self.frozen_at:
                # если заморозили, но даты еще нет
                import datetime
                self.frozen_at = datetime.datetime.utcnow()
            else:
                # если заморозили и дата уже стоит, то менять ничего нельзя
                raise SuspiciousOperation('This model is frozen. Unfreeze it first to change.')
        else:
            # если не заморожено снимаем дату
            self.frozen_at = None
        super(Freezable, self).save(*args, **kwargs)


    def freeze(self, save=True):
        if not self.frozen:
            self.frozen = True
            if save:
                self.save()

    def _unfreeze(self, save=True):
        if self.frozen:
            self.frozen = False
            if save:
                self.save()

    class Meta:
        abstract = True



class GetOrNoneManager(models.Manager):
    """
    Adds get_or_none method to objects
    """
    def get_or_none(self, **kwargs):
        try:
            return self.get(**kwargs)
        except (self.model.DoesNotExist, self.model.MultipleObjectsReturned):
            return None


class PerformOnCreate(models.Model):
    SUCCESS, FAILED = map(str, range(2))
    STATUS_CHOICES = (
        (SUCCESS, 'Success'),
        (FAILED, 'Failed'),
    )
    performed_status = models.CharField(max_length=2,
                                        choices=STATUS_CHOICES,
                                        default=SUCCESS)
    performed_note = models.TextField(null=True, blank=True)
    performed_at = models.DateTimeField(auto_now_add=True)
    # performed = models.BooleanField(default=False)
    performer = models.ForeignKey(get_user_model(), related_name='performables_on_create')

    def save(self, *args, **kwargs):
        perform = not self.pk # если объект еще не существует
        super().save(*args, **kwargs)
        if perform:
            self._perform()

    def _perform(self):
        raise NotImplementedError

    class Meta:
        abstract = True

# @receiver(post_save, sender=PerformOnCreate)
# def perform_on_save(sender, **kwargs):
#     print('Hari!')
#     p = kwargs['instance']
#     if not p.performed:
#         p._perform()
#         p.performed = True
#         p.save()
