// (function () {
//     'use strict';

    var _load = function(deffered, instance, options) {
        if (options && options.fields && options.fields.length == 0) {
            deffered.resolve(instance);
            console.log('cache returned!');
        } else {
            instance.fetch({
                data: options,
                processData: true,
            }).always(function(){
                deffered.resolve(instance);
            });
        }
    };

    var loadCollection = function(collection, variant, cache, data) {
        var d = new $.Deferred();
        var options = {};
        data || (data = {});
        if (variant && collection.prototype.model.prototype.variants)
            $.extend(options, collection.prototype.model.prototype.variants[variant]);
        $.extend(options, data);
        if (cache && cache instanceof Backbone.Collection) {
            d.resolve(cache);
        } else {
            var instance = new collection();
            _load(d, instance, options);
        }
        return d.promise();
    };

    var loadModel = function(model, id, variant, cache) {
        var d = new $.Deferred();
        var options = {};
        if (variant && model.prototype.variants)
            $.extend(options, model.prototype.variants[variant]);

        var instance;

        // КЕШ!
        if (cache){
            if (cache instanceof Backbone.Model && cache.id == id) {
                instance = cache;
                console.log('cache detected!');
            } else if (cache instanceof Backbone.Collection) {
                // может в коллекции он есть
                instance = cache.get(id);
            }
            if (instance) {
                // если нашелся в кеше, то смотрим какие поля надо догружать
                options.fields = _.difference(options.fields, _.keys(instance.attributes));
            }
        }

        instance || (instance = new model({id: id}));

        // подгрузка
        _load(d, instance, options);
        return d.promise();
    };
// })();
