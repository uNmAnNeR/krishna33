var FormMixin = {
    tagName: 'form',

    submit: function(e){
        e.preventDefault();
        e.stopPropagation();
        var data = Backbone.Syphon.serialize(this);
        this.trigger("form:submit", data);
    },

    onFormDataInvalid: function(errors){
        var $form = this.$el;

        var clearFormErrors = function(){
            // var $form = $view.find("form");
            $form.find("span.help-block").each(function(){
                $(this).remove();
            });
            $form.find(".form-group.has-error").each(function(){
                $(this).removeClass("has-error");
            });
        }

        var markErrors = function(value, key) {
            var $formGroup = $form.find("[name="+ key + "]").closest('div.form-group');
            
            if ($formGroup.length ===0) {
                // элемент не был найден, добавляем в крышу
                var $errorEl = $('<span>', { class: "help-block label label-danger", text: key+": "+value });
                $form.prepend($errorEl);//.addClass("has-error");
            } else {
                var $errorEl = $('<span>', { class: "help-block", text: value });
                $formGroup.append($errorEl).addClass("has-error");  
            }
        }

        clearFormErrors();
        _.each(errors, markErrors);
    }
};