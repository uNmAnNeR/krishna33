from django.db import models
from model_utils.managers import InheritanceManager


class DynamicModelMixin(models.Model):

    objects = InheritanceManager()

    @classmethod
    def type_name(cls):
        return cls.__name__

    class Meta:
        abstract = True
