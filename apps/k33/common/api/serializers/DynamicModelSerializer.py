from rest_framework import serializers


class DynamicModelSerializerOptions(serializers.SerializerOptions):

    """
    Meta class options for DynamicModelSerializer
    """

    def __init__(self, meta):
        self.types_map = getattr(meta, 'types_map', None)
        # self.root_model = getattr(meta, 'root_model', None)
        super(DynamicModelSerializerOptions, self).__init__(meta) #dict2class(self.types_map)


class DynamicModelSerializer(serializers.Serializer):

    """
    Класс для сериализации иерархии моделей.
    Предполагается, что используется InstanceManager() из django-model-utils

    Если в потомке указаны fields, то берется это значение, иначе
    происходит сбор всех полей от потомка к родителю. Дополнительные поля в потомке
    указываются в add_fields.
    Аналогично с read_only_fields и exclude.
    """

    _options_class = DynamicModelSerializerOptions
    type_name = serializers.CharField(max_length=200, required=False) # only on create

    def __init__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs
        #
        
        # self.kwargs = {
        #     'instance': instance,
        #     'data': data,
        #     'files': files,
        #     'context': context,
        #     'partial': partial,
        #     'many': many,
        #     'allow_add_remove': allow_add_remove,
        # }
        # self.kwargs.update(kwargs)

        # self.instance = instance
        # self.data = data
        # self.files = files
        # self.context = context
        # self.partial = partial
        # self.many = many
        # self.allow_add_remove = allow_add_remove
        # self.kwargs = kwargs
        # print("KWARGS !!!!", self.kwargs)
        super(DynamicModelSerializer, self).__init__(*self.args, **self.kwargs)

    @classmethod
    def get_serializer_by_model(cls, obj):
        if not obj:
            return serializers.Serializer
        return cls.get_serializer_by_type_name(obj.type_name())

    @classmethod
    def get_serializer_by_type_name(cls, type_name):
        types_map =  cls.Meta.types_map
        return next((s for m,s in types_map.items() if m.type_name() == type_name), 
            serializers.Serializer)

    @staticmethod
    def cast_object(obj):
        if obj and hasattr(obj, 'id'):
            obj = type(obj).objects.get_subclass(id=obj.id)
        return obj

    def to_native(self, obj):
        assert self.opts.types_map is not None, \
            "Serializer class '%s' is missing 'types_map' Meta option" % self.__class__.__name__

        # opts = self.opts # backup opts


        # destination.__dict__.update(source.__dict__)


        ### TEST IT ###
        obj = self.cast_object(obj)
        ###############


        serializer_class = self.get_serializer_by_model(obj)
        serializer = serializer_class(*self.args, **self.kwargs)

        # self.opts = serializers.ModelSerializerOptions(dict2class(self.find_opts_by_model(self.opts.types_map, obj)))
        # self.fields = self.get_fields()
        # ret = super(DynamicModelSerializer, self).to_native(obj)
        # self.opts = opts # restore opts
        return serializer.to_native(obj)

    def from_native(self, data, files):
        """
        Надо именно здесь делать, т.к. уже тут используются fields.
        """
        assert self.opts.types_map is not None, \
            "Serializer class '%s' is missing 'types_map' Meta option" % self.__class__.__name__
        # по полю type_name определяем модель и ее поля
        type_name = None
        self.object = self.cast_object(self.object)
        if self.object is not None:
            # если объект уже существует
            type_name = self.object.type_name()
        elif data:
            # если только создаем объект
            if 'type_name' not in data:
                raise AttributeError('type_name should be set on types_map model when create')
            type_name = data['type_name']
            data = data.copy()
            data.pop('type_name')
        # print('self.obj', self.object)
        # opts = self.opts # backup opts
        # self.opts = serializers.ModelSerializerOptions(dict2class(self.find_opts_by_type_name(self.opts.types_map, type_name)))
        # self.fields = self.get_fields()
        # ret = super(DynamicModelSerializer, self).from_native(data, files)
        # self.opts = opts # restore opts

        serializer_class = self.get_serializer_by_type_name(type_name)
        # print('sc', serializer_class, self.kwargs)
        serializer = serializer_class(*self.args, **self.kwargs)
        # print('s', serializer, data, files)
        return serializer.from_native(data, files)
