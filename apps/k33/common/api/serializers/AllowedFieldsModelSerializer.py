from rest_framework import serializers

class AllowedFieldsModelSerializer(serializers.ModelSerializer):
    """
    A ModelSerializer that takes an additional `fields` argument that
    controls which fields should be displayed.
    """

    def __init__(self, *args, **kwargs):
        # Don't pass the 'fields' arg up to the superclass
        # fields = kwargs.pop('fields', None)

        # Instantiate the superclass normally
        super().__init__(*args, **kwargs)

        if hasattr(self, 'filter_fields'):
            filtered = set(self.filter_fields)
            existing = set(self.fields.keys())
            for field_name in existing - filtered:
                self.fields.pop(field_name)

        if hasattr(self, 'exclude_fields'):
            for field_name in set(self.exclude_fields):
                self.fields.pop(field_name)
