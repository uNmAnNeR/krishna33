# from django.core.urlresolvers import reverse
# from django.contrib.admin.views.decorators import staff_member_required
# from django.contrib.auth.models import User
# from django.http import HttpResponse
# from models import Quation, Option, Answer
# from django.template.response import TemplateResponse
# from django.shortcuts import redirect
# from django.contrib.auth.decorators import login_required
# from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
# from django.core import serializers

# import json

# # Create your views here.
# @login_required
# def index(request):
# 	user = request.user
# 	print('index')
# 	quations = Quation.objects.all()
# 	quations_data = []
# 	for q in quations:
# 		# options = Option.objects.filter(quation=q)
# 		options_data= []
# 		quation_options = q.options.all()
# 		for o in quation_options:
# 			answers_total = Answer.objects.filter(option=o)
# 			# answers_user = answers_total.filter(user=user)
# 			voted_users = User.objects.filter(id__in=answers_total.values_list("user", flat=True))
# 			options_data.append((o, voted_users)) #, any(answers_user)
# 		# answers = { o: (Answer.objects.filter(option=o).count, any(Answer.objects.filter(option=o, user=user))) for o in options }
# 		# user_has_voted = Answer.objects.filter(user=user, option__in=quation_options).count()
# 		quations_data.append((q, options_data)) # user_has_voted,
# 	params = { 'quations_data': quations_data, 'user': user }
    
# 	return TemplateResponse(request,'votings/votings.html', params)

# @login_required
# def option_check(request):
# 	print('check')
# 	user = request.user

# 	if request.method == 'POST':
# 		if request.is_ajax():
# 			option_id = request.POST['option']
# 			quation_id = request.POST['quation']
# 			quation = Quation.objects.get(id=quation_id)
# 			voted = True if request.POST['voted'] == u'true' else False
# 			option = quation.options.get(id=option_id)

# 			options_users = {}
# 			if option:
# 				try:
# 					answer = Answer.objects.get(option=option, user=user)
# 					if not voted:
# 						answer.delete()
# 				except (ObjectDoesNotExist, MultipleObjectsReturned) as e:
# 					if isinstance(e, MultipleObjectsReturned):
# 						print('kuku!')
# 						Answer.objects.filter(option=option, user=user).delete()
# 					if voted:
# 						answer = Answer(option=option, user=user)
# 						answer.save()
# 			answers = Answer.objects.filter(option=option)

# 			voted_users = list(User.objects.filter(id__in=answers.values_list("user", flat=True)).values_list("username", flat=True))

# 			options_users[option_id] = voted_users


# 			if not quation.multicheck:
# 				# get all options where quation = quation
# 				other_options = quation.options.exclude(id=option_id)
# 				other_answers = Answer.objects.filter(option__in=other_options, user=user)
# 				changed_options = list(Option.objects.filter(id__in=other_answers.values_list("option", flat=True)).distinct())
# 				other_answers.delete()
# 				for o in changed_options:
# 					changed_answers = Answer.objects.filter(option=o)
# 					changed_voted_users = list(User.objects.filter(id__in=changed_answers.values_list("user", flat=True)).values_list("username", flat=True))
# 					options_users[o.id] = changed_voted_users
				
# 			return HttpResponse(json.dumps(options_users), mimetype="application/json")

# @login_required
# def quation_remove(request):
# 	print('quation-remove')
# 	user = request.user


# 	success = False
# 	if request.method == 'POST' and request.is_ajax() and user.is_staff:
# 		quation_id = request.POST['quation']
# 		Quation.objects.get(id=quation_id).delete()
# 		success = True
# 	return HttpResponse(json.dumps(success), mimetype="application/json")

# @login_required
# def quation_reset(request):
# 	print('quation-reset')
# 	user = request.user
# 	success = False
# 	if request.method == 'POST' and request.is_ajax() and user.is_staff:
# 		quation_id = request.POST['quation']
# 		Answer.objects.filter(option__in=Quation.objects.get(id=quation_id).options).delete()
# 		success = True
# 	return HttpResponse(json.dumps(success), mimetype="application/json")

# @login_required
# def quation(request):
# 	print('quation')
# 	user = request.user

# 	if request.is_ajax():
# 		if request.method == 'GET':
# 			quation_id = request.GET['quation']
# 			try:
# 				quation = Quation.objects.get(id=quation_id)
# 				quation_options = quation.options.all()
# 				options_data= []
# 				for o in quation_options:
# 					answers_total = Answer.objects.filter(option=o)
# 					voted_users = User.objects.filter(id__in=answers_total.values_list("user", flat=True))
# 					options_data.append((o, voted_users))
# 				params = {'quation': quation, 'options_data': options_data, 'user': user }
# 				return TemplateResponse(request, 'votings/quation.html', params)
# 			except:
# 				return HttpResponse('')

# @login_required
# def quation_edit(request):
# 	print('quation-edit')
# 	user = request.user

# 	if request.is_ajax() and user.is_staff:
# 		if request.method == 'GET':
# 			quation_id = request.GET['quation']
# 			quation = None
# 			quation_options = None
# 			try:
# 				quation = Quation.objects.get(id=quation_id)
# 				quation_options = quation.options.all()
# 			except ObjectDoesNotExist:
# 				pass
# 			params = {'quation': quation, 'options': quation_options }
# 			return TemplateResponse(request, 'votings/quation-edit.html', params)

# 		elif request.method == 'POST':
# 			quation_id = -1
# 			quation_text = request.POST['quation_text']
# 			options = json.loads(request.POST['options'])
# 			real_options_count = len(set(o['text'] for o in options if len(o['text']) > 0))
# 			if len(quation_text) > 0 and real_options_count > 0:
# 				quation_id = request.POST['quation']
# 				quation_multicheck = True if request.POST['quation_multicheck'] == u'true' else False
# 				quation = None
# 				# print(quation_id, quation_text)
# 				try:
# 					quation = Quation.objects.get(id=quation_id)
# 				except (ObjectDoesNotExist, ValueError):
# 					quation = Quation(author=user, status=0)
# 				quation.text = quation_text
# 				quation.multicheck = quation_multicheck
# 				quation.save()
# 				quation_id = quation.id


# 				# mb use bulk_create ?
# 				quation_options = quation.options
				

# 				print(options)
# 				options_texts = []
# 				for o in options:
# 					option_id = o['id']
# 					option_text = o['text']
# 					if len(option_text) > 0 and option_text not in options_texts:
# 						option = None
# 						if (option_id >= 0):
# 							# mb option exists in db
# 							try:
# 								option = quation_options.get(id=option_id)
# 							except ObjectDoesNotExist:
# 								pass
# 						if option is None:
# 							option = Option(quation=quation)
# 						option.text = option_text
# 						option.save()
# 						quation_options = quation_options.exclude(id=option.id)
# 						options_texts.append(option_text)

# 				# all old options text was changed, other should be deleted 
# 				quation_options.delete()
					
# 			return HttpResponse(json.dumps(quation_id), mimetype="application/json")



# # @login_required
# # def option_add(request):
# # 	print('option-add')
# # 	user = request.user


# # 	success = False
# # 	# if request.method == 'POST' and request.is_ajax() and user.is_staff:
# # 	# 	quation_id = request.POST['quation']
# # 	# 	Quation.objects.get(id=quation_id).delete()
# # 	# 	success = True
# # 	return HttpResponse(json.dumps(success), mimetype="application/json")