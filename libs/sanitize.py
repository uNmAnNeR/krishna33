import re
import bleach

VALID_TAGS = ['strong', 'em', 'strike' 'p', 'ul', 'blockquote', 'li', 'ol',
              'br', 'b', 'i', 's', 'u', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6',
              'img', 'hr', 'sup', 'sub', 'a', 'table', 'tr', 'td']

VALID_ATTRS = {
    'a': ['href'],
    'img': ['src', 'width', 'align', 'wrap']
}


def sanitize_html(html):
    html = re.sub(r'\n|\r\n?', '<br>', html.strip())
    return bleach.clean(html, VALID_TAGS, VALID_ATTRS)
