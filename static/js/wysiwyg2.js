(function ($) {
	'use strict';
	$.fn.wysiwyg = function (userOptions, uploadOptions) {
		console.log('DONE!');
		var wysiwyg = this,
			editor = $('textarea', this),
			selectedRange,
			options,
			toolbarBtnSelector,

			execCommand = function (commandWithArgs, valueArg) {
				var commandArr = commandWithArgs.split(' '),
					command = commandArr.shift(),
					args = commandArr.join(' ') + (valueArg || '');
				console.log('COMMAND EXEC:', command, args);
				commands[command](args);
			},
			bindToolbar = function (toolbar) {
				toolbar.find($.fn.wysiwyg.defaults.toolbarBtnSelector).click(function (e) {
					editor.focus();
					execCommand($(this).attr("data-command"));
					e.preventDefault();
				});
			},
			setRange = function(start, end){
				var editorDOM = editor.get(0);
				if(editorDOM.createTextRange) {
					var range = editorDOM.createTextRange();
					range.move("character", start);
					range.select();
				} else if(editorDOM.selectionStart) {
					editorDOM.setSelectionRange(start, end);
				}
			},

			getRange = function () {
				var result = {start: 0, end: 0};
				var editorDOM = editor.get(0);
				// if (editorDOM.setSelectionRange){
				// 	console.log("gg2");
				// 	result.start= editorDOM.selectionStart;
				// 	console.log(result);
				// 	result.end = editorDOM.selectionEnd;
				// } else if (document.selection && document.selection.createRange) {
				// 	console.log("gg");
				// 	var range = document.selection.createRange();
				// 	var stored_range = range.duplicate();
				// 	stored_range.moveToElementText(editorDOM);
				// 	stored_range.setEndPoint('EndToEnd', range);
				// 	result.start = stored_range.text.length - range.text.length;
				// 	result.end = result.start + range.text.length;
				// }
				// return result;

				var start = 0, end = 0, normalizedValue, range,
					textInputRange, len, endRange;

				if (typeof editorDOM.selectionStart == "number" && typeof editorDOM.selectionEnd == "number") {
					start = editorDOM.selectionStart;
					end = editorDOM.selectionEnd;
				} else {
					range = document.selection.createRange();

					if (range && range.parentElement() == editorDOM) {
						len = editorDOM.value.length;
						normalizedValue = editorDOM.value.replace(/\r\n/g, "\n");

						// Create a working TextRange that lives only in the input
						textInputRange = editorDOM.createTextRange();
						textInputRange.moveToBookmark(range.getBookmark());

						// Check if the start and end of the selection are at the very end
						// of the input, since moveStart/moveEnd doesn't return what we want
						// in those cases
						endRange = editorDOM.createTextRange();
						endRange.collapse(false);

						if (textInputRange.compareEndPoints("StartToEnd", endRange) > -1) {
							start = end = len;
						} else {
							start = -textInputRange.moveStart("character", -len);
							start += normalizedValue.slice(0, start).split("\n").length - 1;

							if (textInputRange.compareEndPoints("EndToEnd", endRange) > -1) {
								end = len;
							} else {
								end = -textInputRange.moveEnd("character", -len);
								end += normalizedValue.slice(0, end).split("\n").length - 1;
							}
						}
					}
				}

				return {
					start: start,
					end: end
				};

			},

			insertHeader = function(level) {
				insertTag('h'+level);
			},

			insertImage = function(params){
				// params: src, align, width; width - optional
				console.log('insert image!');
				insertTag('img', params, false);
			},

			insertLink = function(){
				var href = prompt('Введите URL ссылки', 'http://');
				if(href){
					insertTag('a', { 'href': href });
				}
			},

			insertCut = function(){
				var defaultText = 'Читать весь пост';
				var text = prompt('Введите текст кнопки сокращенной версии', defaultText);
				text = text || defaultText;
					insertTag('cut', { 'text': text }, false);
				
			},
			insertTag = function(tagName, attrs, closed){
				if(typeof closed == 'undefined')
					closed = true;
				var attrsArr = _.map(attrs, function(val, key){ 
					return key+'='+'"'+val+'"'; 
				});
				// вставим сам тег в начало
				attrsArr.unshift(tagName);
				var startTag = '<' + attrsArr.join(' ') + '>';
				var endTag = closed ? '</' + tagName + '>' : '';
				editor.focus();

				var scrtop = editor.scrollTop;

				var cursorPos = getRange();
				// console.log(cursorPos);
				var txt_pre = editor.val().substring(0, cursorPos.start);
				var txt_sel = editor.val().substring(cursorPos.start, cursorPos.end);
				var txt_aft = editor.val().substring(cursorPos.end);

				console.log(txt_pre, txt_sel, txt_aft);
				if (cursorPos.start == cursorPos.end){
					var nuCursorPos = cursorPos.start + startTag.length;
				}else{
					var nuCursorPos=String(txt_pre + startTag + txt_sel + endTag).length;
				}

				editor.val(txt_pre + startTag + txt_sel + endTag + txt_aft);

				setRange(nuCursorPos, nuCursorPos);

				if (scrtop) editor.scrollTop = scrtop;
			},

			commands = {
				'bold': function() { insertTag('b'); },
				'italic': function() { insertTag('i'); },
				'underline': function() { insertTag('u'); },
				'strikethrough': function() { insertTag('s'); },
				'blockquote': function() { insertTag('blockquote'); },

				'insertunorderedlist': function() { insertTag('ul'); },
				'insertorderedlist': function() { insertTag('ol'); },
				'insertlistitem': function() { insertTag('li'); },

				'createlink': function() { insertLink(); },
				// 'insertimage': function() { insertImage(); },

				'insertcut': function() { insertCut(); },
				'inserthorizontalrule': function() { insertTag('hr', {}, false); },

				'h1': function() { insertHeader(1); },
				'h2': function() { insertHeader(2); },
				'h3': function() { insertHeader(3); },
			},
			initInsertImagePanel = function(panel) {
				panel.click(function(e) {
					e.stopPropagation();
				});

				panel.keydown('esc', function () {
					this.value='';
					$(this).change();
				});

				var urlInput =  panel.find('input[name="url"]');
				var widthInput =  panel.find('input[name="width"]');
				var wrapCheckbox =  panel.find('input[name="wrap"]');

				var imageAlignPanel = panel.find('.image-align-panel');
				imageAlignPanel.find('a').click(function(ev) {
					imageAlignPanel.find('a').not($(this)).removeClass('btn-primary').addClass('btn-default');
					$(this).removeClass('btn-default').addClass('btn-primary');
					var align = $(this).data('align');
					if (align === 'center') {
						wrapCheckbox.prop({'checked': false, 'disabled': true});
					} else {
						wrapCheckbox.prop('disabled', false);
					}
					return false;
				});

				// var imageUploadButton = panel.find('.upload-image');
				// imageUploadButton.click(function(ev) {
				// 	// ev.preventDefault();
				// 	console.log(panel.find('.upload-input'));
				// 	var fileInput = panel.find('.upload-input');

				// 	fileInput.on('change', function() {
				// 		var formData = new FormData(this);
				// 		console.log(formData);
				// 		formData.append('post', this.model.get('category_id'));
				// 		var view = this;
				// 		var is_success = false;
				// 		$.ajax({
				// 			csrfmiddlewaretoken: csrf_token,
				// 			type: 'POST',
				// 			url: '/api/materials/upload/',
				// 			contentType: false,
				// 			processData: false,
				// 			data: formData,
				// 			beforeSend: function() {
				// 				view.setState('uploading');
				// 			},

				// 			success: function(data) {
				// 				view.model.set(data, {silent: true});
				// 				is_success = true;
				// 			},

				// 			// TODO errors
							
				// 			complete: function() {
				// 				if (is_success) 
				// 					view.setState('uploaded');
				// 				else
				// 					view.setState('noupload');
				// 			},

				// 		});
				// 	});
				// 	fileInput.trigger('click');
				// });
				
				// console.log(panel.find('.cancel-uploading'));
				// panel.find('.cancel-uploading').hide();
				// panel.find('.delete-upload').hide();
				// console.log('uploda pan', panel.find('#fileupload'));
				var imageAddButton = panel.find('[data-manual-command="insertimage"]');

				var imageUploadButton = panel.find('.upload-image');
				var imageCancelUploadingButton = panel.find('.cancel-uploading');
				var imageDeleteUploadButton = panel.find('.delete-upload');
				var imageUploadProgressBar = panel.find('.progress-bar');

				var unitsToggleButton = panel.find('.toggle-units');
				unitsToggleButton.click(function(e){
					if ($(this).text() == 'px') {
						$(this).text('%');
						widthInput.data('units', '%');
					} else {
						$(this).text('px');
						widthInput.data('units', 'px');
					}
				});

			    var imageUpload = panel.find('.image-file-input').fileupload({
			        url: uploadOptions.uploadUrl,
			        dataType: 'json',
			        beforeSend: function(jqXHR) {
			        	imageCancelUploadingButton.show().click(function (e) {
							jqXHR.abort();
						}).parent().append(imageCancelUploadingButton);
			        	imageUploadButton.hide();
			        	imageAddButton.prop('disabled', true);
			        	urlInput.prop('disabled', true).hide();
			        	imageUploadProgressBar.parent().show();
			        },
			        done: function (e, data) {
			        	console.log(data);
			        	urlInput.val(data.result.url);
			        	imageCancelUploadingButton.hide();
			        	imageDeleteUploadButton.show().click(function (e) {
							urlInput.val('');
			        		urlInput.prop('disabled', false);
			        		$(this).hide();
			        		imageUploadButton.show().parent().append(imageUploadButton);
						}).parent().append(imageDeleteUploadButton);
						if (uploadOptions.uploadDone)
							uploadOptions.uploadDone(data.result);
			            // $.each(data.result, function (index, file) {
			            // 	console.log(file.name);
			            //     // $('<p/>').text(file.name).appendTo('#files');
			            // });
			        },
			        progressall: function (e, data) {
			            var progress = parseInt(data.loaded / data.total * 100, 10);
			            if (progress < 100) {
			            	imageUploadProgressBar.css(
				                'width',
				                progress + '%'
				            );
			            } else {
				            imageUploadProgressBar.parent().hide();
				            imageCancelUploadingButton.attr('disabled', true);
				            urlInput.val('Сохранение...');
			        		urlInput.show();
			        	}
			        },
			        error: function(jqXHR, textStatus, errorThrown){
				        if (errorThrown === 'abort') {
				        	urlInput.prop('disabled', false);
				            imageCancelUploadingButton.hide();
			        		imageUploadButton.show().parent().append(imageUploadButton);;
				        } else {
							alert(errorThrown);
				        	imageAddButton.prop('disabled', false).hide();
			        		imageCancelUploadingButton.attr('disabled', false).hide();
			        		imageUploadProgressBar.parent().hide();
			        		urlInput.val('');
			        		urlInput.prop('disabled', false);
			        		urlInput.show();
			        		imageUploadButton.show().parent().append(imageUploadButton);
				        }
			        },
			        complete: function(jqXHR, textStatus) {
			        	imageAddButton.prop('disabled', false);
			        	imageCancelUploadingButton.attr('disabled', false);
			        	// в случае ошибки
			        	imageUploadProgressBar.parent().hide();
			        	urlInput.show();
			        },
			    }).prop('disabled', !$.support.fileInput)
			      .parent().addClass($.support.fileInput ? undefined : 'disabled');

			    


				panel.find('[data-manual-command="insertimage"]').click(function(){
					var widthStr = widthInput.val();
					var widthNum = widthStr.match(/^\s*(\d+)\s*$/);
					var width;
					if (widthNum) { // подходит					
						width = parseInt(widthNum[1]);
						var widthUnits = widthInput.data('units');
						switch(widthUnits) {
							case 'px':
								width = '' + Math.min(Math.max(width, 100), 940);
								break;
							case '%':
								width = '' + Math.min(width, 100) + '%';
								break;
						}
					}
					width = width || '100%';
					var align = imageAlignPanel.find('.btn-primary').data('align');
					var url = urlInput.val();
					var wrap = wrapCheckbox.prop('checked');
					var params = {
						width: width,
						align: align,
						src: url,
						wrap: wrap,
					}
					// TODO check URL/width
					insertImage(params);
					// clean
					urlInput.val('');
					widthInput.val('');
					imageAlignPanel.find('a')
						.removeClass('btn-primary')
						.addClass('btn-default')
						.first().addClass('btn-primary');

					$(panel).prev().dropdown('toggle'); // prev - dropdown button toggle
				});
			}
		options = $.extend({}, $.fn.wysiwyg.defaults, userOptions);
		bindToolbar(wysiwyg.find(options.toolbarSelector), options);
		wysiwyg.find('[title]').tooltip({container:'body'});
		initInsertImagePanel($('.insert-image-dropdown'));


		return this;
	};
	$.fn.wysiwyg.defaults = {
		hotKeys: {
			'ctrl+b meta+b': 'bold',
			'ctrl+i meta+i': 'italic',
			'ctrl+u meta+u': 'underline',
		},
		
		toolbarSelector: '[role=toolbar]',
		toolbarBtnSelector: 'a[data-command],button[data-command]',
		commandRole: 'edit',
		activeToolbarClass: 'btn-info',
		selectionMarker: 'edit-focus-marker',
		selectionColor: 'darkgrey',
	};
}(window.jQuery));
